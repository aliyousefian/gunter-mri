#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 11/10/08
# AUTHOR(S): Zarrar Shehzad


# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
## can override this file at command-line
commandFile="${GUNTHERDIR}/commands/nuisance_nonlin2mm_noPW.sh"
normResiduals=1
## Nuisance Template
nuisanceTemplate="${GUNTHERDIR}/etc/evs_nuisance.txt"
nuisanceTemplate_noGlobal="${GUNTHERDIR}/etc/evs_nuisance_noglobal.txt"

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <files list subjects>"
    echo "  $0 [OPTIONS] -S <subject ids>"
    echo "Program:"
    echo "   Regress out 9 nuisance variables from functional data and output residuals resampled to standard space"
    echo "Required Options:"
    echo "  * -S or -s"
    echo "Available Options:"
    echo "  -N: directory storing all the nuisance info (default: ${gDIR_NUISANCE})"
    echo "  -D: do not normalize the residuals (i.e. do not divide residuals by standard deviation), default is to normalize"
    echo "  -g: do not include global signal so will only have 8 nuisance variables to regress out from functional data (cannot use both -g and -T)"
    echo "  -T: ev info template file for nuisance variables (default: ${nuisanceTemplate})"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
saveOptions=""
while getopts ":N:D:g:T:${standardOptList}" Option; do
    case $Option in
        N ) gDIR_NUISANCE="$OPTARG"
            ;;
        D ) normResiduals=0
            ;;
        g ) nuisanceTemplate="$nuisanceTemplate_noGlobal"
            ;;
        T ) if [[ -e "$OPTARG" ]]; then
                nuisanceTemplate="$OPTARG"
            else
                zerror "Given file '$OPTARG' with nuisance ev info does not exist"
                exit -1
            fi
            ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"


###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"
