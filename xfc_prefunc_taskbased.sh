#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/Preprocess_func_taskbased.sh" # can override this file at command-line
afniDilate=1
hp=0.009
#lp=0.1

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <subject files>"
    echo "  $0 [OPTIONS] -S <subject list>"
    echo "Program:"
    echo "   Preprocessing of functional images for resting-state functional connectivity analysis"
    echo "Required Options:"
    echo "  * -S or -s"
    echo "Available Options:"
    echo "  -D: amount of voxels to dilate the skull stripped functional brain produced by afni (default: ${afniDilate})"
    echo "  -g: low pass filter in Hz (default: ${lp} Hz)"
    echo "  -G: high pass filter in Hz (default: ${hp} Hz)"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":D:g:G:${standardOptList}" Option; do    
    case $Option in
        D ) afniDilate="$OPTARG"
            ;;
        g ) lpSeconds="$OPTARG"
            ;;
        G ) hpSeconds="$OPTARG"
            ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))


###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"
