#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad, The Underpants Gnomes.

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/func_prep.sh" # can override this file at command-line

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <subject files>"
    echo "  $0 [OPTIONS] -S <subject list>"
    echo "Program:"
    echo "   Preparation of Multi-Echo functional images for calibration-based registration resting-state functional connectivity analysis"
    echo "Required Options:"
    echo "  * -S or -s"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":${standardOptList}" Option; do    
    case $Option in
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"


###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"
