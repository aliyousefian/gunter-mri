#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/20/08
# AUTHOR(S): Zarrar Shehzad

# Find and replace something in an fsf file and create a new fsf file

# Input: fsfTemplate
# Output: fsfOutput, fsfOutputDir
# $fsfFind, $fsfReplace -> escape these for safety

# Input: fsfTemplate
# Output: fsfOutput, fsfOutputDir
# $fsfFind, $fsfReplace

# skip output feat dir check, remove output feat dir
# force past output if exists (set to_force)

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/fsf_substitute.sh"
fsfTemplate=""
fsfOutput=""
fsfOutputDir=""
fsfFind=""
fsfReplace=""
feat_to_skip=0
feat_to_remove=0
to_force=0

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 -i <template fsf file> -f <find> -r <replace> -i <input  fsf file>"
    echo "Program:"
    echo "   Finds and replaces something in an fsf file and then checks the new file"
    echo "Required Options:"
    echo "  * -i, -f, -r"
    echo "Available Options:"
    echo "  -i: template fsf file (required)"
    echo "  -f: find (required)"
    echo "  -r: replace (required)"
    echo "  -o: new fsf file (optional), if not given then will find what you gave with -f option in template fsf file and replace it with what you gave with the -r option (e.g., x_fsfsubstitute.sh -f acc -r pcc -i acc.fsf -> this would create a new file called pcc.fsf)"
    echo "  -s: skip checking for output feat directory (in new fsf file)"
    echo "  -d: delete/remove output feat directory (in new fsf file) if exists"
    echo "  -F: force (will continue to run if new fsf file already exists)"
    echo "  -I: ignore confirmation from user when removing files (only applies when '-F' option has been selected)"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":i:f:r:o:FIsdc:" Option; do    
    case $Option in
        i ) fsfTemplate="$OPTARG"
            if [[ ! -e "$fsfTemplate" ]]; then
                zerror "Cannot find fsf template '${fsfTemplate}'!"
                exit -1
            fi
            ;;
        f ) fsfFind=$( x_sedEscapes.sh "$OPTARG" )
            ;;
        r ) fsfReplace=$( x_sedEscapes.sh "$OPTARG" )
            ;;
        o ) fsfOutput="$OPTARG"
            ;;
        F ) to_force=1
            ;;
        I ) to_confirm=""
            to_ignore=1
            ;;
        s ) feat_to_skip=1
            ;;
        d ) feat_to_remove=1
            ;;
        c ) set -- $OPTARG
            to_force="$1"
            feat_to_skip="$2"
            feat_to_remove="$3"
            ;;
    	: ) zerror "Argument value required for option '-$OPTARG'"
    	    exit -1
    	    ;;
    	? ) zerror "Unimplemented option '-$OPTARG' chosen"
    	    exit -1
    	    ;;
    	* ) zerror "Unknown error while processing options...continuing anyway"
    	    ;;
    esac
done

## Check/Set Output
if [[ -z "$fsfOutput" ]]; then
    ## Check that can find fsfFind in the template file name
    echo "$fsfTemplate" | grep -q "${fsfFind}"
    if [[ "$?" -ne 0 ]]; then
        zerror "Could not find '${fsfFind}' in the template filename. Either give a different filename or specify output filename with -o Option"
        exit -1
    else
        fsfOutput=$( echo "$fsfTemplate" | sed s/${fsfFind}/${fsfReplace}/  )
    fi
fi
fsfOutputDir=$( dirname $fsfOutput )

# Check input
if [[ -z "fsfTemplate" || -z "$fsfFind" || -z "$fsfReplace" ]]; then
    zerror "Required input, one of -i, -f, or -r is missing"
    usage
    exit -1
fi
# Next check if fsfFind is indeed in the file
grep -q "${fsfFind}" ${fsfTemplate}
if [[ "$?" -ne 0 ]]; then
    zerror "Could not find '${fsfFind}' in template file '${fsfTemplate}'"
    exit -1
fi

# RUN
intLoop "$commandFile"
