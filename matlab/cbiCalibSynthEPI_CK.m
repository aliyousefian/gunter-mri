function cbiCalibSynthEPI(rhoName, boName, rsName, TE, kyDir, synthName)
% Synthesize an EPI image at the desired TE from calibration data
% rhoName:
%    nifti file rho estimate from calibration scan
% boName:
%    nifti file B0 estimate from calibration scan
%    could be regularized
% rsName:
%    nifti file R2star estimate from calibration scan
%    could be regularized
% kyDir:
%     0 indicates setting B0=0, i.e. T2* only
%    +1 indicates conventional readout
%    -1 indicates reversed readout
% TE:
%    desired TE in units of echo spacing
%
% Note:
%   this program assumes that the EPI and the calibration scan have
%   the same acquisition protocol, i.e. FOV, resolution, echo-spacing, 
%   phase encode direction, etc.)

% Load the parameter estimates
%rho = read_avw(rhoName);
[err, rho, Info, ErrMessage] = BrikLoad (rhoName);
%rho = reshape(rho, Info.DATASET_DIMENSIONS(1), Info.DATASET_DIMENSIONS(2), Info.DATASET_DIMENSIONS(3));

%nfd = fopen(nfd,'read');
%[nfd, rho] = fread(nfd,nfd.nx*nfd.ny*nfd.nz);
%rho = reshape(rho, nfd.nx, nfd.ny, nfd.nz);
%nfd = fclose(nfd);

%bo = read_avw(boName);
[err, bo, Info, ErrMessage] = BrikLoad (boName);
%bo = reshape(bo, Info.DATASET_DIMENSIONS(1), Info.DATASET_DIMENSIONS(2), Info.DATASET_DIMENSIONS(3));

% nfd = fopen(nfd,'read');
% [nfd, bo] = fread(nfd,nfd.nx*nfd.ny*nfd.nz);
% bo = reshape(bo, nfd.nx, nfd.ny, nfd.nz);
% nfd = fclose(nfd);

%rs = read_avw(rsName);
[err, rs, Info, ErrMessage] = BrikLoad (rsName);
%rs = reshape(rs, Info.DATASET_DIMENSIONS(1), Info.DATASET_DIMENSIONS(2), Info.DATASET_DIMENSIONS(3));
% nfd = fopen(nfd,'read');
% [nfd, rs] = fread(nfd,nfd.nx*nfd.ny*nfd.nz);
% rs = reshape(rs, nfd.nx, nfd.ny, nfd.nz);
% nfd = fclose(nfd);

% switch the sign of B0 depending on the readout direction
switch kyDir
    case 0
        bo = 0.0*bo;
    case 1
        % do nothing
    case -1
        % reverse
        bo = -bo;
    otherwise
        % error
        error('kyDir should be 0, +1 or -1')
end

% Open the output file
% nfd = niftifile(synthName,nfd);
% nfd.descrip = 'NYU CBI synthetic EPI image';
% nfd = fopen(nfd,'write');

% Forward model and inverse
% see recon code for sign convention, etc.
% we're going to apply the matrices on the right
% The fourier operator
Ny = double(size(rho,2));
ky = (-Ny/2 : Ny/2-1)';
y  = (-Ny/2 : Ny/2-1)'/Ny;
zFy = zeros(Ny,Ny);
for n = 1:Ny
   zFy(:,n) = exp(2*pi*i*ky(n)*y(:));
end
% inverse is the adjoint
zPFy = zFy';
% forward is the integeral
zFy = 1/Ny * zFy;

% tStart
tStart = TE - Ny/2;

% Loop over the slices and process one at a time
epi = zeros(size(rho));
for nz = 1:size(epi,3),
    rhoslice = rho(:,:,nz);
    boslice  = bo(:,:,nz);
    rsslice  = rs(:,:,nz);
    data = zeros(size(epi,1),Ny);
    brfactor = exp(i*boslice - rsslice);
    %bors = exp(tStart*(i*boslice-rsslice));
    % ignore phase evolution up to begining of readout
    % this fixes a weird artifact in the synthetic data
    % if bo is not exactly right
    bors = exp(-tStart*rs(:,:,nz));
    for n = 1:Ny
        bors = bors.*brfactor;
        data(:,n) = (bors.*rhoslice)*zFy(:,n); % zFy was transposed!
    end
    epi(:,:,nz) = abs(data * zPFy);
end

% % Write to disk
% nfd = fwrite(nfd, single(abs(epi)), nfd.nx*nfd.ny*nfd.nz);
% fclose(nfd);

%save_nii(single(abs(epi)), synthName.nii)
Opt.Prefix=synthName
[err, ErrMessage, Info] = WriteBrik (single(abs(epi)), Info, Opt);

