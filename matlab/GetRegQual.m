%% GetReqQual
% function sombining the two scripts to assess the quality of the
% registration
%
% run at the end of nuisance
% output variables to put in QC doc

function GetRegQual(standard_brain_mask, reg_comp_image, standard_brain, subject_image)

%% assess quality of registration by overlaying images on standard brain
%% mask
[pc_vox_standard,pc_vox_subject]=assess_reg_qual(standard_brain_mask,reg_comp_image)

%% Calculate the spatial correlation between standard brain and subject
%% registered EPI
[Rbrain,Rmidline]=GetRegError(standard_brain, subject_image)

% save values in file
fid=fopen('reg_qual.txt','w');
fprintf(fid,'%3.2f\t',[pc_vox_standard]);
fprintf(fid,'%3.2f\t',[pc_vox_subject]);
fprintf(fid,'%3.2f\t',[Rbrain]);
fprintf(fid,'%3.2f\n',[Rmidline]);
fclose(fid);