%% Function to rudimentary assess the quality of registration
% it looks calculates the percentage of voxels in the standard brain mask
% that is overlapping with the subject image registered to standard space.
% The greater the percentage the better the overlap between the two images
% and hence the better the registration
%
% assumption: registration errors are mostly shifts or overstretching in
% one direction. An overall overstretching of the subject image would cause 
% it to overlap with 100% of the standard brain.  
%
% In reg_comp_image
% 1 = voxels specific to registered subject mask
% 2 = voxels specific to standard brain mask
% 3 = voxels overlapping between registered subject mask and standard brain mask

function [pc_vox_standard,pc_vox_subject]=assess_reg_qual(standard_brain_mask,reg_comp_image)

standard=read_avw(standard_brain_mask);
standard_mask=find(standard==1);

data=read_avw(reg_comp_image);

% calculate percentage of overlap between standard brain and subject
% registered image
pc_vox_standard= (size(standard(data==3),1) /  size(standard_mask,1)) *100;

% calculate number of voxels unique to subject image
% this will tell us how many voxels are outside of the standard mask
num_vox_sub = size(find(data==1),1);
% in percentage how many of this subject's voxels are not overlapping with
% the standard brain
pc_vox_subject= (num_vox_sub / size([find(data==1);find(data==3)],1)) *100;

% save values in file
fid=fopen('assess_reg_qual_pc.txt','w');
fprintf(fid,'%3.2f\t',[pc_vox_standard]);
fprintf(fid,'%3.2f',[pc_vox_subject]);
fclose(fid);






