%% GetRegError
% function to calculate spatial correlation between subject registered
% brain and standard brain

function [Rbrain,Rmidline]=GetRegError(standard, subject_image)

%% load standard brain and get brain voxels
[nii_t1 dims] = read_avw(standard);
t1 = reshape(nii_t1, prod(dims(1:3)), 1);
brain = find(t1 > 0); 
t1_brain = t1(brain);

%% determine center voxels and get standard brain voxels for midline +- 5 voxels
xcenter = (dims(1) + 1) / 2;
mid = xcenter-5:xcenter+5;
nii_t1mid = nii_t1(mid,:,:);
dims_mid = size(nii_t1mid);
t1mid = reshape(nii_t1mid,prod(dims_mid(1:3)),1);
midline = find(t1mid > 0);
t1_midline = t1mid(midline);

%% load subject brain and get brain voxels
[nii_epi, dims] = read_avw(subject_image);
epi = reshape(nii_epi, prod(dims(1:3)), 1);
epi_brain = epi(brain) ;
% get midline brain voxels
nii_epimid = nii_epi(mid,:,:);
epimid = reshape(nii_epimid,prod(dims_mid(1:3)),1);
epi_midline = epimid(midline) ;

%% Compute spatial correlation
Rbrain = IPN_ColCorr(epi_brain, t1_brain);
Rmidline = IPN_ColCorr(epi_midline, t1_midline);
