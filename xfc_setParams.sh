# Allow you to change the standard options at once (export ...)
# Also allows you to change some additional settings not found in standard

#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 01/05/09
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
configFile=""

# USAGE
function usage {
    changegood green
    echo
    echo "Program:"
    echo "   Sets general parameters and saves them in a config file. You can call this file directly in the command-line using source or through the -c option with each script."
    echo "Options:"
    echo "  -s: save the settings into a config file, must specify the filename (required)"
    echo "  -v: # of volumes; the number of time points in your functional image [default: ${gVOLNUM}]"
    echo "  -t: TR; the TR of your functional image [default: ${gTR}]"
    echo "  -F: FWHM; amount of smoothing (mm) for your functional image [default: ${gFWHM}]"
    echo "  -B: base directory; directory in which all of your preprocessed subject data can be found [default: ${gDIR_SUBDATA}]"
    echo "  -P: projects directory; directory in which all of your stats/poststats can be found (e.g. individual and group-level analyses) [default: ${gDIR_PROJECTS}]"
	echo "  -a: mprage; BASE name of T1 scan, with skull (default: ${gFN_MPRAGEBASE}) "
	echo "  -b: brain; BASE name of subject's skull-stripped anatomical image (default: ${gFN_BRAINBASE})"
	echo "  -A: anatomical directory; name of directory in which subject's anatomical data is kept (default: ${gDIR_ANAT})"
	echo "  -r: functional file base; base filename for all functional files (default: ${gFN_FUNCBASE})"
	echo "  -R: functional directory; directory in which all your functional data is kept (default: ${gDIR_FUNC})"
	echo "  -w: standard image; filename of your standard image (default: ${gFN_STANDARD})"
	echo "  -h: displays this help"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
declare -a saveVar
declare -a saveVal
while getopts "s:v:t:F:B:P:a:b:A:r:R:w:h" Option; do    
    case $Option in
        s ) if [[ -e "$OPTARG" ]]; then
                zerror "Config file '${OPTARG}' already exists. You can remove it with 'rm ${OPTARG}'."
                exit -1
            else
                configFile="$OPTARG"
            fi
            ;;
        v ) saveVar[${#saveVar[*]}]="gVOLNUM"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        t ) saveVar[${#saveVar[*]}]="gTR"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        F ) saveVar[${#saveVar[*]}]="gFWHM"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        B ) if [[ -d "$OPTARG" ]]; then
                saveVar[${#saveVar[*]}]="gDIR_SUBDATA"
                saveVal[${#saveVal[*]}]="$OPTARG"
            else
                zerror "Could not find base directory (-B) '${OPTARG}'!"
                exit -1
            fi
            ;;
        P ) if [[ -d "$OPTARG" ]]; then
                saveVar[${#saveVar[*]}]="gDIR_PROJECTS"
                saveVal[${#saveVal[*]}]="$OPTARG"
            else
                zerror "Could not find projects directory (-P) '${OPTARG}'!"
                exit -1
            fi
            ;;
        a ) saveVar[${#saveVar[*]}]="gFN_MPRAGEBASE"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        b ) saveVar[${#saveVar[*]}]="gFN_BRAINBASE"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        A ) saveVar[${#saveVar[*]}]="gDIR_ANAT"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        r ) saveVar[${#saveVar[*]}]="gFN_FUNCBASE"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        R ) saveVar[${#saveVar[*]}]="gDIR_FUNC"
            saveVal[${#saveVal[*]}]="$OPTARG"
            ;;
        w ) if [[ -e "$OPTARG" ]]; then
                saveVar[${#saveVar[*]}]="gFN_STANDARD"
                saveVal[${#saveVal[*]}]="$OPTARG"
            else
                zerror "Could not find custom standard image"
                exit -1
            fi
            ;;
        h ) usage
            exit -1
            ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# Check
if [[ -z "$configFile" ]]; then
    zerror "You must provide the output file using -s"
    exit -1
fi

# Save a config file
zinfo "Creating a config file just for you"
## Create file
zcmd "touch ${configFile}"
## Add Info
for (( i = 0; i < ${#saveVar[@]}; i++ )); do
    zcmd "echo \"${saveVar[$i]}=${saveVal[$i]}\" >> ${configFile}"
done

