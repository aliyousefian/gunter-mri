#!/bin/bash

. ${GUNTHERDIR}/include/Asourceall.sh

# This script will create a png picture of slices of the brain

input=""
output=""

colour_type=1
is_integer=0

slices_x=6
slices_y=5
scale=1

to_force=0

function die {
    echo "$@"
    exit 1
}

function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] INPUT"
    echo "  $0 [OPTIONS] INPUT OUTPUT"
    echo "  $0 [OPTIONS] -b BACKGROUND INPUT OUTPUT"
    echo "  $0 [OPTIONS] -x 3 -y 2 INPUT OUTPUT"
    echo "Program:"
    echo "   Will create a png image with slices of your desired brain. If OUTPUT is not specified will autoset to INPUT.png. Please see fsl programs overlay and slicer for more details."
	echo "Options:"
	echo "  -x: number of slices in x (horizontal) direction (default: ${slices_x})"
	echo "  -y: number of slices in y (vertical) direction (default: ${slices_y})"
	echo "  -s: scale or size of each slice (default: ${scale})"
	echo "  -b: background image (If you specify a background image, the input image will first be overlaid on top of the background and then slices of that will be taken.)"
	echo "  -c: make overlay solid (only used if -b specified)"
	echo "  -i: make output of overlay an integer (only used if -b specified)"
	echo "  -f: if output exists, overwrite (default is not to overwrite)"
	echo "  -h: this help message"
    echo
    changebad
}

while getopts ":x:y:s:b:cif" Option; do    
    case $Option in
		x ) slices_x=$OPTARG
		    ;;
		y ) slices_y=$OPTARG
		    ;;
		s ) scale=$OPTARG
		    ;;
		b ) background=$OPTARG
		    [[ ! -e $background ]] && die "Background image '${background}' does not exist"
		    ;;
		c ) colour_type=1
		    ;;
		i ) is_integer=1
		    ;;
		f ) to_force=1
		    ;;
		h ) usage
		    exit 1
			;;
        * ) zerror "-$Option not recognized"
            usage
            exit 1
            ;;
    esac
done
shift $((${OPTIND}-1))

if [[ "$#" -ne 1 && "$#" -ne 2 ]]; then
    usage
    exit 1
fi

input="$1"
output="${2:-${input%.*}}"
output="${output%.*}.png"

[ ! -e "$input" ] && die "Input '${input}' does not exist"
[[ -e "$output" && to_force -eq 0 ]] && die "Output '${output}' already exists"

if [[ ! -z "$background" ]]; then
    tmp_overlay=$(mktemp)
    rm "$tmp_overlay"
    tmp_overlay="${tmp_overlay}.${gOUTPUT_TYPE}"

    min_vox_val=$(3dBrickStat -non-zero -min $input)
    max_vox_val=$(3dBrickStat -non-zero -max $input)
    zcmd "overlay $colour_type $is_integer $background -a $input $min_vox_val $max_vox_val ${tmp_overlay}"
    
    input="${tmp_overlay}"
fi

dim_x=$(fslinfo ${input} | grep ^dim1 | awk '{print $2}')
dim_y=$(fslinfo ${input} | grep ^dim2 | awk '{print $2}')

zcmd "slicer $input -s $scale -S $(( $dim_y / ( $slices_x * $slices_y ) )) $(( $dim_x * $slices_x * $scale )) $output"

if [[ ! -z "$background" ]]; then
    zcmd "rm ${tmp_overlay}"
fi
