#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/21/08
# AUTHOR(S): Zarrar Shehzad

# This script will get a setting in the fsf file

setting="$1"
fsfFile="$2"

eval echo $( grep "${setting}" ${fsfFile} | awk '{print $3}' )
