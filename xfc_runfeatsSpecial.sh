#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/21/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/run_featsSPECIAL.sh" # can override this file at command-line
templateFile=""
templateSubject=""
newFsfName=""
copy_mat=0
matDir=""
removeFilteredFunc=1
run_feat=1 # Default is to run the feat

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -T template.fsf -j subjectTemplate -c configFile -s <subject files>"
    echo "  $0 [OPTIONS] -T template.fsf -j subjectTemplate -c configFile -S <subject list>"
    echo "Program:"
    echo "   Will batch process a specific feat for all participants"
    echo "Required Options:"
    echo "  * -T, -j, -c"
    echo "Notes: "
    echo "  * By default the program will check the output feat directory for each subject and if it exists then it will not run feat for that subject"
    echo "  * To overwrite a feat directory that already exits, you can use the -f option"
    echo "  * To skip checking for the output feat directory, you can use the -z option (in this case if the feat directory does exists fsl will add a + to the end of the directory name and continue to run)"
    echo "  * the -z and -f options are mutually exclusive"
    echo "Available Options:"
    echo "  -T: template filename"
    echo "  -j: template subject (what to find and replace)"
    echo "  -c: config file (containing the location of the project directory)"    
    echo "  -Ov: default number volumes (in template)"
    echo "  -Nv: required number of volumes"
    echo "  -n: new fsf filename (default: template filename)"
    echo "  -e: Directory in which all the feats for a subject can be found (default: ${gDIR_FEATS})"
    echo "  -E: Directory within the feats directory in which all fsf files are stored (default: ${gDIR_FSF})"
    echo "  -m: Copy mat files for all subject into this directory (by default the program will not copy mat files)"
    echo "  -M: only run feat model (default: run both feat_model + feat)"
    echo "  -D: DO NOT remove the filtered_func_data.nii.gz file generated in the feat directory"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":T:j:c:Ov:Nv:n:e:E:m:MD${standardOptList}" Option; do    
    case $Option in
        T ) templateFile="$OPTARG"
            if [[ ! -e "$templateFile" ]]; then
                zerror "Input template file '${templateFile}' does not exist"
                exit -1
            fi
            ;;
        j ) templateSubject="$OPTARG"
            ;;
	c ) SubConfigFile="$OPTARG"
            ;;
	Ov) DEFnumVols="$OPTARG"	
	    ;;
	Nv) numVols="$OPTARG"    
	    ;;
        n ) newFsfName="${OPTARG%.fsf}"
            ;;
        e ) gDIR_FEATS="$OPTARG"
            ;;
        E ) gDIR_FSF="$OPTARG"
            ;;
        m ) copy_mat=1
            matDir="$OPTARG"
            ;;
        M ) run_feat=0
            ;;
        D ) removeFilteredFunc=0
            ;;
    	h ) usage 3
    	    exit 1
    	    ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# Checks + Settings
if [[ -z "$templateFile" || -z "$templateSubject" ]]; then
    zerror "Required parameters -T or -j not given"
    exit -1
fi
if [[ -z "$newFsfName" ]]; then
    newFsfName=$( basename ${templateFile%.fsf} )
fi

###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$SubConfigFile" "$commandFile"
