#!/usr/bin/env bash

# REGISTRATION SCRIPT
# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/16/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/reg_calib.sh"
cost_func2highres="corratio"
cost_highres2standard="corratio"
linReg=1
nonlinReg=0
commandFile_nonlin="${GUNTHERDIR}/commands/reg_nonlin_calib.sh"

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <subject files>"
    echo "  $0 [OPTIONS] -S <subject list>"
    echo "Program:"
    echo "   Registration of preprocessed functional images"
    echo "Required Options:"
    echo "  * -S or -s | If nonlinear registration has not already been run, you should also specify -n"
    echo "Available Options:"
    echo "  -n: nonlinear registration; run nonlinear registration after running linear registration"
    echo "  -N: nonlinear registration; only run nonlinear registration (by default the program will run LINEAR registration, which MUST BE RUN FIRST)"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":o:O:nN${standardOptList}" Option; do    
    case $Option in
    	n ) nonlinReg=1
    	    ;;
    	N ) linReg=0
    	    nonlinReg=1
    	    ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"


###
# BEGIN PROCESSING
#
## see loops.sh
## linear registration
if [[ "$linReg" == 1 ]]; then
    subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"
fi
## nonlinear registration
if [[ "$nonlinReg" == 1 ]]; then
    subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile_nonlin"
fi
