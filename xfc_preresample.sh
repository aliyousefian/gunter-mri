#!/usr/bin/env bash

# REGISTRATION SCRIPT
# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/16/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/resample_lin.sh" # can override this file at command-line

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <subject files>"
    echo "  $0 [OPTIONS] -S <subject list>"
    echo "Program:"
    echo "   Registration of preprocessed functional images"
    echo "Required Options:"
    echo "  * -S or -s"
    echo "Available Options:"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":${standardOptList}" Option; do    
    case $Option in
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"


###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"



# 1. a) Do the registration steps first! (test out 3dAllineate or whatever the newest afni thing is to register from epi->t1)
# b) then do the registration of the actual functional -> standard

# 2. a) segmentation and extraction of nuisance covariates
    ## find out why we do step for segmentation
    
# can also do step here where get residuals following nuisance covariates

# 3. your done!



