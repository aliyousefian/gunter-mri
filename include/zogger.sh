#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

declare LOG_FILE

# Constants for logging level (what to output for message)
declare -r Z_IMPORTANT=-2
declare -r Z_TITLE=-1
declare -r Z_EMERG=1
declare -r Z_ERROR=2
declare -r Z_WARN=3
declare -r Z_NOTICE=4
declare -r Z_INFO=5
declare -r Z_COMMAND=6
declare -r Z_COMMAND_INFO=61
declare -r Z_COMMAND_EXEC=62
declare -r Z_DEBUG=7
# Save Emergencies and Errors and Warnings
declare -a zsaveProblems
# LOG LEVELS (what to save into log file):
#   1 - save info+
#   2 - save notice+
#   3 - save commands+
#   4 - save title+
#   5 - save warn+
#   6 - save error+

function zogger {
    local default_message="NO MESSAGE GIVEN"

    local message=${1:-$default_message}
    local level=${2:-$E_INFO}

    case $level in
        $Z_COMMAND )
            if [[ ! -z "$LOG_FILE" && "$LOG_LEVEL" < 4 ]]; then
                echo "X: $message" >> "$LOG_FILE"
            fi
            cmdecho "X: $message" green
            eval "$message"
            return "$?"
            ;;
        $Z_COMMAND_INFO )
            if [[ ! -z "$LOG_FILE" && "$LOG_LEVEL" < 4 ]]; then
                echo "X: $message" >> "$LOG_FILE"
            fi
            cmdecho "X: $message" green
            ;;
        $Z_COMMAND_EXEC )
            eval "$message"
            return "$?"
            ;;
        $Z_TITLE )
            cecho "${message}" white blue
            if [[ ! -z "$LOG_FILE" && "$LOG_LEVEL" < 5 ]]; then
                echo "TITLE: $message" >> "$LOG_FILE"
            fi
            ;;
		$Z_IMPORTANT )
			cecho "${message}" black green
			if [[ ! -z "$LOG_FILE" ]]; then
                echo "IMPORTANT: $message" >> "$LOG_FILE"
            fi
			;;
        $Z_DEBUG )
            cecho "-DEBUG- ${message}" cyan underline >&2
            ;;
        $Z_INFO )
            cecho "${message}" cyan
            if [[ ! -z "$LOG_FILE" && "$LOG_LEVEL" < 2 ]]; then
                echo "INFO: $message" >> "$LOG_FILE"
            fi
            ;;
        $Z_NOTICE )
            cecho "*NOTICE* ${message}" magenta >&2
            if [[ ! -z "$LOG_FILE" && "$LOG_LEVEL" < 3 ]]; then
                echo "NOTICE: $message" >> "$LOG_FILE"
            fi
            ;;
        $Z_WARN )
            cecho "*WARNING* ${message}" red >&2
            zsaveProblems[${#zsaveProblems[*]}]="WARNING: ${message}"
            if [[ ! -z "$LOG_FILE" && "$LOG_LEVEL" < 6 ]]; then
                echo "WARNING: $message" >> "$LOG_FILE"
            fi
            ;;
        $Z_ERROR )
            cecho "*ERROR* ${message}" white red >&2
            zsaveProblems[${#zsaveProblems[*]}]="ERROR: ${message}"
            if [[ ! -z "$LOG_FILE" ]]; then
                echo "ERROR: $message" >> "$LOG_FILE"
            fi
            ;;
        $Z_EMERG )
            cecho "*EMERGENCY* ${message}" white red >&2
            zsaveProblems[${#zsaveProblems[*]}]="EMERGENCY: ${message}"
            if [[ ! -z "$LOG_FILE" ]]; then
                echo "EMERGENCY: $message" >> "$LOG_FILE"
            fi
            ;;
        * )
            echo "${message}"
            ;;
    esac
}

function zdebug {
    zogger "$1" "$Z_DEBUG"
}

function zinfo {
    zogger "$1" "$Z_INFO"
}

function zwarn {
    zogger "$1" "$Z_WARN"
}

function znotice {
    zogger "$1" "$Z_NOTICE"
}

function zerror {
    zogger "$1" "$Z_ERROR"
}

function zemerg {
    zogger "$1" "$Z_EMERG"
}

function ztitle {
    zogger "$1" "$Z_TITLE"
}

function zcmd {
    zogger "$1" "$Z_COMMAND"
}

function zcmd_info {
    zogger "$1" "$Z_COMMAND_INFO"
}

function zcmd_exec {
    zogger "$1" "$Z_COMMAND_EXEC"
}

function zimportant {
	zogger "$1" "$Z_IMPORTANT"
}

function zdisplayProblems {
    numProblems=${#zsaveProblems[*]}
    if [[ "$numProblems" != 0 ]]; then
        zimportant "You had ${numProblems} errors/warnings"
        for (( i = 0; i < ${numProblems}; i++ )); do
            zimportant "${zsaveProblems[$i]}"
        done
    fi
}
