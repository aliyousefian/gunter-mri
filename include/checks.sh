#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad


############################
# FUNCTIONS TO CHECK STUFF #
############################

# Info:
#   To check a file that is required
# Usage:
#   checkReqFile <filename>
# Return:
#   0 -> File exists and is not empty
# Exit:
#   -1 -> File does not exist or is empty
#   -2 -> No input
# Notes:
#   * filename(s) must be in quotes
#   * for multipe filenames just seperate with a space
function checkReqFile {
    local files="$1"
    
    if [[ -z "$files" ]]; then
        zemerg "No filename given, nothing to check!"
        exit -2
    else
        for file in ${files}; do
            if [[ ! -s "$file" || ! -e "$file" ]]; then
                zerror "File '${file}' does not exist or size is zero"
                exit -1
            fi
        done
    fi
    
    return 0
}

# Info:
#   To check a file that will be used as an input for a command
# Usage:
#   checkInput "<filename(s)>"
# Return:
#   1 -> File is empty or File does not exist
#   0 -> otherwise
# Exit:
#   -2 -> No input
# Notes:
#   * filename(s) must be in quotes
#   * for multipe filenames just seperate with a space
function checkInput {
    local inputs="$1"
    
    # No filename given
    if [[ -z "$inputs" ]]; then
        zemerg "Input filename given is empty, no file to check!"
        exit -2
    else
        for input in ${inputs}; do
            # File does not exist
            if [[ ! -e "$input" ]]; then
                zerror "Input file '${input}' does not exist"
                return 1
            fi
        done
    fi
    
    return 0
}

# Info:
#   To check a file that is the output of a command
# Usage:
#   checkOutput "<filename(s)>" [force (optional)] [confirm (optional)]
# Options:
#   filename: output filename
#   force: overwrite output if exists; 0=no, 1=yes
#   confirm: if force=1, then confirm removing of output that exists; 0=no, "-i "=yes
# Return:
#   1 -> Output file already exists
#   0 -> Output file does not exist or File exists and to_force=1 (i.e., will overwrite file)
# Exit:
#   -2 -> No input
# Note:
#   * uses global variables for 'force' and 'confirm' if those aren't given
#   * filename(s) must be in quotes
#   * for multipe filenames just seperate with a space
function checkOutput {
    local outputs="$1"
    local to_force="${2:-$to_force}"
    
    # No filename given
    if [[ -z "$outputs" ]]; then
        zemerg "Output filename given is empty, no file to check!"
        exit -2
    else
        for output in ${outputs}; do
            # File already exists
            if [[ -e "$output" ]]; then
                if [[ "$to_force" = 1 ]]; then
                    znotice "Will overwrite existing file '${output}'"
                    zcmd "rm ${output}"
                else
                    zerror "Output file '${output}' already exists...skipping command"
                    return 1
                fi
            fi
        done
    fi

    
    return 0
}

# TODO: add documentation
function checkDir {
    local dirs="$1"
    
    # Directory not given given
    if [[ -z "$dirs" ]]; then
        zemerg "Input given is empty, no directories to check!"
        exit -2
    else
        for dir in ${dires}; do
            # File does not exist
            if [[ ! -d "$dir" ]]; then
                zerror "Directory '${dir}' does not exist"
                return 1
            fi
        done
    fi
    
    return 0
}
