#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad


# REQUIRES $GUNTHERDIR/config/general.sh

# STANDARD OPTIONS

## Variables
subtype=0
to_force=0
dry_run=0
to_skip=0


## Usage

defaultUsage=2
function standardUsage {
    local level=${1:-${defaultUsage}} # Spit out limited set or complete set
    
    # Show what is needed and seen all of the time first
    echo "  -B: base directory; directory in which all of your preprocessed subject data can be found [default: ${gDIR_SUBDATA}]"
    echo "  -d: dry run (while this will not execute anything if you have '-f' selected then it can lead to files being removed)"
    echo "  -f: force; will overwrite old files (default behavior is to skip files that have been run)"
    echo "  -l: log file"
    echo "  -L: log level; detail of logging into file...ask about details"
    echo "  -Z: skip checking for output at the very beginning"
    
    # Show usage with subjects
    if [[ "$level" = 1 || "$level" = 3 ]]; then
        echo "  -s: subjects; file with a list of your subjects will be given as the last argument (it will take each subject and replace it with whatever is defined in the -f option)"
    	echo "  -S: subject; a list of subjects will be given at the end of the command (it will take each subject and replace it with whatever is defined in the -f option)"
    fi
    
    # Show complete usage
    if [[ "$level" = 2 || "$level" = 3 ]]; then
        echo "  -v: # of volumes; the number of time points in your functional image [default: ${gVOLNUM}]"
        echo "  -t: TR; the TR of your functional image [default: ${gTR}]"
        echo "  -F: FWHM; amount of smoothing (mm) for your functional image [default: ${gFWHM}]"
        echo "  -P: projects directory; directory in which all of your stats/poststats can be found (e.g. group-level analyses) [default: ${gDIR_PROJECTS}]"
    	echo "  -a: mprage; BASE name of T1 scan, with skull (default: ${gFN_MPRAGEBASE}) "
    	echo "  -b: brain; BASE name of subject's skull-stripped anatomical image (default: ${gFN_BRAINBASE})"
    	echo "  -A: anatomical directory; name of directory in which subject's anatomical data is kept (default: ${gDIR_ANAT})"
    	echo "  -r: functional file base; base filename for all functional files (default: ${gFN_FUNCBASE})"
    	echo "  -R: functional directory; directory in which all your functional data is kept (default: ${gDIR_FUNC})"
    	echo "  -k: name (without extension) of normalized residual file in functional space (default: ${gFN_FUNCRES%.${gOUTPUT_TYPE}})"
    	echo "  -K: name (without extension) of normalized residual file in standard space (default: ${gFN_FUNCRES2STANDARD%.${gOUTPUT_TYPE}})"
    	echo "  -w standard image; filename of your standard image (default: ${gFN_STANDARD})"
    	echo "  -c: general config file; change variables found in 'z_config.sh' with a custom configuration file (note this doesn't replace the default configuration but overwrites any variables)"
    	echo "  -C: subject config file; REPLACE default variables found in 'z_configSubject.sh' with custom file"
    	echo "  -X: command config file; REPLACE default commands found in this programs specific command config file (e.g. z_configPrefunc.sh)"
    	echo "  -y: command number to start with (default: 1)"
    	echo "  -Y: command number to end with (default: last commmand whatever that is)"
    	echo "  -h: display this help message"
    else
        echo "  -h: display more detailed help message"
    fi
    
}



standardOptList="sSdfhl:L:Zv:t:F:B:P:a:b:A:r:R:k:K:w:c:C:X:y:Y:"
saveOptions=""
function standardOpts {
    local Option="$1"
    local OPTARG="$2"
        
    case $Option in
        s ) subtype=2
            ;;
        S ) subtype=1
            ;;
        d ) dry_run=1
            # saveOptions
            ;;
    	f ) to_force=1
    	    # saveOptions
    	    ;;
    	l ) gLOG_FILE="$OPTARG"
    	    touch "${LOG_FILE}"
    	    saveOptions="-${Option} \"${OPTARG}\""
    	    ;;
    	L ) gLOG_LEVEL="$OPTARG"
    	    saveOptions="-${Option} \"${OPTARG}\""
    	    ;;
    	Z ) to_skip=1
    	    # saveOptions
            ;;
    	h ) usage 3
    	    exit 1
    	    ;;
        v ) gVOLNUM="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        t ) gTR="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        F ) gFWHM="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        B ) if [[ -d "$OPTARG" ]]; then
                gDIR_SUBDATA="$OPTARG"
            else
                zerror "Could not find base directory (-B) '${OPTARG}'!"
                exit -1
            fi
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        P ) if [[ -d "$OPTARG" ]]; then
                gDIR_PROJECTS="$OPTARG"
            else
                zerror "Could not find projects directory (-P) '${OPTARG}'!"
                exit -1
            fi
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        a ) gFN_MPRAGEBASE="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        b ) gFN_BRAINBASE="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        A ) gDIR_ANAT="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        r ) gFN_FUNCBASE="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        R ) gDIR_FUNC="$OPTARG"
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        k ) gFN_FUNCRES="${OPTARG%.${gOUTPUT_TYPE}}.${gOUTPUT_TYPE}"
            ;;
        K ) gFN_FUNCRES2STANDARD="${OPTARG%.${gOUTPUT_TYPE}}.${gOUTPUT_TYPE}"
            ;;
        w ) if [[ -e "$OPTARG" ]]; then
                gFN_STANDARD="$OPTARG"
            else
                zerror "Could not find custom standard image"
                exit -1
            fi
            saveOptions="-${Option} \"${OPTARG}\""
            ;;
        c ) configFile="$OPTARG"
            # saveOptions
            echo "Loading configuration file '${configFile}'..."
    		if [[ ! -e "$configFile" ]]; then
    		    zerror "Could not find general configuration file '${configFile}'!"
    			exit -1
    		fi
    		source "$configFile"
    		;;
    	C ) subConfigFile="$OPTARG"
    	    # saveOptions
    	    if [[ ! -e "$subConfigFile" ]]; then
    	       zerror "Could not find subject configuration file '${subConfigFile}'!"
    	       exit -1
    	    fi
    	    ;;
    	X ) commandFile="$OPTARG"
    	    if [[ ! -e "$commandFile" ]]; then
    	       zerror "Could not find command configuration file '${commandConfigFile}'"
    	       exit -1
    	    fi
            ;;
        y ) commandStart="$OPTARG"
            ;;
        Y ) commandEnd="$OPTARG"
            ;;
    	: ) zerror "Argument value required for option '-$OPTARG'"
    	    exit -1
    	    ;;
    	? ) zerror "Unimplemented option '-$OPTARG' chosen"
    	    exit -1
    	    ;;
    	* ) zerror "Unknown error while processing options...continuing anyway"
    	    ;;
    esac

}
