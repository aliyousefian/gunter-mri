#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

# requires checks.sh

####################
# HELPER FUNCTIONS #
####################

# Hold List of Skipped Participants (for function: skipSubject)
declare -a skippedSubjects
# List of subjects
declare -a subjects


# USAGE
## processSubList
##  <type of list given (1=list of subjects; 2=list of subject files)
##  <list (can contain one of the two options above)>
# EXAMPLE
##  processSubList 1 "sub8010 sub8020 sub8050"
##  processSubList 2 "sublist.txt sublist2.txt"
function procesSubList {
    # VARIABLES
    ## User Variables
    local subType="$1"
    local list="$2"
    ## Function Variables
    local file=""
    local subject=""
    
    # CHECKS
    ## need 2 arguments
    if [[ "$#" -ne 2 ]]; then
        zemerg "Function 'processSubList' only accepts 2 arguments; $# arguments given!"
        exit -2
    fi
    ## need to have subjects
    if [[ -z "$list" ]]; then
        zerror "Subject list is empty, please give me some subjects!"
        exit -2
    fi
    
    # PROCESS
    case "$subType" in
        ## List of Subjects
        1 ) zinfo "reading list of subjects given by user"
            subjects=( ${list} )
            ;;
        ## List of Files
        2 ) for file in ${list}; do
                zinfo "reading list of subjects in $file"
                subjects=( ${subjects[*]} `cat ${file}` )
            done
            ;;
        ## Whoops
        * ) zemerg "Incorrect subject list type given. Must be 1 (list of subjects) or 2 (list of files with subjects listed in them)"
            exit -2
            ;;
    esac
    
    if [[ -z "$subjects" ]]; then
        zerror "Cannot process data, no subject information"
        exit -1
    else
        zinfo "Running subjects: ${subjects[*]}"
        echo
        return 0
    fi
    
}

# Add subjects who have an error to this list
function skipSubject {
	skippedSubjects[${#skippedSubjects[*]}]="$1"
	return 0
}
