#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

source "$GUNTHERDIR/include/colorecho.sh"
source "$GUNTHERDIR/include/zogger.sh"
source "$GUNTHERDIR/include/options.sh"
source "$GUNTHERDIR/include/subjects.sh"
source "$GUNTHERDIR/include/checks.sh"
source "$GUNTHERDIR/include/commands.sh"
source "$GUNTHERDIR/include/loops.sh"
