#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad


# FROM:
# program: y_colorecho.sh
# author: Zarrar
# date: 11/07/07
#
# description: 
#	will echo text messages in color and/or in bold and/or with an underline
# 	only to be used from another script
# usage:
#	cecho <message> bold
#	cecho <message> underline
#	cecho <message> text-color
#	cecho <message> text-color background-color
#	cecho <message> bold underline text-color background-color
#	cecho <message> text-color bold background-color
#	cecho <message> text-color background-color bold underline
# notes: 
#   * can mix and match the 4 options, however text-color must come before background color
#	* color options: black, red, green, yellow, blue, magenta, cyan, white
#	* if you omit anything or give trash it will revert to defaults:
#		- omit bold: text will not be bold
#       - omit underline: text will not be underlined
#		- omit text-color: text will be default set in z_config.ini
#		- omit back-color: text will be default set in z_config.ini
#


#######################
# Important Variables #
######################

# Default values
default_bold=0
default_underline=24
default_text=39
default_background=49

# Main values (set as defaults by default, how convenient)
bold=$default_bold
underline=$default_underline
fore=$default_text
back=$default_background

# Value for bold and not bold
text_bold=1
text_nobold=0

# Value for underline or not underline
text_underline=4
text_nounderline=24

# Inverse the text
text_inverse=7

# Values for foreground color
text_black='30'
text_red='31'
text_green='32'
text_yellow='33'
text_blue='34'
text_magenta='35'
text_cyan='36'
text_white='37'

# Values for background color
background_black='40'
background_red='41'
background_green='42'
background_yellow='43'
background_blue='44'
background_magenta='45'
background_cyan='46'
background_white='47'
# if nothing
empty=''
null='NULL'


#####################
# Helper Functions #
####################

setbold()
{
	set_bold=${1}
	
	case ${set_bold} in
        'bold'	) bold=$text_bold	;;
        *   	) bold=$text_nobold	;;
    esac
}

setunder()
{
	set_underline=${1}
	
	case ${set_underline} in
		'underline'	) underline=$text_underline		;;
		*			) underline=$text_nounderline	;;
	esac
}

setfore()
{
	set_foreground=${1}
	
	case ${set_foreground} in
        'black'     ) fore=$text_black		;;
        'red'       ) fore=$text_red		;;
        'green'     ) fore=$text_green		;;
        'yellow'    ) fore=$text_yellow		;;
        'blue'		) fore=$text_blue		;;
        'magenta'	) fore=$text_magenta	;;
        'cyan'		) fore=$text_cyan		;;
        'white'		) fore=$text_white		;;
        *			) fore=$default_text	;;
    esac

	return
}

setback()
{
	set_background=${1}

	case ${set_background} in
        'black'     ) back=$background_black		;;
        'red'       ) back=$background_red			;;
        'green'     ) back=$background_green		;;
        'yellow'    ) back=$background_yellow		;;
        'blue'		) back=$background_blue			;;
        'magenta'	) back=$background_magenta		;;
        'cyan'		) back=$background_cyan			;;
        'white'		) back=$background_white		;;
        *			) back=$default_background		;;
    esac
	
	return
}

helper_color()
{
    local isset_fore=0

	while [[ "$#" -ne 0 ]]; do
		case $1 in
			'bold' )
				setbold $1
				;;
			'underline' )
				setunder $1
				;;
			* )
				if [[ "$isset_fore" = 0 ]]; then
					setfore $1
					local isset_fore=1
				else
					setback $1
				fi
				;;
		esac
		
		shift
	done
	
	return
}

# Function: cecho
# Argument $1 = message
# Argument $2 = text color
# Argument $3 = background color
# Argument $4 = bold
# Argument $5 = underline
# NOTE: order of arguments is interchangeable, please see usage
cecho()
{
    local default_msg="No message passed"
    local end_line=1

    # check for no newline option
    while getopts ":n" cecho_options; do
    	case $cecho_options in
    	    n ) end_line=0
    	        shift
    	        ;;
    	esac
    done
    # set args
    local color_message=${1:-$default_msg}
	shift

	if [[ "$#" -lt 1 ]]; then
		echo -e -n "\E[0m"
		echo "${color_message}"
		return
	else
	  # Call function to process args
      helper_color $@  
	fi
	
	echo -e -n "\E[${bold};${underline};${fore};${back}m"
    echo -n "${color_message}"
	echo -e -n "\E[0m"
	if [[ "$end_line" = 1 ]]; then
	   echo
	fi
    
	# Reset everything
	bold=$default_bold
	underline=$default_underline
	fore=$default_text
	back=$default_background
	
	OPTIND=0
	
    return
}

# special echo for commands, using eval echo ...
cmdecho()
{
    local default_msg="No message passed"
    local end_line=1

    # check for no newline option
    while getopts ":n" cecho_options; do
    	case $cecho_options in
    	    n ) end_line=0
    	        shift
    	        ;;
    	esac
    done
    # set args
    local color_message=${1:-$default_msg}
	shift

	if [[ "$#" -lt 1 ]]; then
		echo -e -n "\E[0m"
		echo "${color_message}"
		return
	else
	  # Call function to process args
      helper_color $@  
	fi
	
	echo -e -n "\E[${bold};${underline};${fore};${back}m"
    echo -n "${color_message}"
	echo -e -n "\E[0m"
	if [[ "$end_line" = 1 ]]; then
	   echo
	fi
    
	# Reset everything
	bold=$default_bold
	underline=$default_underline
	fore=$default_text
	back=$default_background
	
	OPTIND=0
	
    return
}

changegood()
{
    # Call function to process args
    helper_color $@

	echo -e -n "\E[${bold};${underline};${fore};${back}m"
    
    return
}

changebad()
{
	echo -e -n "\E[0m"
    
    # Reset everything
	bold=$default_bold
	underline=$default_underline
	fore=$default_text
	back=$default_background
	
    return
}