#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/23/08
# AUTHOR(S): Zarrar Shehzad

# Variables
declare -a masks
declare -a xCoords
declare -a yCoords
declare -a zCoords

# USAGE
## processMaskList
##  <type of list given (1=list of masks; 2=list of mask files)
##  <list (can contain one of the two options above)>
# EXAMPLE
##  processMaskList 1 "dlpfcL dlpfcR"
##  processMaskList 2 "toroMasks.txt dosenbachMasks.txt"
# INFO
## will get you mask/ROI names into an array
function processMaskList {
    # VARIABLES
    ## User Variables
    local maskType="$1"
    local list="$2"
    ## Function Variables
    local file=""
    
    # CHECKS
    ## need 2 arguments
    if [[ "$#" -ne 2 ]]; then
        zemerg "Function 'processMaskList' only accepts 2 arguments; $# arguments given!"
        exit -2
    fi
    ## need to have masks
    if [[ -z "$list" ]]; then
        zerror "Mask list is empty, please give me some masks!"
        exit -2
    fi
    
    # PROCESS
    case "$maskType" in
        ## List of Masks
        1 ) zinfo "reading list of masks given by user"
            masks=( ${list} )
            ;;
        ## List of Files
        2 ) for file in ${list}; do
                if [[ ! -e "$file" ]]; then
                    zerror "Cannot find mask file '${file}'"
                    exit -2
                fi
                zinfo "reading list of masks in $file"
                masks=( ${masks[*]} `awk '{print \$1}' ${file}` )
            done
            ;;
        ## Whoops
        * ) zemerg "Incorrect mask list type given. Must be 1 (list of masks) or 2 (list of files with masks listed in them)"
            exit -2
            ;;
    esac
    
    if [[ -z "$masks" ]]; then
        zerror "Cannot process data, no mask information"
        exit -1
    else
        zinfo "Running masks: ${masks[*]}"
        echo
        return 0
    fi
    
}

function processCreateMaskList {
    # VARIABLES
    ## User Variables
    local maskType="$1"
    local list="$2"
    ## Function Variables
    local file=""
    
    # CHECKS
    ## need 2 arguments
    if [[ "$#" -ne 2 ]]; then
        zemerg "Function 'processMaskList' only accepts 2 arguments; $# arguments given!"
        exit -2
    fi
    ## need to have masks
    if [[ -z "$list" ]]; then
        zerror "Mask list is empty, please give me some masks!"
        exit -2
    fi
    
    # PROCESS
    case "$maskType" in
        ## One Mask + values
        1 ) zinfo "reading mask and coordinates given by user"
            set -- $list
            masks=( $1 )
            xCoords=( $2 )
            yCoords=( $3 )
            zCoords=( $4 )
            ;;
        ## List of Files
        2 ) for file in ${list}; do
                if [[ ! -e "$file" ]]; then
                    zerror "Cannot find mask file '${file}'"
                    exit -2
                fi
                zinfo "reading list of masks in $file"
                masks=( ${masks[*]} `awk '{print \$1}' ${file}` )
                xCoords=( ${xCoords[*]} `awk '{print \$2}' ${file}` )
                yCoords=( ${yCoords[*]} `awk '{print \$3}' ${file}` )
                zCoords=( ${zCoords[*]} `awk '{print \$4}' ${file}` )
            done
            ;;
        ## Whoops
        * ) zemerg "Incorrect mask list type given. Must be 1 (list of masks) or 2 (list of files with masks listed in them)"
            exit -2
            ;;
    esac
    
    if [[ -z "$masks" ]]; then
        zerror "Cannot process data, no mask information"
        exit -1
    else
        zinfo "Running masks: ${masks[*]}"
        echo
        return 0
    fi
    
}