#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

# WHAT IS THE FIRST INPUT FOR THESE COMMANDS
BIG_INPUT="${funcRaw}"
# WHAT IS THE LAST OUTPUT FOR THESE COMMANDS
BIG_OUTPUT="${funcPP}"

# Setup commands
# Set Variables for commands
## high pass and low pass filter using fsl
hp=`echo "scale=10;50/${TR}" | bc`
lp=`echo "scale=10;$hp/10" | bc`
# filter = ( seconds / 2 ) / TR
# Set Commands
declare -a COMMANDS
COMMANDS[1]="cd ${funcDir}"
## time shifting slices
COMMANDS[2]="3dTshift -prefix ${funcDir}/${FN_FUNCBASE}_ts -tpattern alt+z -tzero 0 ${BIG_INPUT}"
## deoblique
COMMANDS[3]="3dWarp -deoblique -prefix ${funcDir}/${FN_FUNCBASE}_do ${funcDir}/${FN_FUNCBASE}_ts+orig"
## orient into fsl friendly space
COMMANDS[4]="3dresample -orient RPI -inset ${funcDir}/${FN_FUNCBASE}_do+orig -prefix ${funcDir}/${FN_FUNCBASE}_ro"
## get average func
COMMANDS[5]="3dTstat -mean -prefix ${funcDir}/${FN_FUNCBASE}_mean.${OUTPUT_TYPE} ${funcDir}/${FN_FUNCBASE}_ro+orig"
## motion correct
COMMANDS[6]="3dvolreg -Fourier -twopass -base ${funcDir}/${FN_FUNCBASE}_mean.${OUTPUT_TYPE} -zpad 4 -prefix ${funcDir}/${FN_FUNCBASE}_mc -1Dfile ${funcDir}/${FN_FUNCBASE}_mc.1D -maxdisp1D ${funcDir}/${FN_FUNCBASE}_maskdip.1D ${funcDir}/${FN_FUNCBASE}_ro+orig"
## despike
COMMANDS[7]="3dDespike -ssave ${funcDir}/${FN_FUNCBASE}_spikiness.1D -prefix ${funcDir}/${FN_FUNCBASE}_ds ${funcDir}/${FN_FUNCBASE}_mc+orig"
## skull strip
COMMANDS[8]="3dAutomask -prefix ${funcDir}/${FN_FUNCBASE}_mask -dilate 1 ${funcDir}/${FN_FUNCBASE}_ds+orig"
COMMANDS[9]="3dcalc -a ${funcDir}/${FN_FUNCBASE}_ds+orig -b ${funcDir}/${FN_FUNCBASE}_mask+orig -expr 'a*b' -prefix ${funcDir}/${FN_FUNCBASE}_st"
## spatial smoothing
COMMANDS[10]="3dmerge -1blur_fwhm $FWHM -doall -prefix ${funcDir}/${FN_FUNCBASE}_ss.${OUTPUT_TYPE} ${funcDir}/${FN_FUNCBASE}_st+orig"
## grandmean scaling
COMMANDS[11]="fslmaths ${funcDir}/${FN_FUNCBASE}_ss -ing 10000 ${funcDir}/${FN_FUNCBASE}_gin -odt float"
## temporal filtering
COMMANDS[12]="fslmaths ${funcDir}/${FN_FUNCBASE}_gin -bptf ${hp} ${lp} ${funcPP} -odt float"
## seperate motion parameters into seperate files
COMMANDS[13]="awk '{print \$1}' ${$funcDir}/${FN_FUNCBASE}_mc.1D > ${nuisanceDir}/${FN_MCBASE}1.1D"
COMMANDS[14]="awk '{print \$2}' ${$funcDir}/${FN_FUNCBASE}_mc.1D > ${nuisanceDir}/${FN_MCBASE}2.1D"
COMMANDS[15]="awk '{print \$3}' ${$funcDir}/${FN_FUNCBASE}_mc.1D > ${nuisanceDir}/${FN_MCBASE}3.1D"
COMMANDS[16]="awk '{print \$4}' ${$funcDir}/${FN_FUNCBASE}_mc.1D > ${nuisanceDir}/${FN_MCBASE}4.1D"
COMMANDS[17]="awk '{print \$5}' ${$funcDir}/${FN_FUNCBASE}_mc.1D > ${nuisanceDir}/${FN_MCBASE}5.1D"
COMMANDS[18]="awk '{print \$6}' ${$funcDir}/${FN_FUNCBASE}_mc.1D > ${nuisanceDir}/${FN_MCBASE}6.1D"
COMMANDS[19]="cd -"

# INFO
declare -a COMMANDS_INFO
## nothing for 1
COMMANDS_INFO[2]="Timeshifting (interlaced)"
COMMANDS_INFO[3]="Deobliquing"
COMMANDS_INFO[4]="Reorienting to RPI"
COMMANDS_INFO[5]="Get mean functional image (for motion correction)"
COMMANDS_INFO[6]="Motion correction"
COMMANDS_INFO[7]="Despike"
COMMANDS_INFO[8]="Skull Strip"
## nothing for 9
COMMANDS_INFO[10]="Spatial smoothing"
COMMANDS_INFO[11]="Grand-mean scaling"
COMMANDS_INFO[12]="Band-pass filter (get low-frequencies)"

# INPUTS
declare -a COMMANDS_INPUT

# OUTPUTS
declare -a COMMANDS_OUTPUT
## nothing for 1
COMMANDS_OUTPUT[2]="${funcDir}/${FN_FUNCBASE}_ts+orig.HEAD ${funcDir}/${FN_FUNCBASE}_ts+orig.BRIK"
COMMANDS_OUTPUT[3]="${funcDir}/${FN_FUNCBASE}_do+orig.HEAD ${funcDir}/${FN_FUNCBASE}_do+orig.BRIK"
COMMANDS_OUTPUT[4]="${funcDir}/${FN_FUNCBASE}_ro+orig.HEAD ${funcDir}/${FN_FUNCBASE}_ro+orig.BRIK"
COMMANDS_OUTPUT[5]="${funcDir}/${FN_FUNCBASE}_mean+orig.HEAD ${funcDir}/${FN_FUNCBASE}_mean+orig.BRIK"
COMMANDS_OUTPUT[6]="${funcDir}/${FN_FUNCBASE}_mc+orig.HEAD ${funcDir}/${FN_FUNCBASE}_mc+orig.BRIK"
COMMANDS_OUTPUT[7]="${funcDir}/${FN_FUNCBASE}_ds+orig.HEAD ${funcDir}/${FN_FUNCBASE}_ds+orig.BRIK"
COMMANDS_OUTPUT[8]="${funcDir}/${FN_FUNCBASE}_mask+orig.HEAD ${funcDir}/${FN_FUNCBASE}_mask+orig.BRIK"
COMMANDS_OUTPUT[9]="${funcDir}/${FN_FUNCBASE}_st+orig.HEAD ${funcDir}/${FN_FUNCBASE}_st+orig.BRIK"
COMMANDS_OUTPUT[10]="${funcDir}/${FN_FUNCBASE}_ss.${OUTPUT_TYPE}"
COMMANDS_OUTPUT[11]="${funcDir}/${FN_FUNCBASE}_gin.${OUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${BIG_OUTPUT}"
