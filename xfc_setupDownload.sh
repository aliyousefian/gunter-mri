#!/usr/bin/env bash

# REGISTRATION SCRIPT
# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 11/25/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/setup_download.sh" # can override this file at command-line
cpOptions="cp -n -v"
inputDir="/Volumes/CBI/castellanoslab"

# SPECIAL FUNCTIONS
function setup_copyData {
    local dir=""
    local subDir="$1"
    local dirList="$2"
    
    for dir in ${dirList}; do
        zcmd "mkdir ${rawDir}/${dir} &> /dev/null"
        zcmd "${cpOptions} ${inputDir}/${subDir}/${dir}* ${rawDir}/${dir}"
    done
}

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <subject files>"
    echo "  $0 [OPTIONS] -S <subject list>"
    echo "Program:"
    echo "   Download new participant data from CBI"
    echo "Required Options:"
    echo "  * -S or -s"
    echo "Available Options:"
    echo "  -i: input; directory with subject directories with the raw data to be copied (default: ${inputDir})"
    echo "  -o: raw directory; directory in each subject's folder where the raw data will be downloaded into free of charge (default: ${gDIR_RAW})"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":i:o:${standardOptList}" Option; do    
    case $Option in
        i ) inputDir="$OPTARG"
            ;;
        o ) gDIR_RAW="$OPTARG"
    		;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# If force is set, then setup to overwrite files previously copied
if [[ "$to_force" = 1 ]]; then
    cpOptions="cp -i -v"
fi
if [[ "$to_ignore" = 1 ]]; then
    cpOptions="cp -f -v"
fi
# Check if inputDir exists
if [[ ! -d "$inputDir" ]]; then
    zerror "Input directory '${inputDir}' with raw data (from CBI) to be copied does not exist, please make sure that you have mounted this drive"
    exit -1
fi


###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"



