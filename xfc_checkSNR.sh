#!/usr/bin/env bash

# REGISTRATION SCRIPT
# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 11/25/08
# AUTHOR(S): Zarrar Shehzad

# RUN THE SNR THING SLIGHTLY DIFFERENTLY THEN THE OTHER SCRIPTS
# USE sed... with regards to the input

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/check_snr.sh" # can override this file at command-line
outputDir=""
outputPrefix=""

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -o <output directory> -s <subject files>"
    echo "  $0 [OPTIONS] -o <output directory> -S <subject list>"
    echo "Program:"
    echo "   Quality Control: will go through subjects SNR images and register them into standard space (using linear registration) and do some calculations"
    echo "Note:"
    echo "  * You must have first run xfc_prefunc.sh and xfc_prereg.sh!!!"
    echo "Required Options:"
    echo "  * -o"
    echo "  * -S or -s"
    echo "Available Options:"
    echo "  -o: directory to which you want to output the mean SNR and other images (required)"
    echo "  -p: prefix for two filenames (one that will have a list of the mean/std SNR values and another that will have a list of mean/std z-scores for each participant)"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":o:f:${standardOptList}" Option; do    
    case $Option in
        o ) outputDir="$OPTARG"
            if [[ ! -d "$outputDir" ]]; then
                zerror "Output directory '${outputDir}' does not exist"
                exit -1
            fi
    		;;
    	p ) outputPrefix="$OPTARG"
    	    ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

###
# BEGIN PROCESSING
#
## see loops.sh

## 1. Will register each participant's snr map
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"

## 2. Will combine maps and do various calculations
ztitle "Combining the different snr maps together"
zcmd "cd ${outputDir}"
### compile list of snr files
declare -a snrFiles
for subject in ${subjects}; do
    source $gSubConfigFile
    snrFiles[${#snrFiles[@]}]="${checksDir}/1_func/${gFN_FUNCBASE}_snr.${gOUTPUT_TYPE}"
done
### combining
zcmd "fslmerge -t snr_merge ${snrFiles[*]}"
### getting mask
zcmd "fslmaths snr_merge -Tmin -bin snr_mask"
### getting mean snr
zcmd "fslmaths snr_merge -mas snr_mask -Tmean snr_mean"
### getting standard deviation of snr
zcmd "fslmaths snr_merge -mas snr_mask -Tstd snr_std"
### calculating z-scores
zcmd "fslmaths snr_merge -mas snr_mask -sub snr_mean -div snr_std snr_zscore"
### calculate mean/std values for each participant and spit out into file
zinfo "spitting out file with first column of means and second column of standard deviations"
zcmd "3dmaskave -mask snr_mask.${gOUTPUT_TYPE} -sigma -quiet snr_merge.${gFN_FUNCBASE} > ${outputPrefix}_raw.${gTS_EXT}"
zcmd "3dmaskave -mask snr_mask.${gOUTPUT_TYPE} -sigma -quiet snr_zscore.${gFN_FUNCBASE} > ${outputPrefix}_zscore.${gTS_EXT}"
