#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/22/08
# AUTHOR(S): Zarrar Shehzad

# Extract mean time-series from ROI(s)

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
#commandFile="${GUNTHERDIR}/commands/extractTs.sh" # can override this file at command-line
commandFile="${GUNTHERDIR}/commands/extractTs_noPW.sh" # can override this file at command-line
maskType=0
maskList=""

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -m <file list ROIs> -s <files list subjects>"
    echo "  $0 [OPTIONS] -M <ROI> -s <file list subjects>"
    echo "  $0 [OPTIONS] -m <file list ROIs> -S <subject ids>"
    echo "  $0 [OPTIONS] -M <ROI> -S <subject ids>"
    echo "Program:"
    echo "   Extract mean time-series for given ROI(s)"
    echo "Required Options:"
    echo "  * -M or -m"
    echo "  * -S or -s"
    echo "Available Options:"
    echo "  -m: a file that lists names of ROIs on each line; each name must correspond to the filename of a mask and this name will also be used as the filename for the extracted timeseries (note if this file has more than one column only the first column will be used)"
    echo "  -M: a specific ROI name"
    echo "  -i: directory in which ROI masks can be found (default: ${gDIR_MASKS})"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":m:M:i:${standardOptList}" Option; do    
    case $Option in
        m ) maskType=2
            maskList="$OPTARG"
            if [[ ! -e "$maskList" ]]; then
                zerror "Cannot find the file with list of ROIs '${maskList}'!"
                exit -1
            fi
            ;;
        M ) maskType=1
            maskList="$OPTARG"
            ;;
        i ) gDIR_MASKS="$OPTARG"
            if [[ ! -d "$gDIR_MASKS" ]]; then
                zerror "Cannot find the masks directory '${gDIR_MASKS}'!"
                exit -1
            fi
            ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# Check on required input
if [[ "$maskType" = 0 ]]; then
    zerror "Please specify mask list (-m or -M)"
    usage
    exit -2
fi

###
# BEGIN PROCESSING
#
## see loops.sh
subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile" "maskLoop" "$maskType" "$maskList"
