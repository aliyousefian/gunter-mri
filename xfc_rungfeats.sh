#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/21/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/run_gfeats.sh" # can override this file at command-line
templateFile=""
templateFind=""
newFsfName=""
outputDir=""
run_feat=1 # Default is to run the feat

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -T template.fsf -j subjectTemplate -s <text file with list of names to substitute -j option>"
    echo "  $0 [OPTIONS] -T template.fsf -j subjectTemplate -S <list of names to substitute -j option>"
    echo "Program:"
    echo "   Will batch process a specific feat for all participants"
    echo "Required Options:"
    echo "  * -T, -j, -o"
    echo "Notes: "
    echo "  * By default the program will check the output gfeat directory for each group analysis and if it exists then it will not run that gfeat"
    echo "  * To overwrite a gfeat directory that already exits, you can use the -f option"
    echo "  * To skip checking for the output gfeat directory, you can use the -z option (in this case if the gfeat directory does exists fsl will add a + to the end of the directory name and continue to run)"
    echo "  * the -z and -f options are mutually exclusive"
    echo "Available Options:"
    echo "  -T: template filename"
    echo "  -j: what to find and replace in the template file"
    echo "  -o: Output directory for group feats (e.g., ${gDIR_SUBDATA}/groups)"
    echo "  -n: new fsf filename (default: template filename)"
    echo "  -M: only run feat model (default: run both feat_model + feat)"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":T:j:n:e:E:m:M${standardOptList}" Option; do    
    case $Option in
        T ) templateFile="$OPTARG"
            if [[ ! -e "$templateFile" ]]; then
                zerror "Input template file '${templateFile}' does not exist"
                exit -1
            fi
            ;;
        j ) templateFind="$OPTARG"
            ;;
        n ) newFsfName="${OPTARG%.fsf}"
            ;;
        o ) outputDir="$OPTARG"
            if [[ ! -d "$outputDir" ]]; then
                zerror "Output directory '${outputDir}' does not exist"
                exit -1
            fi
            ;;
        M ) run_feat=0
            ;;
    	h ) usage 2
    	    exit 1
    	    ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# Checks + Settings
if [[ -z "$templateFile" || -z "$templateFind" ]]; then
    zerror "Required parameters -T or -j not given"
    exit -1
fi
if [[ -z "$newFsfName" ]]; then
    newFsfName=$( basename ${templateFile%.fsf} )
fi

###
# BEGIN PROCESSING
#
## see loops.sh (using subLoop but not actually looping subject in this instance)
subLoop "$subtype" "$*" "/dev/null" "$commandFile"
