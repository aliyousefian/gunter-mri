#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/24/08
# AUTHOR(S): Zarrar Shehzad

# Will find and replace a string in a given set of strings

# Check
if [[ "$#" -ne 3 ]]; then
    echo "usage: $0 <find> <replace> <string>"
    exit -1
fi

# Set variables
find="$1"
replace="$2"
oldString="$3"

# Do the find and replace
echo "$oldString" | sed s/${find}/${replace}/g
