#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/20/08
# AUTHOR(S): Zarrar Shehzad

echo "$1" | sed -e 's/\//\\\//g'
