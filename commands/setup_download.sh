#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 11/25/08
# AUTHOR(S): Zarrar Shehzad

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial input (no real final output in this case)
BIG_INPUT="${inputDir} ${iDir}"
BIG_OUTPUT="/crap/crap" # need to be able to not have to set the input or output

# 1. Create raw directory
COMMANDS_INFO[1]="creating raw directory"
COMMANDS[1]="mkdir ${rawDir}"
COMMANDS_INPUT[1]="${iDir}"
COMMANDS_OUTPUT[1]="${rawDir}"

# 2. don't want to copy raw data
COMMANDS_INFO[2]="will not copy .raw files"
COMMANDS[2]="GLOBIGNORE=*.raw"

# 3. Copy main data
## Change directories
COMMANDS[3]="cd ${inputDir}/${subject}"
COMMANDS_INPUT[3]="${inputDir}/${subject}"
## get list of directories
COMMANDS_INFO[4]="getting the list of non-rest directories to copy"
COMMANDS[4]="dirList=( \`ls -d */\` )"
## copy directories (not that for the loop to work, i have escaped the $dir variable that is set anew with each loop)
COMMANDS_INFO[5]="copying all the directories"
COMMANDS[5]="setup_copyData '${subject}' '${dirList[@]}'"
# for dir in \${dirList[@]} ; do zcmd 'mkdir ${rawDir}/\${dir} &> /dev/null'; zcmd '${cpOptions} ${inputDir}/${subject}/\${dir}* ${rawDir}/\${dir}'; done

# 5. Copy rest data
## change directories
COMMANDS[6]="cd ${inputDir}/${subject}_rest"
COMMANDS_INPUT[6]="${inputDir}/${subject}_rest"
## get list of directories
COMMANDS_INFO[7]="getting the list of rest directories to copy"
COMMANDS[4]="dirList=( \`ls -d */\` )"
## copy directories
COMMANDS_INFO[8]="copying all the directories"
COMMANDS[8]="for dir in \${dirList[@]} ; do zcmd 'mkdir ${rawDir}/\${dir} &> /dev/null'; zcmd '${cpOptions} ${inputDir}/${subject}_rest/\${dir}* ${rawDir}/\${dir}'; done"

# 6. Leave your mark that copied the data
COMMANDS_INFO[9]="leaving a mark that data succesfully copied"
COMMANDS[9]="touch ${inputDir}/${subject}/COPIED.txt"
COMMANDS[10]="touch ${inputDir}/${subject}_rest/COPIED.txt"

# 7. Go back to being able to copy raw files
COMMANDS_INFO[11]="clean up"
COMMANDS[11]="GLOBIGNORE=''"

