#!/usr/bin/env bash

# DoGroupFlameo
# Script to run Group analysis using flameo from FSL
# Written by Maarten Mennes
# This script is called by Setup_DoGroupFlameo.sh


#-------
# INPUTS
#-------

project_dir=$1
group_dir=$2
model_dir=$3
model_list=$4
seed_list=$5
seed_dir=$6
subject_list=$7
site=$8
group_mm=$9
shift 
func=$9

#----------------------------
# SET NEEDED LISTS/PARAMETERS
#----------------------------
## GET MODELS
if [ -f ${model_list} ]
then
models=$(cat ${model_list} )
else
models=${model_list}
fi

## GET SEEDS
if [ -f ${seed_list} ]
then
seeds=$(cat ${seed_list} )
else
seeds=${seed_list}
fi

## GET SUBJECTS
subjects=$( cat ${subject_list} )

## SET REGISTR PARAMETER
#if [ "$registration" = "FLIRT" ]
#then
#registr=fl
#fi
#if [ "$registration" = "FNIRT" ]
#then
#registr=fn
#fi

## MAKE DIRS
mkdir -p ${group_dir}/files
mkdir -p ${group_dir}/finished


#--------------
# START LOOPING
#--------------

# LOOP 1.1: For each MODEL
for model in $models
do

# LOOP 1.2: For each SEED
for seed in $seeds
do

echo
echo RUNNING GROUP ANALYSIS ${site} ${model} FOR SEED ${seed}
echo

## check for existance of group analysis file
if [ -f ${group_dir}/finished/${site}_${model}_${seed} ]
then
echo "GROUP ANALYSIS FOR ${site} ${model} FOR SEED ${seed} ALREADY DONE"
else


## CREATE CONCATENATED GROUP FILE
tmpfile=`mktemp`
echo ${tmpfile}
echo "Creating group file for ${site}_${model}_${seed}"
if [ -f ${group_dir}/files/${site}_${model}_${seed}_data.nii.gz ]
then
	echo group file for ${site} ${model} seed ${seed} ALREADY EXISTS
else
	for sub in $subjects
	do
	echo ${project_dir}/${sub}/${func}/${seed_dir}/${seed}_Z_2standard.nii.gz >> ${tmpfile}
	done	
	files=$( cat ${tmpfile} )
	echo $files
	fslmerge -t ${group_dir}/files/${site}_${model}_${seed}_data.nii.gz $files
	rm ${tmpfile}
fi


## CREATE ANALYSIS SPECIFIC MASK
if [ -f ${group_dir}/files/${site}_${model}_mask.nii.gz ]
then
echo MASK OK
else
fslmaths ${group_dir}/files/${site}_${model}_${seed}_data.nii.gz -abs -Tmin -bin ${group_dir}/files/${site}_${model}_mask.nii.gz
fi


## RUN FLAMEO
echo "RUNNING FLAMEO ${model} FOR SEED ${seed}"
flameo \
--cope=${group_dir}/files/${site}_${model}_${seed}_data.nii.gz \
--mask=${group_dir}/files/${site}_${model}_mask.nii.gz \
--dm=${model_dir}/${model}.mat \
--tc=${model_dir}/${model}.con \
--cs=${model_dir}/${model}.grp \
--ld=${group_dir}/${site}_${model}_${seed} \
--runmode=ols


## THRESHOLD ZSTAT MAPS
echo "THRESHOLDING zstat-maps"
curdir=$( pwd )
cd ${group_dir}/${site}_${model}_${seed}

cat ${model_dir}/${model}.con | grep '/NumContrasts' > temp
sed 's_/NumContrasts\t__' < temp > temp2
numcon=$( cat temp2 )
rm temp temp2

# LOOP 1.3: Threshold Zstat maps
for ((z=1 ; z <= ${numcon} ; z++))
do
	echo "thresholding zstat${z}"
	easythresh zstat${z} \
	${group_dir}/files/${site}_${model}_mask.nii.gz \
	2.3 0.05 \
	${FSLDIR}/data/standard/MNI152_T1_${group_mm}_brain.nii.gz \
	zstat${z}

	# THRESHOLD MAP TO AVOID NEG VALUES - these show up for some reason in the thresholded maps
	fslmaths thresh_zstat${z}.nii.gz -thr 0 \
	-mul ${FSLDIR}/data/standard/MNI152_T1_${group_mm}_brain_mask.nii.gz thresh_zstat${z}.nii.gz
	
# END OF LOOP 1.3
done

## CLEAN UP
rm res4d.nii.gz
cd ${curdir}

## MARK GROUP ANAYSIS AS DONE
[ -f ${group_dir}/${site}_${model}_${seed}/thresh_zstat1.nii.gz ] && touch ${group_dir}/finished/${site}_${model}_${seed}

## end of check to see if model seed is already done
echo GROUP ANALYSIS FOR ${site} ${model} ${seed} DONE
echo ----------------------------------------
fi

# END OF LOOP 1.2: SEED
done

# END OF LOOP 1.1: MODEL
done
