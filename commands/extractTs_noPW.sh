#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/24/08
# AUTHOR(S): Zarrar Shehzad

#echo test > ${tsFile}_stdev.${gTS_EXT}


##CREATED 24TH JUNE 2009
## USES DETRENDED, UNPREWHITENED RESIDUALS, THAT ARE NOT NORMALIZED BY STDEV


# Command Vars
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${maskFile} ${funcRes_noPWStandard}"
BIG_OUTPUT="${tsFile}.${gTS_EXT}"

# Making Seeds and Feats directories in your projects directory
COMMANDS_INFO[1]="Creating the subject directory in ${gDIR_PROJECTS}"
COMMANDS[1]="mkdir ${iProjSubDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[2]="Creating the func directory in ${iProjSubDir}"
COMMANDS[2]="mkdir ${iProjDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[3]="Creating the seeds directory"
COMMANDS[3]="mkdir ${tsDir_noPW} 2> /dev/null; echo -n ''"

COMMANDS_INFO[4]="Creating the feats directory"
COMMANDS[4]="mkdir ${featsDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[5]="Creating the corrs directory"
COMMANDS[5]="mkdir ${corrsDir} 2> /dev/null; echo -n ''"

# Obtain average time series of a region
COMMANDS_INFO[6]="Extracting mean time series"
COMMANDS[6]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${funcRes_noPWStandard} > ${tsFile_noPW}.${gTS_EXT}"

COMMANDS_INFO[7]="Checking mean time series"
COMMANDS[7]="x_checkTs.sh -i ${tsFile_noPW}.${gTS_EXT} -v ${gVOLNUM} -d"

