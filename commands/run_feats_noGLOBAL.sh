#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/19/08
# AUTHOR(S): Zarrar Shehzad

# Variables
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# INITIAL INPUT AND FINAL OUTPUT
BIG_INPUT="`dirname ${featsDir_noGLOBAL}`"
BIG_OUTPUT=""

## note that x_fsfsubstitute.sh runs the feat_model and also checks for output, etc. if it fails it spits out a non-zero exit which causes this subject to be skipped
COMMANDS[${#COMMANDS[@]}]="mkdir -p ${featsDir_noGLOBAL}/${gDIR_FSF} 2> /dev/null; echo -n ''"
COMMANDS[${#COMMANDS[@]}]="x_fsfsubstitute.sh -i ${templateFile} -f ${templateSubject} -r ${subject} -o ${featsDir_noGLOBAL}/${gDIR_FSF}/${newFsfName}.fsf -c '1 ${to_skip} ${to_force}'"

if [[ "$copy_mat" == 1 ]]; then
    ## Copy mat file from feat_model to some central directory
    COMMANDS_INFO[${#COMMANDS[@]}]="Copying over mat file"
    COMMANDS[${#COMMANDS[@]}]="cp ${featsDir_noGLOBAL}/${gDIR_FSF}/${newFsfName}.mat ${matDir}/${newFsfName}/${subject}.mat"
fi

if [[ "$run_feat" == 1 ]]; then
    ## Run Feat
    COMMANDS_INFO[${#COMMANDS[@]}]="Running feat"
    COMMANDS[${#COMMANDS[@]}]="feat ${featsDir_noGLOBAL}/${gDIR_FSF}/${newFsfName}.fsf"
    ## Link Reg
    COMMANDS_INFO[${#COMMANDS[@]}]="Linking registration directory"
    COMMANDS[${#COMMANDS[@]}]="outputFeatDir=\`x_fsfinfo.sh \"set fmri(outputdir)\" ${featsDir_noGLOBAL}/${gDIR_FSF}/${newFsfName}.fsf | sed s/\\\.feat$//\`.feat"
    COMMANDS[${#COMMANDS[@]}]="ln -s ${regDir} \${outputFeatDir}/reg"

    if [[ "$removeFilteredFunc" == 1 ]]; then
        COMMANDS_INFO[${#COMMANDS[@]}]="Replacing filtered_func_data file in feat"
        COMMANDS[${#COMMANDS[@]}]="inputFuncFile=\`x_fsfinfo.sh \"set feat_files\" ${featsDir_noGLOBAL}/${gDIR_FSF}/${newFsfName}.fsf\`"
        ## Remove filtered_func_data
        COMMANDS[${#COMMANDS[@]}]="rm \${outputFeatDir}/filtered_func_data.${gOUTPUT_TYPE}"
	COMMANDS[${#COMMANDS[@]}]="rm \${outputFeatDir}/stats/res4d.${gOUTPUT_TYPE}"
        ## Link to input file of feat
        COMMANDS[${#COMMANDS[@]}]="ln -s \${inputFuncFile} \${outputFeatDir}/filtered_func_data.${gOUTPUT_TYPE}"
    fi
    
    ## Do Fisher's Transform
    COMMANDS_INFO[${#COMMANDS[@]}]="Doing Fisher's Z Transform"
    COMMANDS[${#COMMANDS[@]}]="outputFeatDir=\`x_fsfinfo.sh \"set fmri(outputdir)\" ${featsDir_noGLOBAL}/${gDIR_FSF}/${newFsfName}.fsf | sed s/\\\.feat$//\`.feat"
    COMMANDS[${#COMMANDS[@]}]="mv \${outputFeatDir}/stats/cope1.nii.gz \${outputFeatDir}/stats/copy_cope1.nii.gz" 
    COMMANDS[${#COMMANDS[@]}]="3dcalc -a \${outputFeatDir}/stats/copy_cope1.nii.gz -expr '(log(a+1)-log(a-1))/2' -prefix \${outputFeatDir}/stats/cope1.nii"      
fi
