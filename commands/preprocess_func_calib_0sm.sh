#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, Michael Milham, Maarten Mennes, The Underpants Gnomes.

# Initial input and Final output
BIG_INPUT="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} ${calSynth} ${funcregDir}/example_func2synth.mat ${calibDir}/cal_reg_boRPI.${gOUTPUT_TYPE}"
BIG_OUTPUT="${funcPP_DT_0sm}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)

##registration to the cal_synth
COMMANDS_INFO[1]="Registering functional to synthetic image"
COMMANDS[1]="flirt -in ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} -ref ${calSynth} -applyxfm -init ${funcregDir}/example_func2synth.mat -out ${preDir}/3a_${gFN_FUNCBASE}_to_synth"
COMMANDS_OUTPUT[1]="${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}"

##based on Souheil's recommendation, performing motion correction to cal_synth image (which is also used for registration, and can be used for func and func_2).
## motion correct
COMMANDS_INFO[2]="Motion correction"
COMMANDS[2]="3dvolreg -Fourier -twopass -base '${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}[7]' -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[2]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

#unwarping
COMMANDS_INFO[3]="Unwarping functional"
COMMANDS[3]="fugue -i ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} --loadfmap=${calibDir}/cal_reg_boRPI --unwarpdir=x --dwell=1 -u ${preDir}/3c_${gFN_FUNCBASE}_unwarped"
COMMANDS_OUTPUT[3]="${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE}"

## skull strip - LEAVING OUTPUT AS AFNI FILE BECAUSE REFIT FOR TR MIS-SPECIFICATION NOT WORKING ON NIFTI FILES
COMMANDS_INFO[4]="Skull Strip"
COMMANDS[4]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate 1 ${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[4]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[5]="3dcalc -a ${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st"
COMMANDS_OUTPUT[5]="${preDir}/4b_${gFN_FUNCBASE}_st+orig.*"

##The registration produces mis-specficication of the TR in the header - TR correction done here
COMMANDS_INFO[6]="Changing TR specification in header to ensure it's correct"
COMMANDS[6]="3drefit -TR ${gTR} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"

## despike
COMMANDS_INFO[7]="Despike"
COMMANDS[7]="3dDespike -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"
COMMANDS_OUTPUT[7]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## spatial smoothing
#COMMANDS_INFO[8]="Spatial smoothing"
#COMMANDS[8]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
#COMMANDS_OUTPUT[8]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[8]="Grand-mean scaling or intensity normalization"
COMMANDS[8]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin0sm -odt float"
COMMANDS_OUTPUT[8]="${preDir}/7_${gFN_FUNCBASE}_gin0sm.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[9]="Band-pass filter (get low-frequencies)"
COMMANDS[9]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[9]="${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[10]="Detrend"
COMMANDS[10]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE}"
COMMANDS[11]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[11]="${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE}"
COMMANDS[12]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT_0sm}"
COMMANDS_OUTPUT[12]="${funcPP_DT_0sm}"

