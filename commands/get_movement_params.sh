#!/bin/bash

lfoname=${1}
funcDir=${2}
checksDir=${3}

if [ -f ${checksDir}/movement_params.txt ];
	then rm ${checksDir}/movement_params.txt
	else echo
fi

#####MAX DISPLACEMENT
#####awk '{if(min==""){min=max=$1}; if($1>max){max=$1}; if($1<min){min=$1}; total+=$1; count+=1} END {print total/count, min, max}' lfo_maxdisp.1D
# x=`/usr/local/gunther-mri/commands/calc_STD.sh ${funcDir}/${lfoname}_maxdisp.1D`

#Line15-25 Added on 270213 (YC)
line=$(head -1 ${funcDir}/${lfoname}_maxdisp.1D)
s1="# maxdisp"
s2="# max displacement (mm) for each volume"
if [ "$line" != "$s1" ] && [ "$line" != "$s2" ];
	then 
	sed -i '1d' ${funcDir}/${lfoname}_maxdisp.1D
	else
	echo 'yes'
fi 

x=`bash /usr/local/gunther-mri/commands/calc_STD.sh ${funcDir}/${lfoname}_maxdisp.1D`
y=`awk 'max=="" || $1 > max {max=$1} END{ print max}' ${funcDir}/${lfoname}_maxdisp.1D`

printf "%.2f\t" ${x} > ${checksDir}/movement_params.txt
printf "%.2f\t" ${y} >> ${checksDir}/movement_params.txt

# first is mean, second is std, third is max of max displacement
######MOTION PARAMETERS
for j in {1..6}
do
       awk 'max=="" || $1 > max {max=$1}; END {print max}'  ${funcDir}/mc${j}.1D >${checksDir}/maxmin.txt
       awk 'min=="" || $1 < min {min=$1}; END {print min}'  ${funcDir}/mc${j}.1D >${checksDir}/min.txt
       awk '{ if($1>=0) { print $1} else {print $1*-1}}' ${checksDir}/min.txt >> ${checksDir}/maxmin.txt
       value=`awk 'max=="" || $1 > max {max=$1}; END {print max}' ${checksDir}/maxmin.txt`
       printf "%.2f\t" ${value} >> ${checksDir}/movement_params.txt
       rm ${checksDir}/maxmin.txt ${checksDir}/min.txt
done

