#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/25/08
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, and Michael Milham

# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
## high pass and low pass filter using fsl (0.01-0.1 HZ)
hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR
## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## time shifting slices
COMMANDS_INFO[3]="Timeshifting (interlaced)"
COMMANDS[3]="3dTshift -prefix ${preDir}/1_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${funcRaw}"
COMMANDS_OUTPUT[3]="${preDir}/1_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[4]="Deobliquing"
COMMANDS[4]="3drefit -deoblique ${preDir}/1_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[5]="Reorienting to RPI"
COMMANDS[5]="3dresample -orient RPI -inset ${preDir}/1_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[5]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

## get average func
COMMANDS_INFO[6]="Get mean functional image (for motion correction)"
COMMANDS[6]="3dTstat -mean -prefix ${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[6]="${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

## motion correct
COMMANDS_INFO[7]="Motion correction"
COMMANDS[7]="3dvolreg -Fourier -twopass -base ${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[7]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

## skull strip
COMMANDS_INFO[8]="Skull Strip"
COMMANDS[8]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate ${afniDilate} ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[8]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[9]="3dcalc -a ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[9]="${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"

## despike
COMMANDS_INFO[10]="Despike"
COMMANDS[10]="3dDespike -ssave ${funcDir}/${gFN_FUNCBASE}_spikiness.1D -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## example func
COMMANDS_INFO[11]="Get example func in this case the second image (used later for registration)"
COMMANDS[11]="fslroi ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${funcExample} 1 1"
COMMANDS_OUTPUT[11]="${funcExample}"

## spatial smoothing
COMMANDS_INFO[12]="Spatial smoothing"
COMMANDS[12]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[13]="Grand-mean scaling or intensity normalization"
COMMANDS[13]="fslmaths ${preDir}/6_${gFN_FUNCBASE}_ss -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin -odt float"
COMMANDS_OUTPUT[13]="${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[14]="Band-pass filter (get low-frequencies)"
COMMANDS[14]="fslmaths ${preDir}/7_${gFN_FUNCBASE}_gin -bptf ${hp} ${lp} ${funcPP} -odt float"
COMMANDS_OUTPUT[14]="${funcPP}"

## mask
COMMANDS_INFO[15]="Generating another mask of brain (used later for regression analyses)"
COMMANDS[15]="fslmaths ${funcPP} -Tmin -bin ${funcMask} -odt char"
COMMANDS_OUTPUT[15]="${funcMask}"


## SNR

COMMANDS_INFO[16]="Generating SNR of functional image"
COMMANDS[16]="mkdir -p ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[17]="cd ${checksDir}/1_func"

### get mean image
COMMANDS[18]="3dTstat -mean -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[18]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

### get std image
COMMANDS[19]="3dTstat -stdev -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[19]="${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE}"

## get SNR image
COMMANDS[20]="3dcalc -a ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} -expr \"a/b\" -prefix ${checksDir}/1_func/snr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[20]="${checksDir}/1_func/snr.${gOUTPUT_TYPE}"

## spit something out for user
COMMANDS[21]="meanSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M \`"
COMMANDS[22]="sdevSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -S\`"
COMMANDS[23]="zimportant \"${subject} mean SNR is \${meanSNR} and standard deviation of SNR is \${sdevSNR}\""

## get images and put into checks directory
COMMANDS[24]="mkdir ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[25]="slicer ${checksDir}/1_func/${gFN_FUNCBASE}_mean -s 2 -A 750 ${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS_OUTPUT[25]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS[26]="maxSNR=\`3dBrickStat -max ${checksDir}/1_func/snr.${gOUTPUT_TYPE}\`"
COMMANDS[27]="overlay 1 0 ${checksDir}/1_func/${gFN_FUNCBASE}_mean -a ${checksDir}/1_func/snr 0 \${maxSNR} ${checksDir}/1_func/rendered_snr"
COMMANDS_OUTPUT[27]="${checksDir}/1_func/rendered_snr.${gOUTPUT_TYPE}"
COMMANDS[28]="slicer ${checksDir}/1_func/rendered_snr -s 2 -A 750 ${checksDir}/1_func/snr.png"
COMMANDS_OUTPUT[28]="${checksDir}/1_func/snr.png"

