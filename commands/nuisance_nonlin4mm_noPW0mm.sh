
#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/10/08
# AUTHOR(S): Zarrar Shehzad

# Variables
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# INITIAL INPUT AND FINAL OUTPUT
BIG_INPUT="${funcPP_DT_0sm} ${funcDir}/${gFN_MC} ${funcDir}/mask_global.${gOUTPUT_TYPE} ${funcDir}/mask_csf.${gOUTPUT_TYPE} ${funcDir}/mask_wm.${gOUTPUT_TYPE} ${nuisanceTemplate} ${anatregDir}/highres2standard_warp.${gOUTPUT_TYPE} ${funcregDir}/example_func2standard.mat"
BIG_OUTPUT="${funcRes_noPW_0sm_Standard4mm}"

# Variables
nuisanceEvFile="${nuisanceDir}/tmp_evInfo.txt"
# Functions
## nuisance
function setNuisanceEVs {

    local globalFile=$( echo ${nuisanceDir}/${gFN_GLOBAL} | sed -e 's/\//\\\//g' )
    local csfFile=$( echo ${nuisanceDir}/${gFN_CSF} | sed -e 's/\//\\\//g' )
    local wmFile=$( echo ${nuisanceDir}/${gFN_WM} | sed -e 's/\//\\\//g' )
    local mcBase=$( echo ${nuisanceDir}/${gFN_MCBASE} | sed -e 's/\//\\\//g' )

    sed -e "s/GLOBAL_FILE/${globalFile}/" -e "s/CSF_FILE/${csfFile}/" -e "s/WM_FILE/${wmFile}/" -e "s/MC1_FILE/${mcBase}1.${gTS_EXT}/" -e "s/MC2_FILE/${mcBase}2.${gTS_EXT}/" -e "s/MC3_FILE/${mcBase}3.${gTS_EXT}/" -e "s/MC4_FILE/${mcBase}4.${gTS_EXT}/" -e "s/MC5_FILE/${mcBase}5.${gTS_EXT}/" -e "s/MC6_FILE/${mcBase}6.${gTS_EXT}/" ${nuisanceTemplate} > ${nuisanceEvFile}

    zinfo "Created file '${nuisanceEvFile}' with ev info"
    
    return 0
}


# Start
COMMANDS_INFO[1]="Dealing with 9 nuisance variables"
COMMANDS[1]="mkdir ${nuisanceDir} 2> /dev/null; echo -n ''"

# Seperate motion parameters into seperate files
COMMANDS_INFO[2]="Setting up motion covariates in seperate files"
COMMANDS[2]="awk '{print \$1}' ${funcDir}/${gFN_MC} > ${nuisanceDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS_OUTPUT[2]="${nuisanceDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS[3]="awk '{print \$2}' ${funcDir}/${gFN_MC} > ${nuisanceDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS_OUTPUT[3]="${nuisanceDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS[4]="awk '{print \$3}' ${funcDir}/${gFN_MC} > ${nuisanceDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS_OUTPUT[4]="${nuisanceDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS[5]="awk '{print \$4}' ${funcDir}/${gFN_MC} > ${nuisanceDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS_OUTPUT[5]="${nuisanceDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS[6]="awk '{print \$5}' ${funcDir}/${gFN_MC} > ${nuisanceDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS_OUTPUT[6]="${nuisanceDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS[7]="awk '{print \$6}' ${funcDir}/${gFN_MC} > ${nuisanceDir}/${gFN_MCBASE}6.${gTS_EXT}"
COMMANDS_OUTPUT[7]="${nuisanceDir}/${gFN_MCBASE}6.${gTS_EXT}"

# Extract signal for global, csf, and wm
## global
COMMANDS_INFO[8]="Extracting global signal"
COMMANDS[8]="3dmaskave -mask ${funcDir}/mask_global.${gOUTPUT_TYPE} -quiet ${funcPP_DT_0sm} > ${nuisanceDir}/${gFN_GLOBAL}"
COMMANDS_OUTPUT[8]="${nuisanceDir}/${gFN_GLOBAL}"
COMMANDS[9]="x_checkTs.sh -i ${nuisanceDir}/${gFN_GLOBAL} -v ${gVOLNUM} -d"
## csf
COMMANDS_INFO[10]="Extracting signal from csf"
COMMANDS[10]="3dmaskave -mask ${funcDir}/mask_csf.${gOUTPUT_TYPE} -quiet ${funcPP_DT_0sm} > ${nuisanceDir}/${gFN_CSF}"
COMMANDS_OUTPUT[10]="${nuisanceDir}/${gFN_CSF}"
COMMANDS[11]="x_checkTs.sh -i ${nuisanceDir}/${gFN_CSF} -v ${gVOLNUM} -d"
## wm
COMMANDS_INFO[12]="Extracting signal from white matter"
COMMANDS[12]="3dmaskave -mask ${funcDir}/mask_wm.${gOUTPUT_TYPE} -quiet ${funcPP_DT_0sm} > ${nuisanceDir}/${gFN_WM}"
COMMANDS_OUTPUT[12]="${nuisanceDir}/${gFN_WM}"
COMMANDS[13]="x_checkTs.sh -i ${nuisanceDir}/${gFN_WM} -v ${gVOLNUM} -d"

COMMANDS_INFO[14]="Setting up info for nuisance EVs"
COMMANDS[14]="setNuisanceEVs"

# Generate mat file (for use later)
## create fsf file
COMMANDS_INFO[15]="Generating fsf file to model nuisance variables"
COMMANDS_INPUT[15]="${nuisanceDir}/${gFN_GLOBAL} ${nuisanceDir}/${gFN_CSF} ${nuisanceDir}/${gFN_WM}"
COMMANDS[15]="xfc_createfeat.sh -W -G -b ${nuisanceDir} -T nuisanceModel_noPW0sm -j ${subject} -e $nuisanceEvFile -i ${funcPP_DT_0sm} ${saveOptions}"
COMMANDS_OUTPUT[15]="${nuisanceDir}/nuisanceModel_noPW0sm.fsf"
COMMANDS[16]="rm ${nuisanceEvFile}"
## run feat model
COMMANDS_INFO[17]="Running feat model"
COMMANDS[17]="feat_model ${nuisanceDir}/nuisanceModel_noPW0sm"
COMMANDS_OUTPUT[17]="${nuisanceDir}/nuisanceModel_noPW0sm.mat"
## copy data over to be checked
COMMANDS_INFO[18]="Copying image of design matrix for future checking"
COMMANDS[18]="mkdir -p ${checksDir}/4_nuisance 2> /dev/null; echo -n ''"
COMMANDS[19]="cp ${nuisanceDir}/nuisanceModel_noPW0sm*png ${checksDir}/4_nuisance"

# Prepare to generate residuals
COMMANDS_INFO[20]="Get minimum value"
COMMANDS[20]="minVal=\`3dBrickStat -min -mask ${funcMask} ${funcPP_DT_0sm}\`"

# Get residuals
COMMANDS_INFO[21]="Running film to get residuals"
COMMANDS[21]="film_gls -rn ${nuisanceDir}/stats_noPW0sm -noest -sa -ms 5 ${funcPP_DT_0sm} ${nuisanceDir}/nuisanceModel_noPW0sm.mat \${minVal}"
COMMANDS_OUTPUT[21]="${nuisanceDir}/stats_noPW0sm/res4d.${gOUTPUT_TYPE}"

##Demeaning residuals and ADDING 100
COMMANDS_INFO[22]="Demeaning residuals"
COMMANDS[22]="3dTstat -mean -prefix ${nuisanceDir}/stats_noPW0sm/res4d_mean.${gOUTPUT_TYPE} ${nuisanceDir}/stats_noPW0sm/res4d.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[22]="${nuisanceDir}/stats_noPW0sm/res4d_mean.${gOUTPUT_TYPE}"

COMMANDS[23]="3dcalc -a ${nuisanceDir}/stats_noPW0sm/res4d.${gOUTPUT_TYPE} -b ${nuisanceDir}/stats_noPW0sm/res4d_mean.${gOUTPUT_TYPE} -expr '(a-b)+100' -prefix ${funcRes_noPW_0sm}"
COMMANDS_OUTPUT[23]="${funcRes_noPW_0sm}"

# Resample residuals to standard space
COMMANDS_INFO[24]="Resampling residuals to standard space"
COMMANDS[24]="applywarp --ref=${FSLDIR}/data/standard/MNI152_T1_4mm.nii.gz --in=${funcRes_noPW_0sm} --out=${funcRes_noPW_0sm_Standard4mm} --warp=${anatregDir}/highres2standard_warp --premat=${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[24]="${funcRes_noPW_0sm_Standard4mm}"


