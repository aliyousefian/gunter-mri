#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/25/08
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, and Michael Milham

#### MODIFIED JUNE 24TH 2009 
#### ADDED DETRENDING AT THE END OF THE PREPROCESSING STEPS


# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP_DT_0sm}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR
## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## Dropping first 4 TRS
COMMANDS_INFO[3]="Dropping first 4 TRs"
COMMANDS[3]="3dcalc -a '${funcRaw}[${gTRstart}..${gTRend}]' -expr 'a' -prefix ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[3]="${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## Dropping first 4 TRS
COMMANDS_INFO[4]="Changing TR specification in header to ensure it's correct"
COMMANDS[4]="3drefit -TR ${gTR} ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## time shifting slices
COMMANDS_INFO[5]="Timeshifting (interlaced)"
COMMANDS[5]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[5]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[6]="Deobliquing"
COMMANDS[6]="3drefit -deoblique ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[7]="Reorienting to RPI"
COMMANDS[7]="3dresample -orient RPI -inset ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[7]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

## get average func
COMMANDS_INFO[8]="Get mean functional image (for motion correction)"
COMMANDS[8]="3dTstat -mean -prefix ${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[8]="${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

## motion correct
COMMANDS_INFO[9]="Motion correction"
COMMANDS[9]="3dvolreg -Fourier -twopass -base ${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[9]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

## skull strip
COMMANDS_INFO[10]="Skull Strip"
COMMANDS[10]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate ${afniDilate} ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[11]="3dcalc -a ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[11]="${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"

## despike
COMMANDS_INFO[12]="Despike"
COMMANDS[12]="3dDespike -ssave ${funcDir}/${gFN_FUNCBASE}_spikiness.1D -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## example func
COMMANDS_INFO[13]="Get example func in this case the second image (used later for registration)"
COMMANDS[13]="fslroi ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${funcExample} 1 1"
COMMANDS_OUTPUT[13]="${funcExample}"

## grandmean scaling
COMMANDS_INFO[14]="Grand-mean scaling or intensity normalization"
COMMANDS[14]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin0sm -odt float"
COMMANDS_OUTPUT[14]="${preDir}/7_${gFN_FUNCBASE}_gin0sm.${gOUTPUT_TYPE}"

## temporal filtering
##COMMANDS_INFO[15]="Band-pass filter (get low-frequencies)"
##COMMANDS[15]="fslmaths ${preDir}/7_${gFN_FUNCBASE}_gin -bptf ${hp} ${lp} ${funcPP} -odt float"
##COMMANDS_OUTPUT[15]="${funcPP}"

## temporal filtering
COMMANDS_INFO[15]="Band-pass filter (get low-frequencies)"
COMMANDS[15]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[15]="${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[16]="Detrend"
COMMANDS[16]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[16]="${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE}"
COMMANDS[17]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[17]="${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE}"
COMMANDS[18]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT_0sm}"
COMMANDS_OUTPUT[18]="${funcPP_DT_0sm}"
