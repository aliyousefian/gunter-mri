#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): The Underpants Gnomes.

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${head} ${example_funcExample} ${calibDir}/cal_synthNC_RPI.${gOUTPUT_TYPE}"
BIG_OUTPUT="${regDir}/example_func_NOunwarp_to_highres.${gOUTPUT_TYPE}"

### register example_func to synthetic epi
COMMANDS_INFO[1]="Registering example_functional to calibration image"
COMMANDS[1]="flirt -nosearch -cost normcorr -interp sinc -dof 6 -in ${funcExample} -ref ${calibDir}/cal_synthNC_RPI.${gOUTPUT_TYPE} -omat ${regDir}/example_func_to_synth.mat -out ${regDir}/example_func_to_synth"

### register functional to rho image
COMMANDS_INFO[2]="Registering functional using calibration scan"
COMMANDS[2]="flirt -cost normcorr -interp sinc -dof 6 -in ${regDir}/example_func_to_synth -ref ${calibDir}/cal_rhoRPI.${gOUTPUT_TYPE} -omat ${regDir}/example_func_to_rhoREG.mat -out ${regDir}/example_func_to_rhoREG"

### register undistorted image to highres
COMMANDS_INFO[3]="Registering  undistorted functional to highres"
COMMANDS[3]="flirt -nosearch -cost corratio -interp sinc -dof 6 -in ${calibDir}/cal_rhoRPI -ref ${head} -omat ${calibDir}/rho_to_highres.mat -out ${calibDir}/rho_to_highres"

### register unwarped functional to highres
COMMANDS_INFO[4]="Registering  undistorted functional to highres"
COMMANDS[4]="flirt -in ${regDir}/example_func_to_rhoREG -ref ${head} -applyxfm -init ${calibDir}/rho_to_highres.mat -out ${regDir}/example_func_NOunwarp_to_highres"

### concatenate matrices
COMMANDS[5]="convert_xfm -omat ${regDir}/intermediate.mat -concat ${regDir}/example_func_to_rhoREG.mat ${regDir}/example_func_to_synth.mat"
COMMANDS[6]="convert_xfm -omat ${regDir}/CALIB_example_func_to_highres_NOunwarp.mat -concat ${calibDir}/rho_to_highres.mat ${regDir}/intermediate.mat"
COMMANDS[7]="convert_xfm -omat ${regDir}/CALIB_highres_to_example_func_NOunwarp.mat -inverse ${regDir}/CALIB_example_func_to_highres_NOunwarp.mat"
COMMANDS[8]="convert_xfm -omat ${regDir}/CALIB_example_func_to_standard_NOunwarp.mat -concat ${regDir}/highres2standard.mat ${regDir}/CALIB_example_func_to_highres_NOunwarp.mat"
COMMANDS[9]="convert_xfm -omat ${regDir}/CALIB_standard_to_example_func_NOunwarp.mat -inverse ${regDir}/CALIB_example_func_to_standard_NOunwarp.mat"

