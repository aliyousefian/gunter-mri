#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, Michael Milham, Maarten Mennes, The Underpants Gnomes.

##MAJOR CHANGES ON 11/04/09: CHANGED MOTION CORRECTION TO BASE IMAGE (8TH) RATHER THAN MEAN FUNC
##EXAMPLE FUNC, USED FOR REGISTRATION TO HIGHRES IMAGE NOW HAS SKULL (BECAUSE FOR CALILBRATION-MEDIATED REGISTRATION FOR WAVE 1 SCANS, NEED SKULL)
##TEMPORAL FILTERING NOW BETWEEN 0.009 AND 0.1
##LINEAR AND QUADRATIC DETRENDING ADDED


# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP_DT}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## Dropping first 4 TRS
COMMANDS_INFO[3]="Dropping first 4 TRs"
COMMANDS[3]="3dcalc -a '${funcRaw}[${gTRstart}..${gTRend}]' -expr 'a' -prefix ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[3]="${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## Checking TR MOVED THIS TO AFTER REGISTRATION TO SYNTHETIC FUNC BECAUSE OF INCORRECT TR SPECIFICATION AT THAT POINT
#COMMANDS_INFO[4]="Changing TR specification in header to ensure it's correct"
#COMMANDS[4]="3drefit -TR ${gTR} ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## time shifting slices
COMMANDS_INFO[4]="Timeshifting (interlaced)"
COMMANDS[4]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[4]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[5]="Deobliquing"
COMMANDS[5]="3drefit -deoblique ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[6]="Reorienting to RPI"
COMMANDS[6]="3dresample -orient RPI -inset ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[6]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

## get eighth func image (WITH SKULL) 
COMMANDS_INFO[7]="Get example func (in this case the eigth image) for motion correction and registration"
COMMANDS[7]="fslroi ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} ${funcExample} 7 1"
COMMANDS_OUTPUT[7]="${funcExample}"

##registration to the cal_synth
COMMANDS_INFO[8]="Registering motion corrected functional to synthetic image"
COMMANDS[8]="flirt -in ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} -ref ${calSynth} -applyxfm -init ${regDir}/example_func_to_synth.mat -out ${preDir}/3a_${gFN_FUNCBASE}_to_synth"
COMMANDS_OUTPUT[8]="${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}"

##based on Souheil's recommendation, performing motion correction to cal_synth image (which is also used for registration, and can be used for func and func_2).
## motion correct
COMMANDS_INFO[9]="Motion correction"
COMMANDS[9]="3dvolreg -Fourier -twopass -base ${calSynth} -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[9]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

#unwarping
COMMANDS_INFO[10]="Unwarping functional"
COMMANDS[10]="fugue -i ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} --loadfmap=${calibDir}/cal_reg_boRPI --unwarpdir=x --dwell=1 -u ${preDir}/3c_${gFN_FUNCBASE}_unwarped"
COMMANDS_OUTPUT[10]="${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE}"

## skull strip - LEAVING OUTPUT AS AFNI FILE BECAUSE REFIT FOR TR MIS-SPECIFICATION NOT WORKING ON NIFTI FILES
COMMANDS_INFO[11]="Skull Strip"
COMMANDS[11]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate 1 ${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[11]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[12]="3dcalc -a ${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st"
COMMANDS_OUTPUT[12]="${preDir}/4b_${gFN_FUNCBASE}_st+orig*"

##The registration produces mis-specficication of the TR in the header - moved TR correction here
COMMANDS_INFO[13]="Changing TR specification in header to ensure it's correct"
COMMANDS[13]="3drefit -TR ${gTR} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"

## despike
COMMANDS_INFO[14]="Despike"
COMMANDS[14]="3dDespike -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"
COMMANDS_OUTPUT[14]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## spatial smoothing
COMMANDS_INFO[15]="Spatial smoothing"
COMMANDS[15]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[15]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[16]="Grand-mean scaling or intensity normalization"
COMMANDS[16]="fslmaths ${preDir}/6_${gFN_FUNCBASE}_ss -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin -odt float"
COMMANDS_OUTPUT[16]="${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[17]="Band-pass filter (get low-frequencies)"
COMMANDS[17]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[17]="${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[18]="Detrend"
COMMANDS[18]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[18]="${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"
COMMANDS[19]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[19]="${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE}"
COMMANDS[20]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT}"
COMMANDS_OUTPUT[20]="${funcPP_DT}"

## mask
COMMANDS_INFO[21]="Generating another mask of brain (used later for regression analyses)"
COMMANDS[21]="fslmaths ${funcPP_DT} -Tmin -bin ${funcMask} -odt char"
COMMANDS_OUTPUT[21]="${funcMask}"


## SNR
COMMANDS_INFO[22]="Generating SNR of functional image"
COMMANDS[22]="mkdir -p ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[23]="cd ${checksDir}/1_func"

### get mean image
COMMANDS[24]="3dTstat -mean -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[24]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

### get std image
COMMANDS[25]="3dTstat -stdev -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[25]="${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE}"

## get SNR image
COMMANDS[26]="3dcalc -a ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} -expr \"a/b\" -prefix ${checksDir}/1_func/snr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[26]="${checksDir}/1_func/snr.${gOUTPUT_TYPE}"

## spit something out for user
COMMANDS[27]="meanSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M \`"
COMMANDS[28]="sdevSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -S\`"
COMMANDS[29]="zimportant \"${subject} mean SNR is \${meanSNR} and standard deviation of SNR is \${sdevSNR}\""
COMMANDS[30]="fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M -S > ${checksDir}/1_func/temp_snr.1D"

## get images and put into checks directory
COMMANDS[31]="mkdir ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[32]="slicer ${checksDir}/1_func/${gFN_FUNCBASE}_mean -s 2 -A 750 ${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS_OUTPUT[32]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS[33]="maxSNR=\`3dBrickStat -max ${checksDir}/1_func/snr.${gOUTPUT_TYPE}\`"
COMMANDS[34]="overlay 1 0 ${checksDir}/1_func/${gFN_FUNCBASE}_mean -a ${checksDir}/1_func/snr 0 \${maxSNR} ${checksDir}/1_func/rendered_snr"
COMMANDS_OUTPUT[34]="${checksDir}/1_func/rendered_snr.${gOUTPUT_TYPE}"
COMMANDS[35]="slicer ${checksDir}/1_func/rendered_snr -s 2 -A 750 ${checksDir}/1_func/snr.png"
COMMANDS_OUTPUT[35]="${checksDir}/1_func/snr.png"

# Seperate motion parameters into seperate files
COMMANDS_INFO[36]="Setting up motion covariates in seperate files"
COMMANDS[36]="awk '{print \$1}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS_OUTPUT[36]="${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS[37]="awk '{print \$2}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS_OUTPUT[37]="${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS[38]="awk '{print \$3}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS_OUTPUT[38]="${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS[39]="awk '{print \$4}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS_OUTPUT[39]="${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS[40]="awk '{print \$5}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS_OUTPUT[40]="${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS[41]="awk '{print \$6}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"
COMMANDS_OUTPUT[41]="${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"

##Get maximum displacement value
COMMANDS[42]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D > ${checksDir}/1_func/temp"
COMMANDS_OUTPUT[42]="${checksDir}/1_func/temp"
COMMANDS[43]="value=\`cat ${checksDir}/1_func/temp \`"
COMMANDS_OUTPUT[43]="value"
COMMANDS[44]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"
COMMANDS_OUTPUT[44]="${checksDir}/1_func/max_mov.1D"

##Get maximum 6 motion params
COMMANDS[45]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc1.1D > ${checksDir}/1_func/temp1"
COMMANDS_OUTPUT[45]="${checksDir}/1_func/temp1"
COMMANDS[46]="value=\`cat ${checksDir}/1_func/temp1 \`" 
COMMANDS[47]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[48]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc2.1D > ${checksDir}/1_func/temp2"
COMMANDS_OUTPUT[48]="${checksDir}/1_func/temp2"
COMMANDS[49]="value=\`cat ${checksDir}/1_func/temp2 \`" 
COMMANDS[50]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[51]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc3.1D > ${checksDir}/1_func/temp3"
COMMANDS_OUTPUT[51]="${checksDir}/1_func/temp3"
COMMANDS[52]="value=\`cat ${checksDir}/1_func/temp3 \`" 
COMMANDS[53]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[54]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc4.1D > ${checksDir}/1_func/temp4"
COMMANDS_OUTPUT[54]="${checksDir}/1_func/temp4"
COMMANDS[55]="value=\`cat ${checksDir}/1_func/temp4 \`" 
COMMANDS[56]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[57]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc5.1D > ${checksDir}/1_func/temp5"
COMMANDS_OUTPUT[57]="${checksDir}/1_func/temp5"
COMMANDS[58]="value=\`cat ${checksDir}/1_func/temp5 \`" 
COMMANDS[59]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[60]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc6.1D > ${checksDir}/1_func/temp6"
COMMANDS_OUTPUT[60]="${checksDir}/1_func/temp6"
COMMANDS[61]="value=\`cat ${checksDir}/1_func/temp6 \`" 
COMMANDS[62]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[63]="sed 's/ /\t/' < ${checksDir}/1_func/temp_snr.1D > ${checksDir}/1_func/snr.1D"

COMMANDS[64]="rm ${checksDir}/1_func/temp* ${funcDir}/${gFN_MCBASE}*.${gTS_EXT}"

