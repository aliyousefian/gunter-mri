#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, Michael Milham, Maarten Mennes, The Underpants Gnomes.

##MAJOR CHANGES ON 11/04/09: CHANGED MOTION CORRECTION TO BASE IMAGE (8TH) RATHER THAN MEAN FUNC
##EXAMPLE FUNC, USED FOR REGISTRATION TO HIGHRES IMAGE NOW HAS SKULL (BECAUSE FOR CALILBRATION-MEDIATED REGISTRATION FOR WAVE 1 SCANS, NEED SKULL)
##TEMPORAL FILTERING NOW BETWEEN 0.009 AND 0.1
##LINEAR AND QUADRATIC DETRENDING ADDED


# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP_DT}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## Dropping first 4 TRS
COMMANDS_INFO[3]="Dropping first 4 TRs"
COMMANDS[3]="3dcalc -a '${funcRaw}[${gTRstart}..${gTRend}]' -expr 'a' -prefix ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[3]="${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## Checking TR
COMMANDS_INFO[4]="Changing TR specification in header to ensure it's correct"
COMMANDS[4]="3drefit -TR ${gTR} ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## time shifting slices
COMMANDS_INFO[5]="Timeshifting (interlaced)"
COMMANDS[5]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[5]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[6]="Deobliquing"
COMMANDS[6]="3drefit -deoblique ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[7]="Reorienting to RPI"
COMMANDS[7]="3dresample -orient RPI -inset ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[7]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

## get eighth func image (WITH SKULL) 
COMMANDS_INFO[8]="Get example func (in this case the eigth image) for motion correction and registration"
COMMANDS[8]="fslroi ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} ${funcExample} 7 1"
COMMANDS_OUTPUT[8]="${funcExample}"

##based on Souheil's recommendation, performing motion correction to cal_synth image (which is also used for registration, and can be used for func and func_2).

## motion correct
COMMANDS_INFO[9]="Motion correction"
COMMANDS[9]="3dvolreg -Fourier -twopass -base ${calSynth} -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[9]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

##registration to the cal_synth and unwarping would need to be applied here.

## skull strip
COMMANDS_INFO[10]="Skull Strip"
COMMANDS[10]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate 1 ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[11]="3dcalc -a ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[11]="${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"

## despike
COMMANDS_INFO[12]="Despike"
COMMANDS[12]="3dDespike -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## spatial smoothing
COMMANDS_INFO[13]="Spatial smoothing"
COMMANDS[13]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[13]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[14]="Grand-mean scaling or intensity normalization"
COMMANDS[14]="fslmaths ${preDir}/6_${gFN_FUNCBASE}_ss -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin -odt float"
COMMANDS_OUTPUT[14]="${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[15]="Band-pass filter (get low-frequencies)"
COMMANDS[15]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[15]="${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[16]="Detrend"
COMMANDS[16]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[16]="${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"
COMMANDS[17]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[17]="${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE}"
COMMANDS[18]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT}"
COMMANDS_OUTPUT[18]="${funcPP_DT}"

## mask
COMMANDS_INFO[19]="Generating another mask of brain (used later for regression analyses)"
COMMANDS[19]="fslmaths ${funcPP_DT} -Tmin -bin ${funcMask} -odt char"
COMMANDS_OUTPUT[19]="${funcMask}"


## SNR
COMMANDS_INFO[20]="Generating SNR of functional image"
COMMANDS[20]="mkdir -p ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[21]="cd ${checksDir}/1_func"

### get mean image
COMMANDS[22]="3dTstat -mean -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[22]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

### get std image
COMMANDS[23]="3dTstat -stdev -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[23]="${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE}"

## get SNR image
COMMANDS[24]="3dcalc -a ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} -expr \"a/b\" -prefix ${checksDir}/1_func/snr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[24]="${checksDir}/1_func/snr.${gOUTPUT_TYPE}"

## spit something out for user
COMMANDS[25]="meanSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M \`"
COMMANDS[26]="sdevSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -S\`"
COMMANDS[27]="zimportant \"${subject} mean SNR is \${meanSNR} and standard deviation of SNR is \${sdevSNR}\""
COMMANDS[28]="fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M -S > ${checksDir}/1_func/temp_snr.1D"

## get images and put into checks directory
COMMANDS[29]="mkdir ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[30]="slicer ${checksDir}/1_func/${gFN_FUNCBASE}_mean -s 2 -A 750 ${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS_OUTPUT[30]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS[31]="maxSNR=\`3dBrickStat -max ${checksDir}/1_func/snr.${gOUTPUT_TYPE}\`"
COMMANDS[32]="overlay 1 0 ${checksDir}/1_func/${gFN_FUNCBASE}_mean -a ${checksDir}/1_func/snr 0 \${maxSNR} ${checksDir}/1_func/rendered_snr"
COMMANDS_OUTPUT[32]="${checksDir}/1_func/rendered_snr.${gOUTPUT_TYPE}"
COMMANDS[33]="slicer ${checksDir}/1_func/rendered_snr -s 2 -A 750 ${checksDir}/1_func/snr.png"
COMMANDS_OUTPUT[33]="${checksDir}/1_func/snr.png"

# Seperate motion parameters into seperate files
COMMANDS_INFO[34]="Setting up motion covariates in seperate files"
COMMANDS[34]="awk '{print \$1}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS_OUTPUT[34]="${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS[35]="awk '{print \$2}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS_OUTPUT[35]="${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS[36]="awk '{print \$3}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS_OUTPUT[36]="${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS[37]="awk '{print \$4}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS_OUTPUT[37]="${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS[38]="awk '{print \$5}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS_OUTPUT[38]="${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS[39]="awk '{print \$6}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"
COMMANDS_OUTPUT[39]="${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"

##Get maximum displacement value
COMMANDS[40]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D > ${checksDir}/1_func/temp"
COMMANDS_OUTPUT[40]="${checksDir}/1_func/temp"
COMMANDS[41]="value=\`cat ${checksDir}/1_func/temp \`"
COMMANDS_OUTPUT[41]="value"
COMMANDS[42]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"
COMMANDS_OUTPUT[42]="${checksDir}/1_func/max_mov.1D"

##Get maximum 6 motion params
COMMANDS[43]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc1.1D > ${checksDir}/1_func/temp1"
COMMANDS_OUTPUT[43]="${checksDir}/1_func/temp1"
COMMANDS[44]="value=\`cat ${checksDir}/1_func/temp1 \`" 
COMMANDS[45]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[46]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc2.1D > ${checksDir}/1_func/temp2"
COMMANDS_OUTPUT[46]="${checksDir}/1_func/temp2"
COMMANDS[47]="value=\`cat ${checksDir}/1_func/temp2 \`" 
COMMANDS[48]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[49]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc3.1D > ${checksDir}/1_func/temp3"
COMMANDS_OUTPUT[49]="${checksDir}/1_func/temp3"
COMMANDS[50]="value=\`cat ${checksDir}/1_func/temp3 \`" 
COMMANDS[51]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[52]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc4.1D > ${checksDir}/1_func/temp4"
COMMANDS_OUTPUT[52]="${checksDir}/1_func/temp4"
COMMANDS[53]="value=\`cat ${checksDir}/1_func/temp4 \`" 
COMMANDS[54]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[55]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc5.1D > ${checksDir}/1_func/temp5"
COMMANDS_OUTPUT[55]="${checksDir}/1_func/temp5"
COMMANDS[56]="value=\`cat ${checksDir}/1_func/temp5 \`" 
COMMANDS[57]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[58]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc6.1D > ${checksDir}/1_func/temp6"
COMMANDS_OUTPUT[58]="${checksDir}/1_func/temp6"
COMMANDS[59]="value=\`cat ${checksDir}/1_func/temp6 \`" 
COMMANDS[60]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[61]="sed 's/ /\t/' < ${checksDir}/1_func/temp_snr.1D > ${checksDir}/1_func/snr.1D"

COMMANDS[62]="rm ${checksDir}/1_func/temp* ${funcDir}/${gFN_MCBASE}*.${gTS_EXT}"

