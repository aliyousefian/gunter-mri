#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): The Underpants Gnomes.

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${head} ${example_funcExample} ${calSynth}"
BIG_OUTPUT="${regDir}/example_func_unwarp_to_highres.${gOUTPUT_TYPE}"

### register example_func to synthetic epi -  this registration is then applied to the motion corrected image
COMMANDS_INFO[1]="Registering example_functional to calibration image"
COMMANDS[1]="flirt -nosearch -cost normcorr -interp sinc -dof 6 -in ${funcExample} -ref ${calSynth} -omat ${regDir}/example_func_to_synth.mat -out ${regDir}/example_func_to_synth"

### unwarp functional - this unwarping is also done to the motion corrected image. The resulting unwarped functional is then preprocessed as normal
### x is the correct unwarping direction when the images have been reoriented
COMMANDS_INFO[2]="Unwarping functional using calibration scan"
COMMANDS[2]="fugue -i ${regDir}/example_func_to_synth --loadfmap=${calibDir}/cal_reg_boRPI --unwarpdir=x --dwell=1 -u ${regDir}/example_func_to_rho"

##this unwarped functional is actually example_func and is copied into reg directory as such
COMMANDS[3]="cp ${regDir}/example_func_to_rho.${gOUTPUT_TYPE} ${regDir}/example_func.${gOUTPUT_TYPE}"

### register undistorted image to highres - this is the registration that will be applied to the fully preprocessed image 
###i.e., rho_to_highres is the same as example_func_to_highres when functional has been unwarped 
COMMANDS_INFO[4]="Registering  undistorted functional to highres"
COMMANDS[4]="flirt -nosearch -cost corratio -interp sinc -dof 6 -in ${calibDir}/cal_rhoRPI -ref ${head} -omat ${calibDir}/rho_to_highres.mat -out ${calibDir}/rho_to_highres"

### register unwarped functional to highres
COMMANDS_INFO[5]="Registering  undistorted functional to highres"
COMMANDS[5]="flirt -in ${regDir}/example_func_to_rho -ref ${head} -applyxfm -init ${calibDir}/rho_to_highres.mat -out ${regDir}/example_func_unwarp_to_highres"

### concatenate matrices
COMMANDS[6]="cp ${calibDir}/rho_to_highres.mat ${regDir}/CALIB_example_func_to_highres_unwarp.mat"
COMMANDS[7]="convert_xfm -omat ${regDir}/CALIB_highres_to_example_func_unwarp.mat -inverse ${regDir}/CALIB_example_func_to_highres_unwarp.mat"
COMMANDS[8]="convert_xfm -omat ${regDir}/CALIB_example_func_to_standard_unwarp.mat -concat ${regDir}/highres2standard.mat ${regDir}/CALIB_example_func_to_highres_unwarp.mat"
COMMANDS[9]="convert_xfm -omat ${regDir}/CALIB_standard_to_example_func_unwarp.mat -inverse ${regDir}/CALIB_example_func_to_standard_unwarp.mat"
