#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Reza

#MAJOR REVISION August 2012: Output after (Dropping first 6 TRS, time shifting slices, deoblique, orienting into fsl friendly space, motion correction and skull strip removal) is used as input of this function. Spatial smoothing and grandmean scaling is preformed and output is produced  

# Initial input and Final output
BIG_INPUT="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
BIG_OUTPUT="${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA.${gOUTPUT_TYPE} ${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA.${gOUTPUT_TYPE} ${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA_nStandard.${gOUTPUT_TYPE} ${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA_nStandard.${gOUTPUT_TYPE}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)
mkdir ${preDir}/ICA
## spatial smoothing
COMMANDS_INFO[1]="Spatial smoothing"
COMMANDS[1]="fslmaths ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[1]="${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[2]="Grand-mean scaling or intensity normalization"
COMMANDS[2]="fslmaths ${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA.${gOUTPUT_TYPE} -ing 10000 ${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA.${gOUTPUT_TYPE} -odt float"
COMMANDS_OUTPUT[2]="${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA.${gOUTPUT_TYPE}"

# Transforming output after spatial smoothing to standard space
COMMANDS_INFO[3]="Transforming to standard space"
COMMANDS[3]="applywarp --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --in=${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA.${gOUTPUT_TYPE} --out=${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA_nStandard.${gOUTPUT_TYPE} --warp=${anatregDir}/highres2standard_warp --premat=${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[3]="${preDir}/ICA/${gFN_FUNCBASE}_ss_ICA_nStandard.${gOUTPUT_TYPE}"

# Transforming output after grandmean scaling to standard space
COMMANDS_INFO[4]="Transforming to standard space"
COMMANDS[4]="applywarp --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --in=${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA.${gOUTPUT_TYPE} --out=${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA_nStandard.${gOUTPUT_TYPE} --warp=${anatregDir}/highres2standard_warp --premat=${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[4]="${preDir}/ICA/${gFN_FUNCBASE}_ssnorm_ICA_nStandard.${gOUTPUT_TYPE}"

