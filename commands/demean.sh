#!/bin/bash

#14Sept 2010, chief underpants gnome

# =========================================================== #

SC=10            # Scale to be used by bc. Nine decimal places.

# ----------------- Set data file ---------------------
datafile="$1" #  ASCII text file,
outputfile="$2"

rt=0         # Running total.
am=0         # Arithmetic mean.
x=0	
  
while read value   # Read one data point at a time.
do  
      x=$(( $x + 1 ))

      rt=$(echo "scale=$SC; $rt + $value" | bc)

done <"$datafile"

mean=$(echo "scale=$SC; $rt / $x" | bc)

echo $mean

while read value   # Read one line at a time.
  do
 
    	diff=$(echo "scale=$SC; $value - $mean" | bc)
	
	printf "%.9f \r\n" $diff >>$outputfile
	
done <"$datafile"   
