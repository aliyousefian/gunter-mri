#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 11/26/08
# AUTHOR(S): Zarrar Shehzad

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial input
BIG_INPUT="${regDir} ${checksDir}/1_func/${gFN_FUNCBASE}_snr.${gOUTPUT_TYPE}"
BIG_OUTPUT="${checksDir}/1_func/${gFN_FUNCBASE}_snr2standard"

COMMANDS_INFO[1]="Applying registration"
COMMANDS[1]="flirt -ref ${regDir}/standard -in ${checksDir}/1_func/${gFN_FUNCBASE}_snr -out ${checksDir}/1_func/${gFN_FUNCBASE}_snr2standard -applyxfm -init ${regDir}/example_func2standard.mat -interp trilinear"
