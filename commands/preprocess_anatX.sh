#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/22/08
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, and Michael Milham

# Variables
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# 3 Variables: anatDir, mprage, brain that can be replaced.
# ? will use a different subject file

# INITIAL INPUT AND FINAL OUTPUT
BIG_INPUT="${anatDir} ${mprage}"
BIG_OUTPUT="${brain}"

# COMMANDS
## go into anatomical directory
COMMANDS[1]="cd ${anatDir}"

## deoblique
COMMANDS_INFO[2]="deobliquing anatomical"
COMMANDS[2]="3dcopy ${mprage} tmp_anat_do.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[2]="tmp_anat_do.${gOUTPUT_TYPE}"
COMMANDS[3]="3drefit -deoblique tmp_anat_do.${gOUTPUT_TYPE}"

## reorient
COMMANDS_INFO[4]="reorienting anatomical"
COMMANDS[4]="3dresample -orient RPI -inset tmp_anat_do.${gOUTPUT_TYPE} -prefix ${head}"
COMMANDS_OUTPUT[4]="${head}"

## skull strip
COMMANDS_INFO[5]="skull stripping anatomical"
#keep the orginal intensity to avoid exclude the inside CSF. (Xinian Zuo)
COMMANDS[5]="3dSkullStrip -input ${head} -orig_vol -o_ply tmp_anat_surf.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[5]="tmp_anat_surf.${gOUTPUT_TYPE}"
COMMANDS[6]="3dcalc -a ${head} -b tmp_anat_surf.${gOUTPUT_TYPE} -expr 'a*step(b)' -prefix ${brain}"
COMMANDS_OUTPUT[6]="${brain}"

## take pretty pictures of mprage
COMMANDS_INFO[7]="taking pretty pictures of head"
COMMANDS[7]="mkdir ${anatDir}/${gDIR_CHECKS} 2> /dev/null; echo -n ''"
COMMANDS[8]="slicer ${head} -S 10 1200 ${anatDir}/${gDIR_CHECKS}/head.png"
COMMANDS_OUTPUT[8]="${anatDir}/${gDIR_CHECKS}/head.png"

## take pretty pictures of brain mask overlayed onto mprage
COMMANDS_INFO[9]="overlaying mask on head and taking pretty pictures"
COMMANDS[9]="overlay 1 1 ${head} -a tmp_anat_surf.${gOUTPUT_TYPE} 1 1 ${anatDir}/${gDIR_CHECKS}/rendered_mask"
COMMANDS_OUTPUT[9]="${anatDir}/${gDIR_CHECKS}/rendered_mask.${gOUTPUT_TYPE}"
COMMANDS[10]="slicer ${anatDir}/${gDIR_CHECKS}/rendered_mask -S 10 1200 ${anatDir}/${gDIR_CHECKS}/skull_strip.png"
COMMANDS_OUTPUT[10]="${anatDir}/${gDIR_CHECKS}/skull_strip.png"
