#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, Michael Milham, Maarten Mennes, The Underpants Gnomes.

# Initial input and Final output
BIG_INPUT="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} ${calSynth} ${regDir}/example_func2synth.mat ${calibDir}/cal_reg_boRPI.${gOUTPUT_TYPE}"
BIG_OUTPUT="${funcPP_DT}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)

##registration to the cal_synth
COMMANDS_INFO[1]="Registering functional to synthetic image"
COMMANDS[1]="flirt -in ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} -ref ${calSynth} -applyxfm -init ${regDir}/example_func2synth.mat -out ${preDir}/3a_${gFN_FUNCBASE}_to_synth"
COMMANDS_OUTPUT[1]="${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}"

##based on Souheil's recommendation, performing motion correction to cal_synth image (which is also used for registration, and can be used for func and func_2).
## motion correct
COMMANDS_INFO[2]="Motion correction"
COMMANDS[2]="3dvolreg -Fourier -twopass -base ${calSynth} -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/3a_${gFN_FUNCBASE}_to_synth.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[2]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

#unwarping
COMMANDS_INFO[3]="Unwarping functional"
COMMANDS[3]="fugue -i ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} --loadfmap=${calibDir}/cal_reg_boRPI --unwarpdir=x --dwell=1 -u ${preDir}/3c_${gFN_FUNCBASE}_unwarped"
COMMANDS_OUTPUT[3]="${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE}"

## skull strip - LEAVING OUTPUT AS AFNI FILE BECAUSE REFIT FOR TR MIS-SPECIFICATION NOT WORKING ON NIFTI FILES
COMMANDS_INFO[4]="Skull Strip"
COMMANDS[4]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate 1 ${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[4]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[5]="3dcalc -a ${preDir}/3c_${gFN_FUNCBASE}_unwarped.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st"
COMMANDS_OUTPUT[5]="${preDir}/4b_${gFN_FUNCBASE}_st+orig.*"

##The registration produces mis-specficication of the TR in the header - TR correction done here
COMMANDS_INFO[6]="Changing TR specification in header to ensure it's correct"
COMMANDS[6]="3drefit -TR ${gTR} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"

## despike
COMMANDS_INFO[7]="Despike"
COMMANDS[7]="3dDespike -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"
COMMANDS_OUTPUT[7]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## spatial smoothing
COMMANDS_INFO[8]="Spatial smoothing"
COMMANDS[8]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[8]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[9]="Grand-mean scaling or intensity normalization"
COMMANDS[9]="fslmaths ${preDir}/6_${gFN_FUNCBASE}_ss -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin -odt float"
COMMANDS_OUTPUT[9]="${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[10]="Band-pass filter (get low-frequencies)"
COMMANDS[10]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[11]="Detrend"
COMMANDS[11]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[11]="${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"
COMMANDS[12]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE}"
COMMANDS[13]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT}"
COMMANDS_OUTPUT[13]="${funcPP_DT}"

## mask
COMMANDS_INFO[14]="Generating another mask of brain (used later for regression analyses)"
COMMANDS[14]="fslmaths ${funcPP_DT} -Tmin -bin ${funcMask} -odt char"
COMMANDS_OUTPUT[14]="${funcMask}"


## SNR
COMMANDS_INFO[15]="Generating SNR of functional image"
COMMANDS[15]="mkdir -p ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[16]="cd ${checksDir}/1_func"

### get mean image
COMMANDS[17]="3dTstat -mean -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"
COMMANDS_OUTPUT[17]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

### get std image
COMMANDS[18]="3dTstat -stdev -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st+orig"
COMMANDS_OUTPUT[18]="${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE}"

## get SNR image
COMMANDS[19]="3dcalc -a ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} -expr \"a/b\" -prefix ${checksDir}/1_func/snr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[19]="${checksDir}/1_func/snr.${gOUTPUT_TYPE}"

## spit something out for user
COMMANDS[20]="meanSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M \`"
COMMANDS[21]="sdevSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -S\`"
COMMANDS[22]="zimportant \"${subject} mean SNR is \${meanSNR} and standard deviation of SNR is \${sdevSNR}\""
COMMANDS[23]="fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M -S > ${checksDir}/1_func/temp_snr.1D"

## get images and put into checks directory
COMMANDS[24]="mkdir ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[25]="slicer ${checksDir}/1_func/${gFN_FUNCBASE}_mean -s 2 -A 750 ${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS_OUTPUT[25]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS[26]="maxSNR=\`3dBrickStat -max ${checksDir}/1_func/snr.${gOUTPUT_TYPE}\`"
COMMANDS[27]="overlay 1 0 ${checksDir}/1_func/${gFN_FUNCBASE}_mean -a ${checksDir}/1_func/snr 0 \${maxSNR} ${checksDir}/1_func/rendered_snr"
COMMANDS_OUTPUT[27]="${checksDir}/1_func/rendered_snr.${gOUTPUT_TYPE}"
COMMANDS[28]="slicer ${checksDir}/1_func/rendered_snr -s 2 -A 750 ${checksDir}/1_func/snr.png"
COMMANDS_OUTPUT[28]="${checksDir}/1_func/snr.png"

# Seperate motion parameters into seperate files
COMMANDS_INFO[29]="Setting up motion covariates in seperate files"
COMMANDS[29]="awk '{print \$1}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS_OUTPUT[29]="${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS[30]="awk '{print \$2}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS_OUTPUT[30]="${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS[31]="awk '{print \$3}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS_OUTPUT[31]="${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS[32]="awk '{print \$4}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS_OUTPUT[32]="${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS[33]="awk '{print \$5}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS_OUTPUT[33]="${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS[34]="awk '{print \$6}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"
COMMANDS_OUTPUT[34]="${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"

##Get maximum displacement value
COMMANDS[35]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D > ${checksDir}/1_func/temp"
COMMANDS_OUTPUT[35]="${checksDir}/1_func/temp"
COMMANDS[36]="value=\`cat ${checksDir}/1_func/temp \`"
COMMANDS_OUTPUT[36]="value"
COMMANDS[37]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"
COMMANDS_OUTPUT[37]="${checksDir}/1_func/max_mov.1D"

##Get maximum 6 motion params
COMMANDS[38]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc1.1D > ${checksDir}/1_func/temp1"
COMMANDS_OUTPUT[38]="${checksDir}/1_func/temp1"
COMMANDS[39]="value=\`cat ${checksDir}/1_func/temp1 \`" 
COMMANDS[40]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[41]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc2.1D > ${checksDir}/1_func/temp2"
COMMANDS_OUTPUT[41]="${checksDir}/1_func/temp2"
COMMANDS[42]="value=\`cat ${checksDir}/1_func/temp2 \`" 
COMMANDS[43]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[44]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc3.1D > ${checksDir}/1_func/temp3"
COMMANDS_OUTPUT[44]="${checksDir}/1_func/temp3"
COMMANDS[45]="value=\`cat ${checksDir}/1_func/temp3 \`" 
COMMANDS[46]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[47]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc4.1D > ${checksDir}/1_func/temp4"
COMMANDS_OUTPUT[47]="${checksDir}/1_func/temp4"
COMMANDS[48]="value=\`cat ${checksDir}/1_func/temp4 \`" 
COMMANDS[49]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[50]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc5.1D > ${checksDir}/1_func/temp5"
COMMANDS_OUTPUT[50]="${checksDir}/1_func/temp5"
COMMANDS[51]="value=\`cat ${checksDir}/1_func/temp5 \`" 
COMMANDS[52]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[53]="awk 'max==\"\" || \$1 > max {max=\$1} END{ print max}' ${funcDir}/mc6.1D > ${checksDir}/1_func/temp6"
COMMANDS_OUTPUT[53]="${checksDir}/1_func/temp6"
COMMANDS[54]="value=\`cat ${checksDir}/1_func/temp6 \`" 
COMMANDS[55]="printf \"%.2f\t\" \${value} >> ${checksDir}/1_func/max_mov.1D"

COMMANDS[56]="sed 's/ /\t/' < ${checksDir}/1_func/temp_snr.1D > ${checksDir}/1_func/snr.1D"

COMMANDS[57]="rm ${checksDir}/1_func/temp* ${funcDir}/${gFN_MCBASE}*.${gTS_EXT}"

