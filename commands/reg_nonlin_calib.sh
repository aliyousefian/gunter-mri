#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/29/08
# AUTHOR(S): Zarrar Shehzad

# THIS REQUIRES THAT reg_calib HAS BEEN RUN!!!

#MAJOR REVISION 2ND MARCH 2010: CREATED SEPARATE REGISTRATION DIRECTORIES FOR FUNC AND ANAT

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
#BIG_INPUT="${mprage} ${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz ${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask_dil.nii.gz ${regDir}/highres2standard.mat ${regDir}/example_func2standard.mat"
BIG_INPUT="${mprage} ${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz ${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask_dil.nii.gz"
BIG_OUTPUT="${funcregDir}/fnirt_example_func2standard.${gOUTPUT_TYPE}"

# Perform nonlinear registraion (higres to standard)
COMMANDS[1]="fnirt --in=${head} --aff=${anatregDir}/highres2standard.mat --cout=${anatregDir}/highres2standard_warp --iout=${anatregDir}/fnirt_highres2standard --jout=${anatregDir}/highres2standard_jac --config=T1_2_MNI152_2mm --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --refmask=${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask_dil --warpres=10,10,10"
COMMANDS_OUTPUT[1]="${anatregDir}/highres2standard_warp.${gOUTPUT_TYPE}"

# Apply nonlinear registration (func to standard)
COMMANDS[2]="applywarp --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --in=${funcregDir}/example_func.${gOUTPUT_TYPE} --out=${funcregDir}/fnirt_example_func2standard --warp=${anatregDir}/highres2standard_warp --premat=${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[2]="${funcregDir}/fnirt_example_func2standard.${gOUTPUT_TYPE}"
