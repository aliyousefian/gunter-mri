#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/24/08
# AUTHOR(S): Zarrar Shehzad

# Command Vars
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${maskFile} ${funcResEStandard}"
BIG_OUTPUT="${tsFile}.${gTS_EXT}"

# Making Seeds and Feats directories in your projects directory
COMMANDS_INFO[1]="Creating the subject directory in ${gDIR_PROJECTS}"
COMMANDS[1]="mkdir ${iProjSubDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[2]="Creating the func directory in ${iProjSubDir}"
COMMANDS[2]="mkdir ${iProjDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[3]="Creating the seeds directory"
COMMANDS[3]="mkdir ${tsDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[4]="Creating the feats directory"
COMMANDS[4]="mkdir ${featsDir} 2> /dev/null; echo -n ''"

# Obtain average time series of a region
COMMANDS_INFO[5]="Extracting mean time series"
COMMANDS[5]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${funcResEStandard} > ${tsFile}.${gTS_EXT}"

COMMANDS_INFO[6]="Checking mean time series"
COMMANDS[6]="x_checkTs.sh -i ${tsFile}.${gTS_EXT} -v ${gVOLNUM} -d"

COMMANDS_INFO[7]="Scaling time series"
COMMANDS[7]="/frodo/shared/gunther-mri/commands/standardDeviation.sh ${tsFile}.${gTS_EXT} ${tsFile}S.${gTS_EXT}"

COMMANDS_INFO[8]="Deleting unscaled time series"
COMMANDS[8]="rm ${tsFile}.${gTS_EXT}"

COMMANDS_INFO[8]="Renaming scaled time series"
COMMANDS[8]="mv ${tsFile}S.${gTS_EXT} ${tsFile}.${gTS_EXT}"
