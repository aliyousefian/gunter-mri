#!/usr/bin/env bash

# do singlesubject analysis using 3dfim+
# written by Maarten Mennes, Clare Kelly and Xinian Zuo
# called from /frodo/fmri/projects/YOURPROJECT/scripts/DoSingleSubject.sh


## inputs
subject_basedir=$1
project_dir=$2
func=$3
image_for_correlation=$4
image_for_TSextraction=$5
seed_list=$6
mm=$7
final_mm=$8
registration=$9
# shift arguments to left because subject_list calls for argument $10 which bash cannot handle
shift
subject_list=$9



#### SCRIPT SCRIPT SCRIPT SCRIPT SCRIPT ####
#
if [ -d ${project_dir}/SingleSubs ]
then
echo
else
mkdir ${project_dir}/SingleSubs
fi

if [ -d ${project_dir}/SingleSubs/${func} ]
then
echo
else
mkdir -p ${project_dir}/SingleSubs/${func}
fi

seeds=$( cat ${seed_list} )
subjects=$( cat ${subject_list} )

### LOOP 1.1: For each subject
for i in $subjects
do

mkdir -p ${project_dir}/${i}/5a_seeds
mkdir -p ${project_dir}/${i}/7_corrs

echo --------------------
echo RUNNING SUBJECT ${i}
echo

## LOOP 1.2: For each seed
for s in $seeds
do

	# check if seed is already done - file needs to be removed from SingleSubs for analysis to run
	if [ -f ${project_dir}/${i}/7_corrs/${s}_Z_fl2standard.nii.gz ] || [ -f ${project_dir}/${i}/7_corrs/${s}_Z_fn2standard.nii.gz ]
	then
	echo SUBJECT ANALYSIS FOR SEED ${s} ALREADY DONE
	else


	# EXTRACT TIMESERIES
	echo "Extracting timeseries for seed ${s}"
	touch ${project_dir}/SingleSubs/${func}/${s}
	if [ "${mm}" = "1mm" ]
	then
		echo 1mm
		echo $image_for_TSextraction > temp
		sed 's/.nii.gz//' < temp > temp2
		image_for_TSextraction=$( cat temp2 )
		
		# delete timeseries in case they exist already
		rm ${project_dir}/${i}/5a_seeds/${s}.1D		

		for part in 1 2 3 4
		do
			3dROIstats -quiet -mask_f2short -mask /frodo/shared/masks/${mm}/${s}.nii.gz ${subject_basedir}/${i}/${image_for_TSextraction}_${part}.nii.gz >> ${project_dir}/${i}/5a_seeds/${s}.1D
		done
		rm temp temp2
	else
	3dROIstats -quiet -mask_f2short -mask /frodo/shared/masks/${mm}/${s}.nii.gz ${subject_basedir}/${i}/${image_for_TSextraction} > ${project_dir}/${i}/5a_seeds/${s}.1D
	fi

	
	# COMPUTE CORRELATION
	echo "Computing Correlation for seed ${s}"
	3dfim+ -input ${subject_basedir}/${i}/${image_for_correlation} -ideal_file ${project_dir}/${i}/5a_seeds/${s}.1D -fim_thr 0.0009 -out Correlation -bucket ${project_dir}/${i}/7_corrs/${s}_corr.nii.gz
	

	# FISHER Z-TRANSFORM
	echo "Z-transforming seed ${s}"
	3dcalc -a ${project_dir}/${i}/7_corrs/${s}_corr.nii.gz -expr 'log((1+a)/(1-a))/2' -prefix ${project_dir}/${i}/7_corrs/${s}_Z.nii.gz


	# ADD BARTLET TRANSFORMATION
	#echo "still need to add this correction factor"
	# /frodo/shared/gunther-mri/commands/bartletcorrection.sh
	

	# REGISTER Z TO STANDARD SPACE
	echo "Registering Z-transformed map to standard space"

	if [ "${registration}" = "FLIRT" ]; then
	echo "Registering using FLIRT"
	flirt -in ${project_dir}/${i}/7_corrs/${s}_Z.nii.gz -ref ${FSLDIR}/data/standard/MNI152_T1_${final_mm}_brain.nii.gz -applyxfm -init ${subject_basedir}/${i}/2_reg/example_func2standard.mat -out ${project_dir}/${i}/7_corrs/${s}_Z_fl2standard.nii.gz
	fi

	if [ "${registration}" = "FNIRT" ]; then
	echo "Registering using FNIRT"
	sub=$( echo ${i} | cut -f 1 -d / )
	applywarp --ref=/frodo/shared/fsl/data/standard/MNI152_T1_${final_mm}.nii.gz \
	--in=${project_dir}/${i}/7_corrs/${s}_Z.nii.gz \
	--out=${project_dir}/${i}/7_corrs/${s}_Z_fn2standard.nii.gz \
	--warp=${subject_basedir}/${sub}/anat/reg/highres2standard_warp \
	--premat=${subject_basedir}/${i}/2_reg/example_func2highres.mat
	fi

	# end of check for file
	# white line
	echo
	fi


## END OF LOOP 1.2: seed
done

### END OF LOOP 1.1: subject
done
