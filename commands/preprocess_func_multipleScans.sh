#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, Michael Milham, Maarten Mennes, The Underpants Gnomes.

##MAJOR CHANGES ON 11/04/09: CHANGED MOTION CORRECTION TO BASE IMAGE (8TH) RATHER THAN MEAN FUNC
##EXAMPLE FUNC, USED FOR REGISTRATION TO HIGHRES IMAGE NOW HAS SKULL (BECAUSE FOR CALILBRATION-MEDIATED REGISTRATION FOR WAVE 1 SCANS, NEED SKULL)
##TEMPORAL FILTERING NOW BETWEEN 0.009 AND 0.1
##LINEAR AND QUADRATIC DETRENDING ADDED


# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP_DT}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## Dropping first 4 TRS
COMMANDS_INFO[3]="Dropping first 4 TRs"
COMMANDS[3]="3dcalc -a '${funcRaw}[${gTRstart}..${gTRend}]' -expr 'a' -prefix ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[3]="${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## Checking TR - MOVED TO AFTER REGISTRATION STEP AS REGISTRATION RESULTS IN MISSPECIFICATION OF TR
#COMMANDS_INFO[4]="Changing TR specification in header to ensure it's correct"
#COMMANDS[4]="3drefit -TR ${gTR} ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## time shifting slices
COMMANDS_INFO[4]="Timeshifting (interlaced)"
COMMANDS[4]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[4]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[5]="Deobliquing"
COMMANDS[5]="3drefit -deoblique ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[6]="Reorienting to RPI"
COMMANDS[6]="3dresample -orient RPI -inset ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[6]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

##create base for registration
COMMANDS_INFO[7]="Creating base for registration"
COMMANDS[7]="3dcalc -a '${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}[7]' -expr 'a' -prefix ${preDir}/for_reg.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[7]="${funcDir}/for_reg.${gOUTPUT_TYPE}"

## REGISTER EXAMPLE FUNC TO BASE SCAN EXAMPLE FUNC (E.G. SCAN 1, FUNC, SESS_A )
COMMANDS_INFO[8]="Registering base from current scan to base scan"
COMMANDS[8]="flirt -ref ${iDir}/${gFN_REGBASE} -in ${preDir}/for_reg.nii.gz -out ${funcDir}/example_2base_example -omat ${funcDir}/example_2base_example.mat -cost normcorr -dof 6 -interp sinc"
COMMANDS_OUTPUT[8]="${funcDir}/example_2base_example.${gOUTPUT_TYPE}"

## APPLY REGISTRATION 
COMMANDS_INFO[9]="Applying registration to base scan"
COMMANDS[9]="flirt -ref ${iDir}/${gFN_REGBASE} -in ${preDir}/2_${gFN_FUNCBASE}_ro -out ${preDir}/3a_${gFN_FUNCBASE}_aligned_to_baseScan -applyxfm -init ${funcDir}/example_2base_example.mat -interp sinc"
COMMANDS_OUTPUT[9]="${preDir}/3a_${gFN_FUNCBASE}_aligned_to_baseScan.${gOUTPUT_TYPE}"

## motion correct
COMMANDS_INFO[10]="Motion correction"
COMMANDS[10]="3dvolreg -Fourier -twopass -base '3a_${gFN_FUNCBASE}_aligned_to_baseScan.nii.gz[7]' -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/3a_${gFN_FUNCBASE}_aligned_to_baseScan.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

###TR BECOMES INCORRECT AFTER REGISTRATION - CORRECTED HERE because doesn't work if you try to correct registered image
COMMANDS_INFO[11]="Changing TR specification in header to ensure it's correct"
COMMANDS[11]="3drefit -TR ${gTR} ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

## skull strip
COMMANDS_INFO[12]="Skull Strip"
COMMANDS[12]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate 1 ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[13]="3dcalc -a ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[13]="${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"

## get eighth func image (WITHOUT SKULL) 
COMMANDS_INFO[14]="Get example func (in this case the eigth image) for registration"
COMMANDS[14]="3dcalc -a '3a_${gFN_FUNCBASE}_aligned_to_baseScan.nii.gz[7]' -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${funcExample}"
COMMANDS_OUTPUT[14]="${funcExample}"

## despike
COMMANDS_INFO[15]="Despike"
COMMANDS[15]="3dDespike -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[15]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## spatial smoothing
COMMANDS_INFO[16]="Spatial smoothing"
COMMANDS[16]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[16]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[17]="Grand-mean scaling or intensity normalization"
COMMANDS[17]="fslmaths ${preDir}/6_${gFN_FUNCBASE}_ss -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin -odt float"
COMMANDS_OUTPUT[17]="${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[18]="Band-pass filter (get low-frequencies)"
COMMANDS[18]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[18]="${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[19]="Detrend"
COMMANDS[19]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[191]="${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"
COMMANDS[20]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[20]="${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE}"
COMMANDS[21]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT}"
COMMANDS_OUTPUT[21]="${funcPP_DT}"

## mask
COMMANDS_INFO[22]="Generating another mask of brain (used later for regression analyses)"
COMMANDS[22]="fslmaths ${funcPP_DT} -Tmin -bin ${funcMask} -odt char"
COMMANDS_OUTPUT[22]="${funcMask}"
