#!/usr/bin/env bash

source "$GUNTHERDIR/include/Asourceall.sh"

# Seperate motion parameters into seperate files
awk '{print \$1}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}1.${gTS_EXT}
awk '{print \$2}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}2.${gTS_EXT}
awk '{print \$3}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}3.${gTS_EXT}
awk '{print \$4}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}4.${gTS_EXT}
awk '{print \$5}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}5.${gTS_EXT}
awk '{print \$6}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}6.${gTS_EXT}

##Get maximum displacement value
awk 'max=="" || $1 > max {max=$1} END{ print max}' ${funcDir}/maxdisp.1D > temp
value=$(<temp)
printf "%.2f\t" $value >> max_mov.1D

##Get maximum 6 motion params
for i in 1 2 3 4 5 6
do
awk 'max=="" || $1 > max {max=$1} END{ print max}' ${funcDir}/mc${i}.1D > temp
value=$(<temp)
printf "%.2f\t" $value >> max_mov.1D
done

rm temp
