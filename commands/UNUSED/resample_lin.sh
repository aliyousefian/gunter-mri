#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/02/08
# AUTHOR(S): Zarrar Shehzad

# General Input and Output of Commands
BIG_INPUT="${funcPP} ${regDir}/standard.nii.gz ${regDir}/example_func2highres.mat"
BIG_OUTPUT="${funcDir}/${gFN_FUNCtoSTANDARD}"

# Command Variables
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT


# COMMANDS
COMMANDS_INFO[1]="Registering and resampling functional image to standard space"
COMMANDS[1]="flirt -in ${funcPP} -ref ${regDir}/standard -out ${funcDir}/${gFN_FUNCtoSTANDARD} -applyxfm -init ${regDir}/example_func2standard.mat -interp trilinear"
