#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/02/08
# AUTHOR(S): Zarrar Shehzad

# Input and Output of Commands
BIG_INPUT="${funcPP} ${regDir}/standard ${regDir}/example_func2highres.mat ${regDir}/highres2standard_warp"
BIG_OUTPUT="${funcDir}/${gFN_FUNCtoSTANDARD}"

# COMMANDS
declare -a COMMANDS
COMMANDS[1]="applywarp --ref=${regDir}/standard --in=${funcPP} --out=${funcDir}/${gFN_FUNCtoSTANDARD} --warp=${regDir}/highres2standard_warp --premat=${regDir}/example_func2highres.mat"

# INFO FOR COMMANDS
declare -a COMMANDS_INFO
COMMANDS_INFO[1]="Registering and resampling functional image to standard space"

# INPUT FOR COMMANDS
declare -a COMMANDS_INPUT
COMMANDS_INPUT[1]="${BIG_INPUT}"

# OUTPUT FOR COMMANDS
declare -a COMMANDS_OUTPUT
COMMANDS_OUTPUT[1]="${BIG_OUTPUT}"
