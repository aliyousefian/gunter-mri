#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/19/08
# AUTHOR(S): Zarrar Shehzad

# Variables
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

## TODO: add function to delete these files, unless option -N is specified

# INITIAL INPUT AND FINAL OUTPUT
BIG_INPUT=""
BIG_OUTPUT=""

## note that x_fsfsubstitute.sh runs the feat_model
COMMANDS_INFO[1]="Copying over new fsf file"
COMMANDS[1]="mkdir -p ${outputDir}/${gDIR_FSF} 2> /dev/null; echo -n ''"
COMMANDS[2]="x_fsfsubstitute.sh -i ${templateFile} -f ${templateFind} -r ${subject} -o ${outputDir}/${newFsfName}.fsf -c '1 ${to_skip} ${to_force}'"

COMMANDS_INFO[3]="Run feat"
COMMANDS[3]="feat ${outputDir}/${newFsfName}.fsf"
