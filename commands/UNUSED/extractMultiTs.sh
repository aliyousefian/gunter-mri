#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/24/08
# AUTHOR(S): Zarrar Shehzad

# Command Vars
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${maskFile} ${funcResEStandard_1} ${funcResEStandard_2} ${funcResEStandard_3} ${funcResEStandard_4}"
BIG_OUTPUT="${tsFile}.${gTS_EXT}"

# Making Seeds and Feats directories in your projects directory
COMMANDS_INFO[1]="Creating the subject directory in ${gDIR_PROJECTS}"
COMMANDS[1]="mkdir ${iProjSubDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[2]="Creating the func directory in ${iProjSubDir}"
COMMANDS[2]="mkdir ${iProjDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[3]="Creating the seeds directory"
COMMANDS[3]="mkdir ${tsDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[4]="Creating the feats directory"
COMMANDS[4]="mkdir ${featsDir} 2> /dev/null; echo -n ''"

# Obtain average time series of a region
COMMANDS_INFO[5]="Extracting mean time series"
COMMANDS[5]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${funcResEStandard_1} > ${tsFile}_1.${gTS_EXT}"

COMMANDS_INFO[6]="Extracting mean time series"
COMMANDS[6]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${funcResEStandard_2} > ${tsFile}_2.${gTS_EXT}"

COMMANDS_INFO[7]="Extracting mean time series"
COMMANDS[7]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${funcResEStandard_3} > ${tsFile}_3.${gTS_EXT}"

COMMANDS_INFO[8]="Extracting mean time series"
COMMANDS[8]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${funcResEStandard_4} > ${tsFile}_4.${gTS_EXT}"

COMMANDS_INFO[9]="Concatenating mean time series"
COMMANDS[9]="cat ${tsFile}_1.${gTS_EXT} ${tsFile}_2.${gTS_EXT} ${tsFile}_3.${gTS_EXT} ${tsFile}_4.${gTS_EXT} > ${tsFile}.${gTS_EXT}"

COMMANDS_INFO[10]="Removing time series pieces"
COMMANDS[10]="rm ${tsFile}_1.${gTS_EXT} ${tsFile}_2.${gTS_EXT} ${tsFile}_3.${gTS_EXT} ${tsFile}_4.${gTS_EXT}"

#COMMANDS_INFO[11]="Checking mean time series"
#COMMANDS[11]="x_checkTs.sh -i ${tsFile}.${gTS_EXT} -v ${gVOLNUM} -d"

#Transpose the time series
COMMANDS_INFO[11]="Transposing seed time series"
COMMANDS[11]="1dtranspose ${tsFile}.${gTS_EXT} > ${tsFile}_transpose.${gTS_EXT}"

# Compute the stdev of the seed
COMMANDS_INFO[12]="Computing standard deviation"
COMMANDS[12]="3dTstat -stdev -prefix ${tsFile}_stdev ${tsFile}_transpose.${gTS_EXT}"
COMMANDS_OUTPUT[12]="${tsFile}_stdev.${gTS_EXT}"

# Solution for strange 1deval problem!!
COMMANDS[13]="/frodo/shared/gunther-mri/commands/stdevTS.sh ${tsFile} ${gTS_EXT}"

#remove the original and intermediates
COMMANDS[14]="rm ${tsFile}.${gTS_EXT}"
COMMANDS[15]="rm ${tsFile}_transpose.${gTS_EXT}"
COMMANDS[16]="rm ${tsFile}_stdev.${gTS_EXT}"

#rename scaled time series 
COMMANDS[17]="mv ${tsFile}_scaled.${gTS_EXT} ${tsFile}.${gTS_EXT}"

