#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/24/08
# AUTHOR(S): Zarrar Shehzad

# Command Vars
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${maskFile} ${funcResEStandard}"
BIG_OUTPUT="${tsFile}.${gTS_EXT}"

# Making Seeds and Feats directories in your projects directory
COMMANDS_INFO[1]="Creating the subject directory in ${gDIR_PROJECTS}"
COMMANDS[1]="mkdir ${iProjSubDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[2]="Creating the func directory in ${iProjSubDir}"
COMMANDS[2]="mkdir ${iProjDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[3]="Creating the seeds directory"
COMMANDS[3]="mkdir ${tsDir} 2> /dev/null; echo -n ''"

COMMANDS_INFO[4]="Creating the feats directory"
COMMANDS[4]="mkdir ${featsDir} 2> /dev/null; echo -n ''"

# Create temporary file
COMMANDS_INFO[5]="Masking by probability weighting"
COMMANDS[5]="3dcalc -a ${funcResEStandard} -b ${maskFile} -expr 'a*b' -datum float -prefix ${featsDir}/tmp_prob.nii"

# Obtain average time series of a region
COMMANDS_INFO[6]="Extracting mean time series"
COMMANDS[6]="3dROIstats -mask_f2short -mask ${maskFile} -quiet ${featsDir}/tmp_prob.nii.gz > ${tsFile}.${gTS_EXT}"

# Delete temporary file
COMMANDS_INFO[7]="Deleting temporary file"
COMMANDS[7]="rm ${featsDir}/tmp_prob.nii.gz"

COMMANDS_INFO[8]="Checking mean time series"
COMMANDS[8]="x_checkTs.sh -i ${tsFile}.${gTS_EXT} -v ${gVOLNUM} -d"


#Transpose the time series
COMMANDS_INFO[9]="Transposing seed time series"
COMMANDS[9]="1dtranspose ${tsFile}.${gTS_EXT} > ${tsFile}_transpose.${gTS_EXT}"

# Compute the stdev of the seed
# Mistery 1: if prefix is with extension it does not make the files in the right location!
# solution: remove .${gTS_EXT} from prefix
COMMANDS_INFO[10]="Computing standard deviation"
COMMANDS[10]="3dTstat -stdev -prefix ${tsFile}_stdev ${tsFile}_transpose.${gTS_EXT}"
COMMANDS_OUTPUT[10]="${tsFile}_stdev.${gTS_EXT}"

# Solution for strange 1deval problem!!
COMMANDS[11]="/frodo/shared/gunther-mri/commands/stdevTS.sh ${tsFile} ${gTS_EXT}"

#remove the original and intermediates
COMMANDS[12]="rm ${tsFile}.${gTS_EXT}"
COMMANDS[13]="rm ${tsFile}_transpose.${gTS_EXT}"
COMMANDS[14]="rm ${tsFile}_stdev.${gTS_EXT}"

#rename scaled time series 
COMMANDS[15]="mv ${tsFile}_scaled.${gTS_EXT} ${tsFile}.${gTS_EXT}"
