#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/25/08
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, and Michael Milham

# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR
## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## time shifting slices
COMMANDS_INFO[3]="Timeshifting (interlaced)"
COMMANDS[3]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${funcRaw}"
COMMANDS_OUTPUT[3]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## get average func
COMMANDS_INFO[4]="Get mean functional image (for motion correction)"
COMMANDS[4]="3dTstat -mean -prefix ${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[4]="${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

## motion correct
COMMANDS_INFO[5]="Motion correction"
COMMANDS[5]="3dvolreg -Fourier -twopass -base ${preDir}/3a_${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[5]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

## skull strip
COMMANDS_INFO[6]="Skull Strip"
COMMANDS[6]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate ${afniDilate} ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[6]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[7]="3dcalc -a ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[7]="${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"

## despike
COMMANDS_INFO[8]="Despike"
COMMANDS[8]="3dDespike -ssave ${funcDir}/${gFN_FUNCBASE}_spikiness.1D -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[8]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## example func
COMMANDS_INFO[9]="Get example func in this case the second image (used later for registration)"
COMMANDS[9]="fslroi ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${funcExample} 1 1"
COMMANDS_OUTPUT[9]="${funcExample}"

## spatial smoothing
COMMANDS_INFO[10]="Spatial smoothing"
COMMANDS[10]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[11]="Grand-mean scaling or intensity normalization"
COMMANDS[11]="fslmaths ${preDir}/6_${gFN_FUNCBASE}_ss -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin -odt float"
COMMANDS_OUTPUT[11]="${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"

## temporal filtering
##COMMANDS_INFO[15]="Band-pass filter (get low-frequencies)"
##COMMANDS[15]="fslmaths ${preDir}/7_${gFN_FUNCBASE}_gin -bptf ${hp} ${lp} ${funcPP} -odt float"
##COMMANDS_OUTPUT[15]="${funcPP}"

## temporal filtering
COMMANDS_INFO[12]="Band-pass filter (get low-frequencies)"
COMMANDS[12]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${funcPP} ${preDir}/7_${gFN_FUNCBASE}_gin.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${funcPP}"

## mask
COMMANDS_INFO[13]="Generating another mask of brain (used later for regression analyses)"
COMMANDS[13]="fslmaths ${funcPP} -Tmin -bin ${funcMask} -odt char"
COMMANDS_OUTPUT[13]="${funcMask}"


## SNR

COMMANDS_INFO[14]="Generating SNR of functional image"
COMMANDS[14]="mkdir -p ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[14]="cd ${checksDir}/1_func"

### get mean image
COMMANDS[15]="3dTstat -mean -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[15]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE}"

### get std image
COMMANDS[16]="3dTstat -stdev -prefix ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[16]="${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE}"

## get SNR image
COMMANDS[17]="3dcalc -a ${checksDir}/1_func/${gFN_FUNCBASE}_mean.${gOUTPUT_TYPE} -b ${checksDir}/1_func/${gFN_FUNCBASE}_std.${gOUTPUT_TYPE} -expr \"a/b\" -prefix ${checksDir}/1_func/snr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[17]="${checksDir}/1_func/snr.${gOUTPUT_TYPE}"

## spit something out for user
COMMANDS[18]="meanSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M \`"
COMMANDS[19]="sdevSNR=\`fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -S\`"
COMMANDS[20]="zimportant \"${subject} mean SNR is \${meanSNR} and standard deviation of SNR is \${sdevSNR}\""
COMMANDS[21]="fslstats ${checksDir}/1_func/snr.${gOUTPUT_TYPE} -M -S > ${checksDir}/1_func/snr.1D"

## get images and put into checks directory
COMMANDS[22]="mkdir ${checksDir}/1_func 2> /dev/null; echo -n ''"
COMMANDS[23]="slicer ${checksDir}/1_func/${gFN_FUNCBASE}_mean -s 2 -A 750 ${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS_OUTPUT[23]="${checksDir}/1_func/${gFN_FUNCBASE}_mean.png"
COMMANDS[24]="maxSNR=\`3dBrickStat -max ${checksDir}/1_func/snr.${gOUTPUT_TYPE}\`"
COMMANDS[25]="overlay 1 0 ${checksDir}/1_func/${gFN_FUNCBASE}_mean -a ${checksDir}/1_func/snr 0 \${maxSNR} ${checksDir}/1_func/rendered_snr"
COMMANDS_OUTPUT[25]="${checksDir}/1_func/rendered_snr.${gOUTPUT_TYPE}"
COMMANDS[26]="slicer ${checksDir}/1_func/rendered_snr -s 2 -A 750 ${checksDir}/1_func/snr.png"
COMMANDS_OUTPUT[26]="${checksDir}/1_func/snr.png"

# Seperate motion parameters into seperate files
COMMANDS_INFO[27]="Setting up motion covariates in seperate files"
COMMANDS[27]="awk '{print \$1}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS_OUTPUT[27]="${funcDir}/${gFN_MCBASE}1.${gTS_EXT}"
COMMANDS[28]="awk '{print \$2}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS_OUTPUT[28]="${funcDir}/${gFN_MCBASE}2.${gTS_EXT}"
COMMANDS[29]="awk '{print \$3}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS_OUTPUT[29]="${funcDir}/${gFN_MCBASE}3.${gTS_EXT}"
COMMANDS[30]="awk '{print \$4}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS_OUTPUT[40]="${funcDir}/${gFN_MCBASE}4.${gTS_EXT}"
COMMANDS[31]="awk '{print \$5}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS_OUTPUT[31]="${funcDir}/${gFN_MCBASE}5.${gTS_EXT}"
COMMANDS[32]="awk '{print \$6}' ${funcDir}/${gFN_MC} > ${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"
COMMANDS_OUTPUT[32]="${funcDir}/${gFN_MCBASE}6.${gTS_EXT}"

COMMANDS[33]="/frodo/shared/gunther-mri/get_movement_params.sh ${gFN_FUNCBASE} ${funcDir} ${checksDir}/1_func"

COMMANDS[34]="rm ${funcDir}/${gFN_MCBASE}*.${gTS_EXT}"

