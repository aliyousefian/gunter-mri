#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/25/08
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, and Michael Milham

# TODO:
#   - make version that goes into standard space + uses a prior
#   - move the fsl fast part to the anat folder (in anatDir/segment...)

# General Input and Output of Commands
BIG_INPUT="${brain} ${funcMask} ${funcregDir}"
BIG_OUTPUT="${segmentDir}/mask_global.${gOUTPUT_TYPE} ${segmentDir}/mask_csf.${gOUTPUT_TYPE} ${segmentDir}/mask_white.${gOUTPUT_TYPE}"

# declaration of variables
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

## segment the brain
COMMANDS_INFO[1]="Segmenting the brain and generate probability map of gray, white, and csf"
COMMANDS[1]="cd ${anatDir}"
COMMANDS[2]="fast -t ${input_type} -g -p -o segment ${brain}"
COMMANDS_OUTPUT[2]="segment_prob_0.${gOUTPUT_TYPE} segment_prob_1.${gOUTPUT_TYPE} segment_prob_2.${gOUTPUT_TYPE}"

## setup for functional stuff
COMMANDS_INFO[3]="Setup to create masks for 3 nuisance signals"
COMMANDS[3]="cd `dirname ${segmentDir}`"
COMMANDS[4]="mkdir ${segmentDir} 2> /dev/null; echo -n ''"
COMMANDS[5]="cd ${segmentDir}"

## global
COMMANDS_INFO[6]="Creating global brain mask"
COMMANDS[6]="fslmaths ${funcMask} ${funcDir}/mask_global.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[6]="${funcDir}/mask_global.${gOUTPUT_TYPE}"

## csf
COMMANDS_INFO[7]="Creating csf mask"
COMMANDS[7]="echo ''"
### register csf to native space
COMMANDS_INFO[8]="Registering csf to native (functional) space"
COMMANDS[8]="flirt -in ${anatDir}/segment_prob_0 -ref ${funcExample} -applyxfm -init ${funcregDir}/highres2example_func.mat -out 1_csf2func"
COMMANDS_OUTPUT[8]="1_csf2func.${gOUTPUT_TYPE}"
### smooth image...warning may not work with 0mm blurring
COMMANDS_INFO[9]="Smoothing image"
COMMANDS[9]="fslmaths 1_csf2func -kernel gauss ${sigma} -fmean 2_csf_sm"
COMMANDS_OUTPUT[9]="2_csf_sm.${gOUTPUT_TYPE}"
## register to standard
COMMANDS_INFO[10]="Registering to standard space"
COMMANDS[10]="flirt -in 2_csf_sm -ref ${gFN_STANDARD} -applyxfm -init ${funcregDir}/example_func2standard.mat -out 3_csf2standard"
COMMANDS_OUTPUT[10]="3_csf2standard.${gOUTPUT_TYPE}"
## find overlap with prior
COMMANDS_INFO[11]="Find overlap between subject's CSF and prior"
COMMANDS[11]="fslmaths 3_csf2standard -mas ${gPRIOR_CSF} 4_csf_masked"
COMMANDS_OUTPUT[11]="4_csf_masked.${gOUTPUT_TYPE}"
## revert back to functional space
COMMANDS_INFO[12]="Register back to native (functional) space"
COMMANDS[12]="flirt -in 4_csf_masked -ref ${funcExample} -applyxfm -init ${funcregDir}/standard2example_func.mat -out 5_csf_native"
COMMANDS_OUTPUT[12]="5_csf_native.${gOUTPUT_TYPE}"
## Threshold and binarize probability map of csf
COMMANDS_INFO[13]="Threshold and binarize csf probability map"
COMMANDS[13]="fslmaths 5_csf_native -thr 0.4 -bin 6_csf_bin"
COMMANDS_OUTPUT[13]="6_csf_bin.${gOUTPUT_TYPE}"
## include only brain
COMMANDS_INFO[14]="Only keep stuff in brain"
COMMANDS[14]="fslmaths 6_csf_bin -mas ${funcDir}/mask_global ${funcDir}/mask_csf"
COMMANDS_OUTPUT[14]="${funcDir}/mask_csf.${gOUTPUT_TYPE}"

## wm
COMMANDS_INFO[15]="Creating white matter mask"
COMMANDS[15]="echo -n ''"
### register wm to native space
COMMANDS_INFO[16]="Registering wm to native (functional) space"
COMMANDS[16]="flirt -in ${anatDir}/segment_prob_2 -ref ${funcExample} -applyxfm -init ${funcregDir}/highres2example_func.mat -out 1_wm2func"
COMMANDS_OUTPUT[16]="1_wm2func.${gOUTPUT_TYPE}"
### smooth image...warning won't work with 0mm blurring
COMMANDS_INFO[17]="Smoothing image (unless gFWHM=0)"
COMMANDS[17]="fslmaths 1_wm2func -kernel gauss ${sigma} -fmean 2_wm_sm"
COMMANDS_OUTPUT[17]="2_wm_sm.${gOUTPUT_TYPE}"
## register to standard
COMMANDS_INFO[18]="Registering to standard space"
COMMANDS[18]="flirt -in 2_wm_sm -ref ${gFN_STANDARD} -applyxfm -init ${funcregDir}/example_func2standard.mat -out 3_wm2standard"
COMMANDS_OUTPUT[18]="3_wmstandard.${gOUTPUT_TYPE}"
## find overlap with prior
COMMANDS_INFO[19]="Find overlap between subject's WM and prior"
COMMANDS[19]="fslmaths 3_wm2standard -mas ${gPRIOR_WHITE} 4_wm_masked"
COMMANDS_OUTPUT[19]="4_wm_masked.${gOUTPUT_TYPE}"
## revert back to functional space
COMMANDS_INFO[20]="Register back to native (functional) space"
COMMANDS[20]="flirt -in 4_wm_masked -ref ${funcExample} -applyxfm -init ${funcregDir}/standard2example_func.mat -out 5_wm_native"
COMMANDS_OUTPUT[20]="5_wm_native.${gOUTPUT_TYPE}"
## Threshold and binarize probability map of csf
COMMANDS_INFO[21]="Threshold and binarize wm probability map"
COMMANDS[21]="fslmaths 5_wm_native -thr 0.66 -bin 6_wm_bin"
COMMANDS_OUTPUT[21]="6_wm_bin.${gOUTPUT_TYPE}"
## include only brain
COMMANDS_INFO[22]="Only keep stuff in brain"
COMMANDS[22]="fslmaths 6_wm_bin -mas ${funcDir}/mask_global ${funcDir}/mask_wm"
COMMANDS_OUTPUT[22]="${funcDir}/mask_wm.${gOUTPUT_TYPE}"

## CHECKS
COMMANDS_INFO[23]="Creating images of global, csf, and white matter masks in ${checksDir}/${gDIR_SEGMENT} for user to CHECK"
### make directory
COMMANDS[23]="mkdir -p ${checksDir}/${gDIR_SEGMENT} 2> /dev/null; echo -n ''"
### global
COMMANDS_INFO[24]="global"
COMMANDS[24]="overlay 1 1 ${funcExample} -a ${funcDir}/mask_global.${gOUTPUT_TYPE} 1 1 ${checksDir}/${gDIR_SEGMENT}/rendered_global"
COMMANDS_OUTPUT[24]="${checksDir}/${gDIR_SEGMENT}/rendered_global.${gOUTPUT_TYPE}"
COMMANDS[25]="slicer ${checksDir}/${gDIR_SEGMENT}/rendered_global -s 2 -A 750 ${checksDir}/${gDIR_SEGMENT}/global.png"
COMMANDS_OUTPUT[25]="${checksDir}/${gDIR_SEGMENT}/global.png"
### csf
COMMANDS_INFO[26]="csf"
COMMANDS[26]="overlay 1 1 ${funcExample} -a ${funcDir}/mask_csf.${gOUTPUT_TYPE} 1 1 ${checksDir}/${gDIR_SEGMENT}/rendered_csf"
COMMANDS_OUTPUT[26]="${checksDir}/${gDIR_SEGMENT}/rendered_csf.${gOUTPUT_TYPE}"
COMMANDS[27]="slicer ${checksDir}/${gDIR_SEGMENT}/rendered_csf -s 2 -A 750 ${checksDir}/${gDIR_SEGMENT}/csf.png"
COMMANDS_OUTPUT[27]="${checksDir}/${gDIR_SEGMENT}/csf.png"
### wm
COMMANDS_INFO[28]="white matter"
COMMANDS[28]="overlay 1 1 ${funcExample} -a ${funcDir}/mask_wm.${gOUTPUT_TYPE} 1 1 ${checksDir}/${gDIR_SEGMENT}/rendered_wm"
COMMANDS_OUTPUT[28]="${checksDir}/${gDIR_SEGMENT}/rendered_wm.${gOUTPUT_TYPE}"
COMMANDS[29]="slicer ${checksDir}/${gDIR_SEGMENT}/rendered_wm -s 2 -A 750 ${checksDir}/${gDIR_SEGMENT}/wm.png"
COMMANDS_OUTPUT[29]="${checksDir}/${gDIR_SEGMENT}/wm.png"

