#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): The Underpants Gnomes.

#MAJOR REVISION 2ND MARCH 2010: CREATED SEPARATE REGISTRATION DIRECTORIES FOR FUNC AND ANAT

#### REVISION 20th August 2010: REGISTRATION WAS FAILING FOR SOME WAVE 1 KIDS.
#### THE PROBLEM IS IN THE REGISTRATION FROM CAL_RHO TO HIGHRES
#### FIXED BY USING MUTUAL INFORMATION AS COST FUNCTION RATHER THAN CORRATIO


# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${head} ${brain} ${funcExample} ${calSynth} ${gFN_STANDARD}"
BIG_OUTPUT="${funcregDir}/example_func2highres.${gOUTPUT_TYPE}"

## 1. SETUP
COMMANDS[1]="cd `dirname ${anatregDir}`"
COMMANDS[2]="mkdir ${anatregDir} 2> /dev/null; echo -n ''"
COMMANDS[3]="mkdir ${funcregDir} 2> /dev/null; echo -n ''"

## Linear registration
COMMANDS[4]="flirt -ref ${gFN_STANDARD} -in ${brain} -out ${anatregDir}/highres2standard -omat ${anatregDir}/highres2standard.mat -cost ${cost_highres2standard} -searchcost ${cost_highres2standard} -dof 12 -interp trilinear"
COMMANDS_OUTPUT[4]="${anatregDir}/highres2standard.mat"
## Create mat file for conversion from standard to high res
COMMANDS[5]="convert_xfm -inverse -omat ${anatregDir}/standard2highres.mat ${anatregDir}/highres2standard.mat"
COMMANDS_OUTPUT[5]="${anatregDir}/standard2highres.mat"

### register example_func to synthetic epi
COMMANDS_INFO[6]="Registering example func (eighth image) to synthetic calibration image"
COMMANDS[6]="flirt -nosearch -cost normcorr -interp sinc -dof 6 -in ${funcExample} -ref ${calSynth} -omat ${funcregDir}/example_func2synth.mat -out ${funcregDir}/example_func2synth"
COMMANDS_OUTPUT[6]="${funcregDir}/example_func2synth.mat"

### unwarp the calib-registered functional - this unwarping is also performed on the motion corrected image. The resulting unwarped functional is then preprocessed as normal
### x is the correct unwarping direction when the images have been reoriented
COMMANDS_INFO[7]="Unwarping functional using calibration scan"
COMMANDS[7]="fugue -i ${funcregDir}/example_func2synth --loadfmap=${calibDir}/cal_reg_boRPI --unwarpdir=x --dwell=1 -u ${funcregDir}/example_func2rho"
COMMANDS_OUTPUT[7]="${funcregDir}/example_func2rho.${gOUTPUT_TYPE}"

##this unwarped functional is actually example_func and is copied into reg directory as such
COMMANDS[8]="cp ${funcregDir}/example_func2rho.${gOUTPUT_TYPE} ${funcregDir}/example_func.${gOUTPUT_TYPE}"

### register undistorted image to highres - this is the registration that will be applied to the fully preprocessed image 
###i.e., rho2highres is the same as example_func2highres when functional has been unwarped 
COMMANDS_INFO[9]="Registering  undistorted functional to highres"
COMMANDS[9]="flirt -nosearch -cost mutualinfo -interp sinc -dof 6 -in ${calibDir}/cal_rhoRPI -ref ${head} -omat ${calibDir}/rho2highres.mat -out ${calibDir}/rho2highres"
COMMANDS_OUTPUT[9]="${calibDir}/rho2highres.mat"

### register unwarped functional to highres
COMMANDS_INFO[10]="Registering  undistorted functional to highres"
COMMANDS[10]="flirt -in ${funcregDir}/example_func2rho -ref ${head} -applyxfm -init ${calibDir}/rho2highres.mat -out ${funcregDir}/example_func2highres"
COMMANDS_OUTPUT[10]="${funcregDir}/example_func2highres.${gOUTPUT_TYPE}"

### concatenate matrices
COMMANDS[11]="cp ${calibDir}/rho2highres.mat ${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[11]="${funcregDir}/example_func2highres.mat"
COMMANDS[12]="convert_xfm -omat ${funcregDir}/highres2example_func.mat -inverse ${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[12]="${funcregDir}/highres2example_func.mat"
COMMANDS[13]="convert_xfm -omat ${funcregDir}/example_func2standard.mat -concat ${anatregDir}/highres2standard.mat ${funcregDir}/example_func2highres.mat"
COMMANDS_OUTPUT[13]="${funcregDir}/example_func2standard.mat"
COMMANDS[14]="convert_xfm -omat ${funcregDir}/standard2example_func.mat -inverse ${funcregDir}/example_func2standard.mat"
COMMANDS_OUTPUT[14]="${funcregDir}/standard2example_func.mat"
