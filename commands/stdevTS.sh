#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/24/08
# AUTHOR(S): Zarrar Shehzad

tsFile=$1
gTS_EXT=$2

tmpBase="$GUNTHERDIR"
source "$tmpBase/include/Asourceall.sh"

zcmd "cat ${tsFile}_stdev.${gTS_EXT} | grep -v '#' > stdev"
zcmd "sed 's. ..' <stdev > stdv"
zcmd "sdv=$( cat stdv )"
zcmd "echo $sdv"
zcmd "1deval -a ${tsFile}.${gTS_EXT} -expr '(a/${sdv})+100' > ${tsFile}_scaled.${gTS_EXT}"

rm stdev
rm stdv
