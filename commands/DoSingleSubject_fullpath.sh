
#!/usr/bin/env bash

# do singlesubject analysis using 3dfim+
# written by Maarten Mennes, Clare Kelly and Xinian Zuo
# called from /frodo/fmri/projects/YOURPROJECT/scripts/DoSingleSubject.sh

##MODIFIED 3RD SEPT 2010 TO READ ANAT and FUNC FROM SUBJECT LIST

## inputs
project_dir=$1
subject_list=$2
seed_list=$3
image_for_correlation=$4
image_for_TSextraction=$5
mm=$6
final_mm=$7
registration=$8


#### SCRIPT SCRIPT SCRIPT SCRIPT SCRIPT ####
#

seeds=$( cat ${seed_list} )
#subjectlist is fed at the back of the subjectloop
#subjects=$( cat ${subject_list} )

### LOOP 1.1: For each subject
while read line
do

# get full path, subjectnumber and which anat to use
sub_path=$( echo ${line} | cut -f 1 -d ' ' )
echo subject path = $sub_path
i=$( echo ${line} | cut -f 2 -d ' ' )
echo subject number = ${i}
anat=$( echo ${line} | cut -f 3 -d ' ' )
echo anatomical scan to use = ${anat}
echo

echo
echo --------------------
echo RUNNING SUBJECT ${i}
echo

mkdir -p ${project_dir}/${i}/5a_seeds
mkdir -p ${project_dir}/${i}/7_corrs

if [ ! -f ${sub_path}/${image_for_TSextraction} ]
then
echo SUBJECT ${sub_path} does not have ${image_for_TSextraction}
echo SKIPPING SUBJECT
echo
continue
fi

## LOOP 1.2: For each seed
for s in $seeds
do

# s is the full line, ie. s=/full/path/seed_name.nii.gz
seed=${s##*/}
seed_name=$( echo $seed | cut -f 1 -d '.' )

	# check if seed is already done - file needs to be removed from SingleSubs for analysis to run
	if [ -f ${project_dir}/${i}/7_corrs/${seed_name}_Z_fl2standard.nii.gz ] || [ -f ${project_dir}/${i}/7_corrs/${seed_name}_Z_fn2standard.nii.gz ]
	then
	echo SUBJECT ANALYSIS FOR SEED ${s} ALREADY DONE
	else


	# EXTRACT TIMESERIES
	echo "Extracting timeseries for seed ${seed_name}"
	if [ "${mm}" = "1mm" ]
	then
		echo 1mm
		echo $image_for_TSextraction > temp
		sed 's/.nii.gz//' < temp > temp2
		image_for_TSextraction=$( cat temp2 )
		
		# delete timeseries in case they exist already
		rm ${project_dir}/${i}/5a_seeds/${s}.1D		

		for part in 1 2 3 4
		do
			3dROIstats -quiet -mask_f2short -mask ${s} ${sub_path}/${image_for_TSextraction}_${part}.nii.gz >> ${project_dir}/${i}/5a_seeds/${s}.1D
		done
		rm temp temp2
	else
	3dROIstats -quiet -mask_f2short -mask ${s} ${sub_path}/${image_for_TSextraction} > ${project_dir}/${i}/5a_seeds/${seed_name}.1D
	fi

	
	# COMPUTE CORRELATION
	echo "Computing Correlation for seed ${s}"
	3dfim+ -input ${sub_path}/${image_for_correlation} -ideal_file ${project_dir}/${i}/5a_seeds/${seed_name}.1D -fim_thr 0.0009 -out Correlation -bucket ${project_dir}/${i}/7_corrs/${seed_name}_corr.nii.gz
	

	# FISHER Z-TRANSFORM
	echo "Z-transforming seed ${s}"
	3dcalc -a ${project_dir}/${i}/7_corrs/${seed_name}_corr.nii.gz -expr 'log((1+a)/(1-a))/2' -prefix ${project_dir}/${i}/7_corrs/${seed_name}_Z.nii.gz


	# REGISTER Z TO STANDARD SPACE
	echo "Registering Z-transformed map to standard space"

	if [ "${registration}" = "FLIRT" ]; then
	echo "Registering using FLIRT"
	flirt -in ${project_dir}/${i}/7_corrs/${seed_name}_Z.nii.gz -ref ${FSLDIR}/data/standard/MNI152_T1_${final_mm}_brain.nii.gz -applyxfm -init ${sub_path}/2_reg/example_func2standard.mat -out ${project_dir}/${i}/7_corrs/${seed_name}_Z_fl2standard.nii.gz
	fi

	if [ "${registration}" = "FNIRT" ]; then
	echo "Registering using FNIRT"
	applywarp --ref=/frodo/shared/fsl/data/standard/MNI152_T1_${final_mm}.nii.gz \
	--in=${project_dir}/${i}/7_corrs/${seed_name}_Z.nii.gz \
	--out=${project_dir}/${i}/7_corrs/${seed_name}_Z_fn2standard.nii.gz \
	--warp=${anat}/reg/highres2standard_warp \
	--premat=${sub_path}/2_reg/example_func2highres.mat
	fi

	# end of check for file
	# white line
	echo
	fi


## END OF LOOP 1.2: seed
done

### END OF LOOP 1.1: subject
done < ${subject_list}
