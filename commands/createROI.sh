#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/19/08
# AUTHOR(S): Zarrar Shehzad

# Command Vars
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

BIG_INPUT="${gDIR_MASKS} ${gFN_STANDARD}"
BIG_OUTPUT="${maskFile}"

COMMANDS[1]="cd ${gDIR_MASKS}"
COMMANDS[2]="echo '${xCoord} ${yCoord} ${zCoord}' | 3dUndump -prefix ${maskFile} -master ${gFN_STANDARD} -srad ${roi_radius} -orient LPI -xyz -"
COMMANDS[3]="3dNotes -a \"${mask}: ${xCoord} (x), ${yCoord} (y), ${zCoord} (z)\" ${maskFile}"
COMMANDS[4]="3dNotes -a \"ROI radius = ${roi_radius}\" ${maskFile}"
