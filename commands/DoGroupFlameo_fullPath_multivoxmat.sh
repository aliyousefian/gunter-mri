#!/usr/bin/env bash

# DoGroupFlameo
# Script to run Group analysis using flameo from FSL
# Written by Maarten Mennes
# This script is called by Setup_DoGroupFlameo.sh


#-------
# INPUTS
#-------

project_dir=$1
group_dir=$2
model_list=$3
seed_list=$4
subject_list=$5
registration=$6
group_mm=$7
func=$8
#voxel-matched regression inputs
ev_num_one=${9}
# shift arguments to left because subject_list calls for argument $10 which bash cannot handle
shift
vm_file_name_one=${9}
shift
ev_num_two=${9}
shift
vm_file_name_two=${9}

#----------------------------
# SET NEEDED LISTS/PARAMETERS
#----------------------------
## GET MODELS
if [ -f ${model_list} ]
then
models=$(cat ${model_list} )
else
models=${model_list}
fi

## GET SEEDS
if [ -f ${seed_list} ]
then
seeds=$(cat ${seed_list} )
else
seeds=${seed_list}
fi

## GET SUBJECTS
subjects=$( cat ${subject_list} )

## SET REGISTR PARAMETER
if [ "$registration" = "FLIRT" ]
then
registr=fl
fi
if [ "$registration" = "FNIRT" ]
then
registr=fn
fi

## MAKE DIRS
mkdir -p ${group_dir}/files
mkdir -p ${group_dir}/finished


#--------------
# START LOOPING
#--------------

# LOOP 1.1: For each MODEL
for model in $models
do

# LOOP 1.2: For each SEED
for seed in $seeds
do

echo
echo RUNNING GROUP ANALYSIS ${model} FOR SEED ${seed}
echo

## check for existance of group analysis file
if [ -f ${group_dir}/finished/${model}_${seed} ]
then
echo "GROUP ANALYSIS FOR ${model} FOR SEED ${seed} ALREADY DONE"
else


## CREATE CONCATENATED GROUP FILE
tmpfile=`mktemp`
echo ${tmpfile}
echo "Creating group file for ${model}_${seed}"
if [ -f ${group_dir}/files/${model}_${seed}_data.nii.gz ]
then
	echo group file for ${model} seed ${seed} ALREADY EXISTS
else
	for sub in $subjects
	do
	echo ${sub}/${seed}_Z_${registr}2standard.nii.gz >> ${tmpfile}
	done	
	files=$( cat ${tmpfile} )
	echo $files
	fslmerge -t ${group_dir}/files/${model}_${seed}_data.nii.gz $files
	rm ${tmpfile}
fi


## CREATE ANALYSIS SPECIFIC MASK
if [ -f ${group_dir}/files/${model}_mask.nii.gz ]
then
echo MASK OK
else
fslmaths ${group_dir}/files/${model}_${seed}_data.nii.gz -abs -Tmin -bin ${group_dir}/files/${model}_mask.nii.gz
fi


## RUN FLAMEO
echo "RUNNING FLAMEO ${model} FOR SEED ${seed}"

flameo \
--cope=${group_dir}/files/${model}_${seed}_data.nii.gz \
--mask=${group_dir}/files/${model}_mask.nii.gz \
--dm=${project_dir}/group_models/${model}.mat \
--tc=${project_dir}/group_models/${model}.con \
--cs=${project_dir}/group_models/${model}.grp \
--ven=${ev_num_one},${ev_num_two} \
--vef=${vm_file_name_one},${vm_file_name_two} \
--ld=${group_dir}/${model}_${seed} \
--runmode=ols


## THRESHOLD ZSTAT MAPS
echo "THRESHOLDING zstat-maps"
curdir=$( pwd )
cd ${group_dir}/${model}_${seed}

cat ${project_dir}/group_models/${model}.con | grep '/NumContrasts' > temp
sed 's_/NumContrasts\t__' < temp > temp2
numcon=$( cat temp2 )
rm temp temp2

# LOOP 1.3: Threshold Zstat maps
for ((z=1 ; z <= ${numcon} ; z++))
do
	echo "thresholding zstat${z}"
	easythresh zstat${z} \
	${group_dir}/files/${model}_mask.nii.gz \
	2.3 0.05 \
	${FSLDIR}/data/standard/MNI152_T1_${group_mm}_brain.nii.gz \
	zstat${z}

	# THRESHOLD MAP TO AVOID NEG VALUES - these show up for some reason in the thresholded maps
	fslmaths thresh_zstat${z}.nii.gz -thr 0 thresh_zstat${z}.nii.gz
	
# END OF LOOP 1.3
done

## create group_reg file
echo
echo CREATING group_reg file
echo OPEN ${group_dir}/${model}_${seed}/group_reg.nii.gz with fslview, choose colorscale Random-Rainbow
n_vols=$( 3dinfo ${group_dir}/files/${model}_${seed}_data.nii.gz | grep 'time' | cut -f 6 -d \ )
fslmaths ${group_dir}/files/${model}_${seed}_data.nii.gz -abs -bin -Tmean -mul ${n_vols} group_reg.nii.gz

## CLEAN UP
rm res4d.nii.gz
cd ${curdir}

## MARK GROUP ANAYSIS AS DONE
[ -f ${group_dir}/${model}_${seed}/thresh_zstat1.nii.gz ] && touch ${group_dir}/finished/${model}_${seed}

## end of check to see if model seed is already done
echo GROUP ANALYSIS FOR ${model} ${seed} DONE
echo ----------------------------------------
fi

# END OF LOOP 1.2: SEED
done

# END OF LOOP 1.1: MODEL
done
