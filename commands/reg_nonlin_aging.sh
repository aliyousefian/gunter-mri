#!/usr/bin/env bash
#reg_nonlin_aging.sh
# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/29/08
# AUTHOR(S): Zarrar Shehzad

# THIS REQUIRES THE LINEAR REGISTRATION TO HAVE BEEN RUN!!!

#MAJOR REVISION 2ND MARCH 2010: CREATED SEPARATE REGISTRATION DIRECTORIES FOR FUNC AND ANAT

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${head} ${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz ${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask_dil.nii.gz"

BIG_OUTPUT="${funcregDir}/diff2standard_warp.${gOUTPUT_TYPE}"

# Perform nonlinear registration (higres to standard)
COMMANDS[1]="fnirt --in=${head} --aff=${anatregDir}/str2standard.mat --cout=${anatregDir}/str2standard_warp_cof --iout=${anatregDir}/fnirt_str2standard --jout=${anatregDir}/str2standard_jac --config=T1_2_MNI152_2mm --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --refmask=${FSLDIR}/data/standard/MNI152_T1_2mm_brain_mask_dil.nii.gz --warpres=10,10,10"
COMMANDS_OUTPUT[1]="${anatregDir}/fnirt_str2standard.${gOUTPUT_TYPE}"

# Apply nonlinear registration (func to standard)
COMMANDS[2]="applywarp --ref=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --in=${funcExample} --out=${funcregDir}/diff2standard_warp --warp=${anatregDir}/str2standard_warp_cof --premat=${funcregDir}/diff2str.mat"
COMMANDS_OUTPUT[2]="${funcregDir}/diff2standard_warp.${gOUTPUT_TYPE}"


# Apply nonlinear registration (standard to struc)
COMMANDS[3]="invwarp --ref=${brain}  --warp=${anatregDir}/str2standard_warp_cof --out=${anatregDir}/standard2str_warp" 
COMMANDS_OUTPUT[3]="${anatregDir}/standard2str_warp.${gOUTPUT_TYPE}"

# Apply nonlinear registration ( standard to DTI)
COMMANDS[4]="applywarp --ref=${funcExample} --in=${FSLDIR}/data/standard/MNI152_T1_2mm.nii.gz --out=${funcregDir}/standard2diff_warp --warp=${anatregDir}/standard2str_warp --postmat=${funcregDir}/str2diff.mat"
COMMANDS_OUTPUT[4]="${funcregDir}/standard2diff_warp.${gOUTPUT_TYPE}"
