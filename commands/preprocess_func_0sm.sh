#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, Clare Kelly, Daniel Margulies, Xinian Zuo, Michael Milham, Maarten Mennes, The Underpants Gnomes.

##MAJOR CHANGES ON 11/04/09: CHANGED MOTION CORRECTION TO BASE IMAGE (8TH) RATHER THAN MEAN FUNC
##TEMPORAL FILTERING NOW BETWEEN 0.009 AND 0.1
##LINEAR AND QUADRATIC DETRENDING ADDED


#MAJOR REVISION 2ND MARCH 2010: CREATED SEPARATE REGISTRATION DIRECTORIES FOR FUNC AND ANAT, AND CORRECTED MOTION PARAM OUTPUT

# Initial input and Final output
BIG_INPUT="${funcRaw}"
BIG_OUTPUT="${funcPP_DT_0sm}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT
##CK 12th Feb 2009: replaced FSL filtering with 3dFourier
## high pass and low pass filter using fsl (0.01-0.1 HZ)
##hp=`echo "scale=10;${hpSeconds}/${gTR}" | bc`
##lp=`echo "scale=10;${lpSeconds}/${gTR}" | bc`
# filter = ( seconds / 2 ) / gTR

## sigma for spatial smoothing
sigma=$(echo "scale=10;${gFWHM}/2.3548" | bc)


# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## Dropping first 4 TRS
COMMANDS_INFO[3]="Dropping first 4 TRs"
COMMANDS[3]="3dcalc -a '${funcRaw}[${gTRstart}..${gTRend}]' -expr 'a' -prefix ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[3]="${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## Checking TR
COMMANDS_INFO[4]="Changing TR specification in header to ensure it's correct"
COMMANDS[4]="3drefit -TR ${gTR} ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## time shifting slices
COMMANDS_INFO[5]="Timeshifting (interlaced)"
COMMANDS[5]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[5]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[6]="Deobliquing"
COMMANDS[6]="3drefit -deoblique ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[7]="Reorienting to RPI"
COMMANDS[7]="3dresample -orient RPI -inset ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[7]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

## motion correct using eighth func image with skull
COMMANDS_INFO[8]="Motion correction"
COMMANDS[8]="3dvolreg -Fourier -twopass -base '2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}[7]' -zpad 4 -prefix ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -1Dfile ${funcDir}/${gFN_MC} -maxdisp1D ${funcDir}/${gFN_FUNCBASE}_maxdisp.1D ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[8]="${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"

## skull strip
COMMANDS_INFO[9]="Skull Strip"
COMMANDS[9]="3dAutomask -prefix ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -dilate 1 ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[9]="${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
COMMANDS[10]="3dcalc -a ${preDir}/3b_${gFN_FUNCBASE}_mc.${gOUTPUT_TYPE} -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"

## get eighth func image (WITHOUT SKULL) 
COMMANDS_INFO[11]="Get example func (in this case the eigth image) for registration"
COMMANDS[11]="3dcalc -a '2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}[7]' -b ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} -expr 'a*b' -prefix ${funcExample}"
COMMANDS_OUTPUT[11]="${funcExample}"

## despike
COMMANDS_INFO[12]="Despike"
COMMANDS[12]="3dDespike -prefix ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} ${preDir}/4b_${gFN_FUNCBASE}_st.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[12]="${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE}"

## spatial smoothing
#COMMANDS_INFO[13]="Spatial smoothing"
#COMMANDS[13]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -kernel gauss ${sigma} -fmean -mas ${preDir}/4a_${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE} ${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"
#COMMANDS_OUTPUT[13]="${preDir}/6_${gFN_FUNCBASE}_ss.${gOUTPUT_TYPE}"

## grandmean scaling
COMMANDS_INFO[13]="Grand-mean scaling or intensity normalization"
COMMANDS[13]="fslmaths ${preDir}/5_${gFN_FUNCBASE}_ds.${gOUTPUT_TYPE} -ing 10000 ${preDir}/7_${gFN_FUNCBASE}_gin0sm -odt float"
COMMANDS_OUTPUT[13]="${preDir}/7_${gFN_FUNCBASE}_gin0sm.${gOUTPUT_TYPE}"

## temporal filtering
COMMANDS_INFO[14]="Band-pass filter (get low-frequencies)"
COMMANDS[14]="3dFourier -lowpass ${lp} -highpass ${hp} -retrend -prefix ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE} ${preDir}/7_${gFN_FUNCBASE}_gin0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[14]="${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"

## Detrend
COMMANDS_INFO[15]="Detrend"
COMMANDS[15]="3dTstat -mean -prefix ${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[15]="${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE}"
COMMANDS[16]="3dDetrend -polort 2 -prefix ${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE} ${preDir}/8_${gFN_FUNCBASE}_filt0sm.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[16]="${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE}"
COMMANDS[17]="3dcalc -a ${preDir}/9a_${gFN_FUNCBASE}_mean0sm.${gOUTPUT_TYPE} -b ${preDir}/9b_${gFN_FUNCBASE}_dt0sm.${gOUTPUT_TYPE} -expr 'a+b' -prefix ${funcPP_DT_0sm}"
COMMANDS_OUTPUT[17]="${funcPP_DT_0sm}"
