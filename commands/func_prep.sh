#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, The Underpants Gnomes.

##This script was created to prepare the multi-echo functional data for processing. You must use this script prior to running
##xfc_reg_calib, and xfc_prefunc_calib

# Initial input and Final output
BIG_INPUT="${funcRaw} ${calibDir}/cal_rs.${gOUTPUT_TYPE} ${calibDir}/cal_rho.${gOUTPUT_TYPE} ${calibDir}/cal_bo.${gOUTPUT_TYPE} ${calibDir}/cal_reg_bo.${gOUTPUT_TYPE}"
#BIG_OUTPUT="${calSynth}"

# Setup commands
# Set Variables for commands
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Set Commands
COMMANDS[1]="mkdir ${preDir} 2> /dev/null; echo -n ''"
COMMANDS[2]="cd ${preDir}"

## Dropping first 4 TRS
## 05212010 - Maarten & Reza
## added: read number of volumes from ${funcRaw}, subtract 1 to get TRend, overwrites info from config file
nvols=$(  fslhd ${funcRaw} | cat -v | grep dim4 | awk   '/dim4           /  { print $2 }' )
gTRend=$( echo ${nvols} -1  | bc )
COMMANDS_INFO[3]="Dropping first 4 TRs"
COMMANDS[3]="3dcalc -a '${funcRaw}[${gTRstart}..${gTRend}]' -expr 'a' -prefix ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[3]="${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"

## time shifting slices
COMMANDS_INFO[4]="Timeshifting (interlaced)"
COMMANDS[4]="3dTshift -prefix ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -tpattern alt+z -tzero 0 ${preDir}/1a_${gFN_FUNCBASE}_dr.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[4]="${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## deoblique
COMMANDS_INFO[5]="Deobliquing"
COMMANDS[5]="3drefit -deoblique ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE}"

## orient into fsl friendly space
COMMANDS_INFO[6]="Reorienting to RPI"
COMMANDS[6]="3dresample -orient RPI -inset ${preDir}/1b_${gFN_FUNCBASE}_ts.${gOUTPUT_TYPE} -prefix ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[6]="${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE}"

## get eighth func image (WITH SKULL) 
COMMANDS_INFO[7]="Get example func (in this case the eigth image) for motion correction and registration"
COMMANDS[7]="fslroi ${preDir}/2_${gFN_FUNCBASE}_ro.${gOUTPUT_TYPE} ${funcExample} 7 1"
COMMANDS_OUTPUT[7]="${funcExample}"

##Preparing the various calibration files for registration
COMMANDS_INFO[8]="Preparing Calibration files for registration"

COMMANDS[8]="3dcopy ${calibDir}/cal_rs.${gOUTPUT_TYPE} ${calibDir}/cal_rs"
COMMANDS_OUTPUT[8]="${calibDir}/cal_rs+orig.*"
COMMANDS[9]="3drefit -deoblique ${calibDir}/cal_rs+orig"
COMMANDS[10]="3dresample -orient RPI -inset ${calibDir}/cal_rs+orig -prefix ${calibDir}/cal_rsRPI.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[10]="${calibDir}/cal_rsRPI.${gOUTPUT_TYPE}"

COMMANDS[11]="3dcopy ${calibDir}/cal_rho.${gOUTPUT_TYPE} ${calibDir}/cal_rho"
COMMANDS_OUTPUT[11]="${calibDir}/cal_rho+orig.*"
COMMANDS[12]="3drefit -deoblique ${calibDir}/cal_rho+orig"
COMMANDS[13]="3dresample -orient RPI -inset ${calibDir}/cal_rho+orig -prefix ${calibDir}/cal_rhoRPI.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[13]="${calibDir}/cal_rhoRPI.${gOUTPUT_TYPE}"

COMMANDS[14]="3dcopy ${calibDir}/cal_bo.${gOUTPUT_TYPE} ${calibDir}/cal_bo"
COMMANDS_OUTPUT[14]="${calibDir}/cal_bo+orig.*"
COMMANDS[15]="3drefit -deoblique ${calibDir}/cal_bo+orig"
COMMANDS[16]="3dresample -orient RPI -inset ${calibDir}/cal_bo+orig -prefix ${calibDir}/cal_boRPI.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[16]="${calibDir}/cal_boRPI.${gOUTPUT_TYPE}"

COMMANDS[17]="3dcopy ${calibDir}/cal_reg_bo.${gOUTPUT_TYPE} ${calibDir}/cal_reg_bo"
COMMANDS_OUTPUT[17]="${calibDir}/cal_reg_bo+orig.*"
COMMANDS[18]="3drefit -deoblique ${calibDir}/cal_reg_bo+orig"
COMMANDS[19]="3dresample -orient RPI -inset ${calibDir}/cal_reg_bo+orig -prefix ${calibDir}/cal_reg_boRPI.${gOUTPUT_TYPE}"
COMMANDS_OUTPUT[19]="${calibDir}/cal_reg_boRPI.${gOUTPUT_TYPE}"

##Create matlab command file
COMMANDS_INFO[20]="Creating matlab command"
COMMANDS[20]="echo \"addpath /frodo/shared/gunther-mri/matlab:/frodo/shared/afni/afni_matlab/matlab/:\">>matlab_command.m"
COMMANDS[21]="echo \"cbiCalibSynthEPI_CK('${calibDir}/cal_rho+orig', '${calibDir}/cal_bo+orig', '${calibDir}/cal_rs+orig', 30, 1, '${calibDir}/cal_synth');\">>matlab_command.m"
##Exectute matlab command file
COMMANDS_INFO[22]="Executing matlab command"
COMMANDS[22]="matlab -nojvm -nodisplay -nosplash < matlab_command.m"
COMMANDS[23]="rm matlab_command.m"

COMMANDS_INFO[24]="Reorienting synthetic image to RPI"
COMMANDS[24]="3dresample -orient RPI -inset ${calibDir}/cal_synth+orig -prefix ${calSynth}"
COMMANDS_OUTPUT[24]="${calSynth}"

