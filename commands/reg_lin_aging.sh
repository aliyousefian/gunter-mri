#reg_lin_aging.sh
#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 3.0
# DATE: 11/04/09
# AUTHOR(S): Zarrar Shehzad, The Underpants Gnomes.

#MAJOR REVISION 2ND MARCH 2010: CREATED SEPARATE REGISTRATION DIRECTORIES FOR FUNC AND ANAT

# COMMANDS
declare -a COMMANDS
declare -a COMMANDS_INFO
declare -a COMMANDS_INPUT
declare -a COMMANDS_OUTPUT

# Initial Input and final output
BIG_INPUT="${brain} ${gFN_STANDARD} ${funcExample}"
BIG_OUTPUT="${funcregDir}/diff2standard.${gOUTPUT_TYPE}"

## 1. SETUP
COMMANDS[1]="cd `dirname ${anatregDir}`"
COMMANDS[2]="mkdir ${anatregDir} 2> /dev/null; echo -n ''"
COMMANDS[3]="mkdir ${funcregDir} 2> /dev/null; echo -n ''"


# 2. REGISTER FUNC->T1
# Register functional image to subject's anatomical
COMMANDS[4]="flirt -ref ${brain} -in ${funcExample} -out ${funcregDir}/diff2str -omat ${funcregDir}/diff2str.mat -cost corratio -dof 6 -interp trilinear"
COMMANDS_OUTPUT[4]="${funcregDir}/diff2str.mat"
# Create mat file for conversion from subject's anatomical to functional
COMMANDS[5]="convert_xfm -inverse -omat ${funcregDir}/str2diff.mat ${funcregDir}/diff2str.mat"
COMMANDS_OUTPUT[5]="${funcregDir}/str2diff.mat"

# 3. REGISTER T1->STANDARD
## Linear registration
COMMANDS[6]="flirt -ref ${gFN_STANDARD} -in ${brain} -out ${anatregDir}/str2standard -omat ${anatregDir}/str2standard.mat -cost ${cost_highres2standard} -searchcost ${cost_highres2standard} -dof 12 -interp trilinear"
COMMANDS_OUTPUT[6]="${anatregDir}/str2standard.mat"
## Create mat file for conversion from standard to high res
COMMANDS[7]="convert_xfm -inverse -omat ${anatregDir}/standard2str.mat ${anatregDir}/str2standard.mat"
COMMANDS_OUTPUT[7]="${anatregDir}/standard2str.mat"

# 4. REGISTER FUNC->STANDARD
## Create mat file for registration of functional to standard
COMMANDS[8]="convert_xfm -omat ${funcregDir}/diff2standard.mat -concat ${anatregDir}/str2standard.mat ${funcregDir}/diff2str.mat"
COMMANDS_OUTPUT[8]="${funcregDir}/diff2standard.mat"
## do registration
COMMANDS[9]="flirt -ref ${gFN_STANDARD} -in ${funcExample} -out ${funcregDir}/diff2standard -applyxfm -init ${funcregDir}/diff2standard.mat -interp trilinear"
COMMANDS_OUTPUT[9]="${funcregDir}/diff2standard.${gOUTPUT_TYPE}"
## Create mat file for registration of standard to functional
COMMANDS[10]="convert_xfm -inverse -omat ${funcregDir}/standard2diff.mat ${funcregDir}/diff2standard.mat"
COMMANDS_OUTPUT[10]="${funcregDir}/standard2diff.mat"
