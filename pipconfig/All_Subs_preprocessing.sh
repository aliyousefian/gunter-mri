#Usage: All_Subs_preprocessing.sh

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad
# Modified by Reza Khosrowabadi and Majid Saberi November 2017


#set directory of process and list of subjects
Dir="/home/test05/pipeline-test/Data/"
listfile="/home/test05/pipeline-test/subj.txt"

#set path of general file
source /home/test05/pipeline-test/general.sh



subj=$(cat {$listfile})
declare -i ncore nsubj nloop nrem

#insert number of cpu cores(less than active cores)
ncore=2
#insert number of subjects
nsubj=2


nloop=nsubj/ncore
nrem=nsubj%ncore

#make comment unwilling steps of pre-processing
function preprocess(){
cd ${Dir}/$1
export gDIR_SUBDATA=${Dir}/$1

# anatomica    
    xfc_preanat.sh -B ${Dir} -S $1
	
# resting state
	xfc_prefunc.sh -B ${Dir} -S $1
# Movement parameters are Mean, STD, Max ( of displacement), roll, Pitch, Yaw (rotation about I-S, R-L, A-P axis: CCW), dS, dL, dP (displacement in superior, left and posterior direction: mm)

# registration
    xfc_prereg.sh  -B ${Dir} -n -S $1

# segmentation
	xfc_preseg.sh  -B ${Dir} -S $1

# removing nuisance covariates with global parameter
    xfc_prenuisance_1.sh -B ${Dir} -S $1

# removing nuisance covariates without global parameter
	xfc_prenuisance_2.sh -B ${Dir} -g evs_nuisance_noGlobal.txt -N 5_nuisance -S $1

#for use in GIFT-ICA
	xfc_prefunc_ICA.sh -B ${Dir} -S $1

    return 0
}



####################################
######executing pre-processing######
####################################
for i in $(seq 1 $nloop)
do 
seqcod=""
for j in $(seq 1 $ncore)
do 
seqcod="preprocess $(sed -n $((ncore*$i+1-$j))p ${listfile}) & $seqcod"

done
eval $seqcod 
wait 
echo all processes complete
done


if [ $nrem -ne 0 ]
then 
seqcod=""
for i in $(seq 1 $nrem)
do 
seqcod="preprocess $(sed -n $((ncore*nloop+$i))p ${listfile}) & $seqcod"
done

eval $seqcod 
wait 
echo all processes complete

fi
