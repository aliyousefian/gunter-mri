#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad
# Modified by Reza Khosrowabadi April 2012
######################
# CONFIGURATION FILE #
######################


# SET OUTPUT FILE TYPE
# (based on FSL)

## Extension for time series files (e.g. global signal)
export gTS_EXT="1D"

## Extension for images
case ${FSLOUTPUTTYPE} in
    "ANALYZE" )
        gOUTPUT_TYPE="img"
        ;;
    "NIFTI" )
        gOUTPUT_TYPE="nii"
        ;;
    "NIFTI_PAIR" )
        gOUTPUT_TYPE="img"
        ;;
    "ANALYZE_GZ" )
        gOUTPUT_TYPE="img.gz"
        ;;
    "NIFTI_GZ" )
        gOUTPUT_TYPE="nii.gz"
        ;;
    "NIFTI_PAIR_GZ" )
        gOUTPUT_TYPE="img.gz"
        ;;
    * )
        echo "$FSLOUTPUTTYPE not recognized"
        exit 1
        ;;
esac
export gOUTPUT_TYPE


#
# DO OTHER THINGS NOW
#

# SPECIFIC CONFIG FILE
export gSubConfigFile="$GUNTHERDIR/config/subject.sh"
export gMaskConfigFile="$GUNTHERDIR/config/mask.sh"
export gCreateMaskConfigFile="$GUNTHERDIR/config/createMask.sh"

# LOGGING
export gLOG_FILE="" ## default, no logging
export gLOG_LEVEL=3 ## default, save commands+

# SETTINGS
export gTR=2  ## Your TR
export gTRstart=5  ## HOW MANY IMAGES DO YOU NEED TO DROP - HERE WE ARE DROPPING 0, 1, 2 AND 3, AND STARTING WITH 4
export gTRend=300 ## WHAT NUMBER IMAGE IS AT THE END?
export gVOLNUM=295 ## HOW VOLUMES ARE LEFT? (IE HOW MANY VOLUMES WILL YOU BE ANALYZING?)
export gFWHM=5 ## FULL-WIDTH HALF-MAX OF YOUR SPATIAL SMOOTHING KERNAL (1.5-2 TIMES YOUR VOXEL SIZE)

# MISCELLANEOUS gDIRECTORIES
#export gDIR_MASKS="/root/gunter-test/Data/Masks"
#export gDIR_PRIORS="/opt/gunther-mri/etc/priors"
export gDIR_MASKS="/usr/local/gunter-mri/Masks"
export gDIR_PRIORS="/usr/local/gunther-mri/etc/priors"
export gDIR_LOG="logs"
export gDIR_DESIGN="design"
export gDIR_MAT="mat"
export gDIR_FSF="fsf"


####
# INDIVIDUAL SUBJECT DATA
####

export gDIR_SUBDATA="/home/test05/pipeline-test/Data/" 

# RAW DATA
export gDIR_RAW="Raw"

# QUALITY CONTROL
export gDIR_CHECKS="0_checks"

# ANATOMICAL DATA
# location: gDIR_DATA/subject/gDIR_ANAT 
export gDIR_ANAT="/session_1/anat_1" 
export gFN_MPRAGEBASE="mprage"
export gFN_MPRAGE="${gFN_MPRAGEBASE}.${gOUTPUT_TYPE}"
export gFN_HEAD="head.${gOUTPUT_TYPE}"
export gFN_BRAINBASE="brain"
export gFN_BRAIN="${gFN_BRAINBASE}.${gOUTPUT_TYPE}"

# STANDARD ANATOMICAL
export gDIR_STANDARDS="${FSLDIR}/data/standard"
export gFN_STANDARD_1="${gDIR_STANDARDS}/MNI152_T1_1mm_brain.nii.gz"
export gFN_STANDARD_2="${gDIR_STANDARDS}/MNI152_T1_2mm_brain.nii.gz"
export gFN_STANDARD="${gFN_STANDARD_2}"

# CALIBRATION
export gDIR_CALIB="calib"
export gFN_CALSYNTH="cal_synthRPI.${gOUTPUT_TYPE}"

# FUNCTIONAL DATA
# location: /gDIR_PREDATA/subject/gDIR_FUNC
export gDIR_FUNC="/session_1/rest_1"
export gFN_FUNCBASE="rest" 
export gFN_FUNCRAW="${gFN_FUNCBASE}.${gOUTPUT_TYPE}" 
export gFN_FUNCEXAMPLE="${gFN_FUNCBASE}_example.${gOUTPUT_TYPE}"
export gFN_FUNCMASK="${gFN_FUNCBASE}_mask.${gOUTPUT_TYPE}"
export gFN_FUNCPP="${gFN_FUNCBASE}_pp.${gOUTPUT_TYPE}"
export gFN_FUNCPP_DT="${gFN_FUNCBASE}_pp_DTf.${gOUTPUT_TYPE}"
export gFN_FUNCPP_DT_0sm="${gFN_FUNCBASE}_pp_DTf_0sm.${gOUTPUT_TYPE}"
export gFN_FUNCPP_DT_4sm="${gFN_FUNCBASE}_pp_DTf_4sm.${gOUTPUT_TYPE}"
export gFN_FUNCPP_DT_8sm="${gFN_FUNCBASE}_pp_DTf_6sm.${gOUTPUT_TYPE}"
export gFN_FUNCPP_DT_8sm="${gFN_FUNCBASE}_pp_DTf_8sm.${gOUTPUT_TYPE}"
export gFN_FUNCPP_DT_10fwhm="${gFN_FUNCBASE}_pp_DTf_10fwhm.${gOUTPUT_TYPE}"
export gFN_FUNCRESe="${gFN_FUNCBASE}_res"    
export gFN_FUNCRESe2STANDARD="${gFN_FUNCBASE}_res2standard"
export gFN_FUNCRES="${gFN_FUNCBASE}_normres"
export gFN_FUNCRES2STANDARD="${gFN_FUNCBASE}_normres2standard"
export gFN_FUNCRES_noPW="${gFN_FUNCBASE}_res_noPWf"
export gFN_FUNCRES_noPW_0sm="${gFN_FUNCBASE}_res_noPWf_0sm"
export gFN_FUNCRES_noPW_4sm="${gFN_FUNCBASE}_res_noPWf_4sm"
export gFN_FUNCRES_noPW_8sm="${gFN_FUNCBASE}_res_noPWf_8sm"
export gFN_FUNCRES_noPW_10sm="${gFN_FUNCBASE}_res_noPWf_10sm"
export gFN_FUNCRES_noPW_0sm_noGLOBAL="${gFN_FUNCBASE}_res_noPWf_0sm_noGLOBAL"
export gFN_FUNCRES_noPW_noGLOBAL="${gFN_FUNCBASE}_res_noPWf_noGLOBAL"
export gFN_FUNCRES_noPW2STANDARD="${gFN_FUNCBASE}_res_noPWf2standard"
export gFN_FUNCRES_noPW_0sm_2STANDARD="${gFN_FUNCBASE}_res_noPWf_0sm_2standard"
export gFN_FUNCRES_noPW_4sm_2STANDARD="${gFN_FUNCBASE}_res_noPWf_4sm_2standard"
export gFN_FUNCRES_noPW_8sm_2STANDARD="${gFN_FUNCBASE}_res_noPWf_8sm_2standard"
export gFN_FUNCRES_noPW_10fwhm_2STANDARD="${gFN_FUNCBASE}_res_noPWf_10fwhm_2standard"
export gFN_FUNCRES_noPW_0sm_noGLOBAL_2STANDARD="${gFN_FUNCBASE}_res_noPWf_0sm_noGLOBAL_2standard"
export gFN_FUNCRES_noPW_noGLOBAL_2STANDARD="${gFN_FUNCBASE}_res_noPWf_noGLOBAL_2standard"

# PREPROCESSING (of functional)
# location: /gDIR_PREDATA/subject/gDIR_FUNC/gDIR_PRE
export gDIR_PRE="1_preprocess"

# REGISTRATION
# location: /gDIR_PREDATA/subject/gDIR_FUNC/gDIR_REG
export gDIR_ANAT_REG="reg"
export gDIR_FUNC_REG="2_reg"

# SEGMENTATION
# location: /gDIR_PREDATA/subject/gDIR_FUNC/gDIR_SEGMENT
export gDIR_SEGMENT="3_segment"
export gDIR_SEGMENTPRIORS="${GUNTHERDIR}/etc/priors"
export gPRIOR_WHITE="${gDIR_SEGMENTPRIORS}/avg152T1_white_bin.nii.gz"
export gPRIOR_CSF="${gDIR_SEGMENTPRIORS}/avg152T1_csf_bin.nii.gz"

# NUISANCE COVARIATE FILES
# location: /gDIR_PREDATA/subject/gDIR_FUNC/gDIR_NUISANCE
export gDIR_NUISANCE="4_nuisance"
export gFN_GLOBAL="global.${gTS_EXT}"
export gFN_CSF="csf.${gTS_EXT}"
export gFN_WM="wm.${gTS_EXT}"
export gFN_GLOBAL_0sm="global_0sm.${gTS_EXT}"
export gFN_CSF_0sm="csf_0sm.${gTS_EXT}"
export gFN_WM_0sm="wm_0sm.${gTS_EXT}"
export gFN_GLOBAL_10fwhm="global_10fwhm.${gTS_EXT}"
export gFN_CSF_10fwhm="csf_10fwhm.${gTS_EXT}"
export gFN_WM_10fwhm="wm_10fwhm.${gTS_EXT}"
export gFN_GLOBAL_NONGREY="global_nongrey.${gTS_EXT}"
export gFN_MCBASE="mc"
export gFN_MC="${gFN_FUNCBASE}_${gFN_MCBASE}.${gTS_EXT}"

## PROJECTS DIRECTOR
export gDIR_PROJECTS="/home/test05/pipeline-test/Data/"

# EXTRACTED TIME SERIES
export gDIR_TS="5_seeds"
export gDIR_TS_noPW="5_seeds_noPW"
export gDIR_TS_noPW_noGLOBAL="5_seeds_noPW_noGLOBAL"

# FEATS
export gDIR_FEATS="6_feats"
export gDIR_FEATS_noGLOBAL="6_feats_noGLOBAL"

#AFNI-computed Correlations
export gDIR_CORRS="7_corrs"

#####
# GROUP DATA
#####

# GROUP PROCESSED DATA
#export gDIR_GROUPDATA="/home/test0/pipeline-test/Stats/Gprojects"

