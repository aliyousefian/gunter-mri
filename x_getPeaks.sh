#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/25/08
# AUTHOR(S): Zarrar Shehzad



# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/createROI_fromPeaks.sh" # can override this file at command-line
input=""
outputDir=`pwd`
prefix=""
min_dist=10

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -i <input file> -p <prefix> -o (output directory) -m (minumum distance between voxels)"
    echo "Program:"
    echo "   Will find peaks and corresponding regions"
    echo "Available Options:"
    echo "  -i: input; file to find and generate peaks from, often a thresh_zstat file (required)"
    echo "  -p: prefix; prefix for your generated files (required)"
    echo "  -o: output directory; directory in which to output masks and other stuff (default: ${outputDir})"
    echo "  -m: minimum distance (voxels): the minumum number of voxels between peaks (default: ${min_dist})"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":i:p:r:m:o:dfIhl:L:" Option; do    
    case $Option in
        i ) input="$OPTARG"
            if [[ ! -e "$input" ]]; then
                zerror "Input file '${input}' does not exist"
                exit -1
            fi
    		;;
    	p ) prefix="$OPTARG"
    	    ;;
    	m ) min_dist="$OPTARG"
    	    ;;
    	o ) outputDir="$OPTARG"
    	    if [[ ! -d "$outputDir" ]]; then
    	       zerror "Output directory '${outputDir}' does not exist"
    	       exit -1
    	    fi
    	    ;;
    	h ) usage 3
    	    exit 1
    	    ;;
    	: ) zerror "Argument value required for option '-$OPTARG'"
    	    exit -1
    	    ;;
    	? ) zerror "Unimplemented option '-$OPTARG' chosen"
    	    exit -1
    	    ;;
    	* ) zerror "Unknown error while processing options...continuing anyway"
    	    ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# Checks
## output
if [[ -e ${outputDir}/${prefix}_coords.txt || -e ${outputDir}/${prefix}_regions.txt ]]; then
    zerror "Some sort of output (${outputDir}/${prefix}_regions.txt; ${outputDir}/${prefix}_coords.txt) already exists"
    exit -1
fi
## required options
if [[ -z "$input" || -z "$prefix" ]]; then
    zerror "You missed one of the required options: -i, -p"
    exit -1
fi

# Run
tmpShortFile=`mktemp`

zinfo "Converting data to short"
zcmd "3dcalc -datum short -prefix ${tmpShortFile} -a ${input} -expr 'a'"

zinfo "Finding the peaks and spitting them out"
zcmd "3dmaxima -input ${tmpShortFile}+tlrc -thresh 2.3 -min_dist ${min_dist}"
3dmaxima -input ${tmpShortFile}+tlrc -thresh 2.3 -min_dist ${min_dist} -dset_coords > ${outputDir}/${prefix}_coords.txt

zinfo "Finding the regions and spitting them out"
zcmd "3dmaxima -input ${tmpShortFile}+tlrc -thresh 2.3 -min_dist ${min_dist} -coords_only | whereami -space MNI -tab -coord_file -"
3dmaxima -input ${tmpShortFile}+tlrc -thresh 2.3 -min_dist ${min_dist} -coords_only | whereami -space MNI -tab -coord_file - > ${outputDir}/${prefix}_regions.txt

zinfo "Cleaninp up"
zcmd "rm ${tmpShortFile}*"
