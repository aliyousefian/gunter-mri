#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

iDir="${gDIR_SUBDATA}/${subject}"

## anatomical
anatDir="${iDir}/${gDIR_ANAT}"
mprage="${anatDir}/${gFN_MPRAGE}"
head="${anatDir}/${gFN_HEAD}"
brain="${anatDir}/${gFN_BRAIN}"

## functional
funcDir="${iDir}/${gDIR_FUNC}"

##calibration
calibDir="${iDir}/${gDIR_CALIB}"
calSynth="${iDir}/${gDIR_CALIB}/${gFN_CALSYNTH}"

### dirs
checksDir="${funcDir}/${gDIR_CHECKS}"
preDir="${funcDir}/${gDIR_PRE}"
anatregDir="${anatDir}/${gDIR_ANAT_REG}"
funcregDir="${funcDir}/${gDIR_FUNC_REG}"
segmentDir="${funcDir}/${gDIR_SEGMENT}"
nuisanceDir="${funcDir}/${gDIR_NUISANCE}"
#for multisite
regDir="${funcDir}/${gDIR_REG}"

##project-level
iProjSubDir="${gDIR_PROJECTS}/${subject}"
iProjDir="${iProjSubDir}/${gDIR_FUNC}"
tsDir="${iProjDir}/${gDIR_TS}"
tsDir_noPW="${iProjDir}/${gDIR_TS_noPW}"
tsDir_noPW_noGLOBAL="${iProjDir}/${gDIR_TS_noPW_noGLOBAL}"
featsDir="${iProjDir}/${gDIR_FEATS}"
featsDir_noGLOBAL="${iProjDir}/${gDIR_FEATS_noGLOBAL}"
corrsDir="${iProjDir}/${gDIR_CORRS}"

### files
funcBase="${funcDir}/${gFN_FUNCBASE}"
funcRaw="${funcDir}/${gFN_FUNCRAW}"
funcExample="${funcDir}/${gFN_FUNCEXAMPLE}"
funcMask="${funcDir}/${gFN_FUNCMASK}"
funcPP="${funcDir}/${gFN_FUNCPP}"
funcPP_DT="${funcDir}/${gFN_FUNCPP_DT}"
funcPP_DT_0sm="${funcDir}/${gFN_FUNCPP_DT_0sm}"
funcPP_DT_10fwhm="${funcDir}/${gFN_FUNCPP_DT_10fwhm}"
funcRes="${funcDir}/${gFN_FUNCRES}.${gOUTPUT_TYPE}"
funcRes_noPW="${funcDir}/${gFN_FUNCRES_noPW}.${gOUTPUT_TYPE}"
funcRes_noPW_0sm="${funcDir}/${gFN_FUNCRES_noPW_0sm}.${gOUTPUT_TYPE}"
funcRes_noPW_4sm="${funcDir}/${gFN_FUNCRES_noPW_4sm}.${gOUTPUT_TYPE}"
funcRes_noPW_8sm="${funcDir}/${gFN_FUNCRES_noPW_8sm}.${gOUTPUT_TYPE}"
funcRes_noPW_10sm="${funcDir}/${gFN_FUNCRES_noPW_10sm}.${gOUTPUT_TYPE}"
funcRes_noPW_0sm_noGLOBAL="${funcDir}/${gFN_FUNCRES_noPW_0sm_noGLOBAL}.${gOUTPUT_TYPE}"
funcRes_noPW_noGLOBAL="${funcDir}/${gFN_FUNCRES_noPW_noGLOBAL}.${gOUTPUT_TYPE}"
funcRes_noPW_1="${funcDir}/${gFN_FUNCRES_noPW}_1.${gOUTPUT_TYPE}"
funcRes_noPW_2="${funcDir}/${gFN_FUNCRES_noPW}_2.${gOUTPUT_TYPE}"
funcRes_noPW_3="${funcDir}/${gFN_FUNCRES_noPW}_3.${gOUTPUT_TYPE}"
funcRes_noPW_4="${funcDir}/${gFN_FUNCRES_noPW}_4.${gOUTPUT_TYPE}"
funcRes_noPWStandard="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPW_0sm_Standard="${funcDir}/${gFN_FUNCRES_noPW_0sm_2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPW_4sm_Standard="${funcDir}/${gFN_FUNCRES_noPW_4sm_2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPW_8sm_Standard="${funcDir}/${gFN_FUNCRES_noPW_8sm_2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPW_10sm_Standard="${funcDir}/${gFN_FUNCRES_noPW_10sm_2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPW_0sm_noGLOBAL_Standard="${funcDir}/${gFN_FUNCRES_noPW_0sm_noGLOBAL_2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPW_noGLOBAL_Standard="${funcDir}/${gFN_FUNCRES_noPW_noGLOBAL_2STANDARD}.${gOUTPUT_TYPE}"
funcRes_noPWStandard3mm="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}_3mm.${gOUTPUT_TYPE}"
funcRes_noPWStandard4mm="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}_4mm.${gOUTPUT_TYPE}"
funcRes_noPW_0sm_Standard3mm="${funcDir}/${gFN_FUNCRES_noPW_0sm_2STANDARD}_3mm.${gOUTPUT_TYPE}"
funcRes_noPW_0sm_Standard4mm="${funcDir}/${gFN_FUNCRES_noPW_0sm_2STANDARD}_4mm.${gOUTPUT_TYPE}"
funcRes_noPWStandard_1="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}_1.${gOUTPUT_TYPE}"
funcRes_noPWStandard_2="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}_2.${gOUTPUT_TYPE}"
funcRes_noPWStandard_3="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}_3.${gOUTPUT_TYPE}"
funcRes_noPWStandard_4="${funcDir}/${gFN_FUNCRES_noPW2STANDARD}_4.${gOUTPUT_TYPE}"
#for multisite
funcResE="${funcDir}/${gFN_FUNCRESe}.${gOUTPUT_TYPE}"
funcResEStandard="${funcDir}/${gFN_FUNCRESe2STANDARD}.${gOUTPUT_TYPE}"


## raw data
rawDir="${iDir}/${gDIR_RAW}"

