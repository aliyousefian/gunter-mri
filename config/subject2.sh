#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 09/30/08
# AUTHOR(S): Zarrar Shehzad

iDir="${gDIR_SUBDATA}/${subject}"

## anatomical
anatDir="${iDir}/${gDIR_ANAT}"
mprage="${anatDir}/${gFN_MPRAGE}"
head="${anatDir}/${gFN_HEAD}"
brain="${anatDir}/${gFN_BRAIN}"

## functional
funcDir="${iDir}/${gDIR_FUNC}"
### dirs
checksDir="${funcDir}/${gDIR_CHECKS}"
preDir="${funcDir}/${gDIR_PRE}"
regDir="${funcDir}/${gDIR_REG}"
segmentDir="${funcDir}/${gDIR_SEGMENT}"
nuisanceDir="${funcDir}/${gDIR_NUISANCE}"
tsDir="${funcDir}/${gDIR_TS}"
featsDir="${funcDir}/${gDIR_FEATS}"

### files
funcBase="${funcDir}/${gFN_FUNCBASE}"
funcRaw="${funcDir}/${gFN_FUNCRAW}"
funcExample="${funcDir}/${gFN_FUNCEXAMPLE}"
funcMask="${funcDir}/${gFN_FUNCMASK}"
funcPP="${funcDir}/${gFN_FUNCPP}"
funcRes="${funcDir}/${gFN_FUNCRES}.${gOUTPUT_TYPE}"
funcResE="${funcDir}/${gFN_FUNCRESe}.${gOUTPUT_TYPE}"
funcResE_1="${funcDir}/${gFN_FUNCRESe}_1.${gOUTPUT_TYPE}"
funcResE_2="${funcDir}/${gFN_FUNCRESe}_2.${gOUTPUT_TYPE}"
funcResE_3="${funcDir}/${gFN_FUNCRESe}_3.${gOUTPUT_TYPE}"
funcResE_4="${funcDir}/${gFN_FUNCRESe}_4.${gOUTPUT_TYPE}"
funcResEStandard="${funcDir}/${gFN_FUNCRESe2STANDARD}.${gOUTPUT_TYPE}"
funcResEStandard_1="${funcDir}/${gFN_FUNCRESe2STANDARD}_1.${gOUTPUT_TYPE}"
funcResEStandard_2="${funcDir}/${gFN_FUNCRESe2STANDARD}_2.${gOUTPUT_TYPE}"
funcResEStandard_3="${funcDir}/${gFN_FUNCRESe2STANDARD}_3.${gOUTPUT_TYPE}"
funcResEStandard_4="${funcDir}/${gFN_FUNCRESe2STANDARD}_4.${gOUTPUT_TYPE}"

## raw data
rawDir="${iDir}/${gDIR_RAW}"

