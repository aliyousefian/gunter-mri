#!/usr/bin/env bash

# PACKAGE: Gunther-fcMRI
# VERSION: 2.0
# DATE: 10/24/08
# AUTHOR(S): Zarrar Shehzad

tsFile="${tsDir}/${mask}"
tsFile_noPW="${tsDir_noPW}/${mask}"
tsFile_noPW_noGLOBAL="${tsDir_noPW_noGLOBAL}/${mask}"
maskFile="${gDIR_MASKS}/${mask}.${gOUTPUT_TYPE}"
funcFile="${funcResStandard}"  

