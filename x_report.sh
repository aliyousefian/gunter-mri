#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/20/08
# AUTHOR(S): Zarrar Shehzad

# USAGE
## option to spit out following:
##  config etc files using; general settings; preproc files; proc indiv files; proc group files
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -i <input filename> -T <times to split file> -o <output basename>"
    echo "Program:"
    echo "   Splits a 4D image into a set number of files"
    echo "Required Options:"
    echo "  * -i, -T, -o"
    echo "Available Options:"
    echo "  -i: input filename"
    echo "  -T: number of files that you want to split your input file into"
    echo "  -o: output file basename"
    standardUsage "$1"
    echo
    changebad
}

# PARSE OPTIONS
while getopts ":${standardOptList}" Option; do    
    case $Option in
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done