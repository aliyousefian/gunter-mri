#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 12/19/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/createROI.sh" # can override this file at command-line
maskType=0
maskList=""
roi_radius=""

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -z <radius> -m <file with list of ROIs>"
    echo "  $0 [OPTIONS] -z <radius> -M <mask name> <x coordinate> <y coordinate> <z coordinate>"
    echo "Program:"
    echo "   Will create ROIs"
    echo "Required Options:"
    echo "  * -M or -m"
    echo "  * -z"
    echo "Notes: "
    echo "  * The -M and -m options must be given as the last option"
    echo "Available Options:"
    echo "  -z: radius; the radius (in mm) for each generated ROI (required)"
    echo "  -m: a file that lists on each line: names of ROI, x coordinate, y coordinate, and z coordinate; each ROI name will correspond to the filename of a mask"
    echo "  -M: a specific ROI name with the corresponding x coordinate, y coordinate, and z coordinate"
    echo "  -o: directory in which ROI masks will be found (default: ${gDIR_MASKS})"
    standardUsage 0
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":mMo:z:${standardOptList}" Option; do    
    case $Option in
        m ) maskType=2
            ;;
        M ) maskType=1
            ;;
        o ) gDIR_MASKS="$OPTARG"
            if [[ ! -d "$gDIR_MASKS" ]]; then
                zerror "Cannot find the output masks directory '${gDIR_MASKS}'!"
                exit -1
            fi
            ;;
    	z ) roi_radius="$OPTARG"
    	    ;;
    	h ) usage 2
    	    exit 1
    	    ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))
# Get mask list
maskList="$@"


# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"

# Check on required input
if [[ "$maskType" = 0 ]]; then
    zerror "Please specify mask list (-m or -M)"
    usage
    exit -2
fi
if [[ -z "$roi_radius" ]]; then
    zerror "Please specify the ROI radius (-z)"
    usage
    exit -2
fi

###
# BEGIN PROCESSING
#
## see loops.sh
createMaskLoop "$commandFile" "$maskType" "$maskList"
