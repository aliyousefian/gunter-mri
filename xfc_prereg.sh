#!/usr/bin/env bash

# REGISTRATION SCRIPT
# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 10/16/08
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# VARIABLES
commandFile="${GUNTHERDIR}/commands/reg_lin.sh"
cost_func2highres="corratio"
cost_highres2standard="corratio"
linReg=1
nonlinReg=0
commandFile_nonlin="${GUNTHERDIR}/commands/reg_nonlin.sh"

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 [OPTIONS] -s <subject files>"
    echo "  $0 [OPTIONS] -S <subject list>"
    echo "Program:"
    echo "   Registration of preprocessed functional images"
    echo "Required Options:"
    echo "  * -S or -s"
    echo "Available Options:"
    echo "  -o: cost; Cost + search cost function for func -> highres, see flirt for options (default: ${cost_func2highres})"
    echo "  -O: cost; Cost + search cost function for highres -> standard, see flirt for options (default: ${cost_highres2standard})"
    echo "  -n: nonlinear registration; run nonlinear registration after running linear registration"
    echo "  -N: nonlinear registration; only run nonlinear registration (by default the program will run LINEAR registration, which MUST BE RUN FIRST)"
    standardUsage "$1"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":o:O:nN${standardOptList}" Option; do    
    case $Option in
        o ) case "$OPTARG" in
    			mutualinfo | corratio | normcorr | normmi | leastsq )
    				cost_func2highres="$OPTARG"
    				;;
    			* ) 
    				zwarn "$OPTARG is not a valid cost for option -o, see flirt for details"
    				exit -1
    				;;
    		esac
    		;;
    	O ) case "$OPTARG" in
    			mutualinfo | corratio | normcorr | normmi | leastsq )
    				cost_highres2standard="$OPTARG"
    				;;
    			* ) 
    				zwarn "$OPTARG is not a valid cost for option -O, see flirt for details"
    				exit -1
    				;;
    		esac
    		;;
    	n ) nonlinReg=1
    	    ;;
    	N ) linReg=0
    	    nonlinReg=1
    	    ;;
        * ) # Reference standard options found in options.sh
            standardOpts "$Option" "$OPTARG"
            ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

# Give user feedback of what is running
ztitle "Executing: '$0 ${command}'"


###
# BEGIN PROCESSING
#
## see loops.sh
## linear registration
if [[ "$linReg" == 1 ]]; then
    subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile"
fi
## nonlinear registration
if [[ "$nonlinReg" == 1 ]]; then
    subLoop "$subtype" "$*" "$gSubConfigFile" "$commandFile_nonlin"
fi
