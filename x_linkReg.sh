#!/usr/bin/env bash

# PACKAGE: Gunther
# VERSION: 2.0
# DATE: 01/06/09
# AUTHOR(S): Zarrar Shehzad

# INCLUDE
source "$GUNTHERDIR/include/Asourceall.sh"

# USAGE
function usage {
    changegood green
    echo
    echo "Usage"
    echo "  $0 -i <input reg directory> -d <feat directory>"
    echo "  $0 -i <input reg directory> -f <fsf file>"
    echo "Program:"
    echo "   Will create a soft link (shortcut) in a feat directory to a previous registration directory"
    echo "Available Options:"
    echo "  -i: input; directory where registration has already been done"
    echo "  -d: output; feat directory in which to create 'reg' directory link"
    echo "  -f: output; fsf file in which to find the feat directory path and create a 'reg' directory link in it"
    echo
    changebad
}

if [[ "$#" = 0 ]]; then
    usage
    exit 2
fi

# PARSE OPTIONS
while getopts ":i:d:f:" Option; do    
    case $Option in
        i ) inputDir="$OPTARG"
            if [[ ! -e "$inputDir" ]]; then
                zerror "Input reg directory '${inputDir}' does not exist"
                exit -1
            fi
    		;;
    	d ) outputDir="$OPTARG"
    	    if [[ ! -d "$outputDir" ]]; then
                zerror "Output feat directory '${outputDir}' does not exist"
    	        exit -1
    	    fi
    	    ;;
    	f ) fsfFile="$OPTARG"
    	    if [[ ! -e "$fsfFile" ]]; then
    	        zerror "Fsf file '${fsfFile}' does not exist"
    	        exit -1
    	    else
    	        outputDir=$( x_fsfinfo.sh "set fmri(outputdir)" ${fsfFile} | sed s/.feat// ).feat
    	        if [[ ! -d "$outputdir" ]]; then
                    zerror "Output feat directory '${outputDir}' does not exist"
        	        exit -1
        	    fi
    	    fi
    	    ;;
    	: ) zerror "Argument value required for option '-$OPTARG'"
    	    exit -1
    	    ;;
    	? ) zerror "Unimplemented option '-$OPTARG' chosen"
    	    exit -1
    	    ;;
    	* ) zerror "Unknown error while processing options...continuing anyway"
    	    ;;
    esac
done

## save command
command="$@"
## move past any options
shift $((${OPTIND}-1))

zcmd "ln -s ${inputDir} ${outputDir}/reg"

